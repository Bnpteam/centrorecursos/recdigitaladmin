package pe.gob.bnp.recdigital.category.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.recdigital.category.model.Category;
import pe.gob.bnp.recdigital.category.repository.CategoryRepository;
import pe.gob.bnp.recdigital.category.validation.CategoryValidation;
import pe.gob.bnp.recdigital.category.dto.CategoryDto;
import pe.gob.bnp.recdigital.category.dto.CategoryRequest;
import pe.gob.bnp.recdigital.category.exception.CategoryException;
import pe.gob.bnp.recdigital.category.mapper.CategoryMapper;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

@Service
public class CategoryService {
	
	private String codeSuccess = "0000";

	@Autowired
	CategoryRepository categoryRepository;

	@Autowired
	CategoryMapper categoryMapper;

	public ResponseTransaction save(CategoryDto categoryDto) {

		Notification notification = CategoryValidation.validation(categoryDto);
		
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}

		Category category= categoryMapper.reverseMapperSave(categoryDto);
		ResponseTransaction response = categoryRepository.persist(category);
		if (response!= null && response.getCodeResponse()!=null && response.getCodeResponse().equals(codeSuccess)){
			for (Category  subcategory : category.getSubcategories()) {
					categoryRepository.persistSubcategory(Utility.parseStringToLong(response.getId()),subcategory);			  
			}
		}
		
		return CategoryException.setMessageResponseSave(response);

	}
	
	public ResponseTransaction update(Long id,CategoryDto categoryDto) {
		
    	Notification notification = CategoryValidation.validation(categoryDto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}

		Category category = categoryMapper.reverseMapperUpdate(id,categoryDto);
		ResponseTransaction response = categoryRepository.update(category);
		if (response!= null && response.getCodeResponse()!=null && response.getCodeResponse().equals(codeSuccess)){
			for (Category  subcategory : category.getSubcategories()) {
					System.out.println("tiene subcategorias");
					categoryRepository.persistSubcategory(Utility.parseStringToLong(response.getId()),subcategory);			  
			}
		}
		
		return CategoryException.setMessageResponseUpdate(response);

	}
	
	public ResponseTransaction searchAll(String name){
			CategoryRequest categoryRequest = new CategoryRequest();
			categoryRequest.setName(name);
			
			Notification notification = CategoryValidation.validation(categoryRequest);
			if (notification.hasErrors()) {
				throw new IllegalArgumentException(notification.errorMessage());
			}		
			return categoryRepository.list(categoryRequest);
	}//searchSubcategoriesAll
	
	public ResponseTransaction searchSubcategoriesAll(Long id){
		Notification notification = CategoryValidation.validation(id);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		/*CategoryRequest categoryRequest = new CategoryRequest();
		categoryRequest.setName(name);
		
		Notification notification = CategoryValidation.validation(categoryRequest);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}*/		
		return categoryRepository.listSubcategory(id);
}
	
	public ResponseTransaction get(Long id) {
		Notification notification = CategoryValidation.validation(id);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		return categoryRepository.read(id);
	}

}
