package pe.gob.bnp.recdigital.category.model;

import java.util.ArrayList;
import java.util.List;
import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.model.BaseEntity;

public class Category extends BaseEntity{
	
	private String  categoryCode;
	private String  categoryName;
	private Integer categoryOrder;
	private String subcategoryCode;
	private List<Category> subcategories;

	
	public Category() {
		super();
		this.categoryCode = Constants.EMPTY_STRING;;
		this.categoryName = Constants.EMPTY_STRING;;
		this.categoryOrder=Constants.ZERO_INTEGER;
		this.subcategoryCode=Constants.EMPTY_STRING;		
		this.subcategories = new ArrayList<>();
	}


	public String getCategoryCode() {
		return categoryCode;
	}


	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}


	public String getCategoryName() {
		return categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public Integer getCategoryOrder() {
		return categoryOrder;
	}


	public void setCategoryOrder(Integer categoryOrder) {
		this.categoryOrder = categoryOrder;
	}


	public String getSubcategoryCode() {
		return subcategoryCode;
	}


	public void setSubcategoryCode(String subcategoryCode) {
		this.subcategoryCode = subcategoryCode;
	}


	public List<Category> getSubcategories() {
		return subcategories;
	}


	public void setSubcategories(List<Category> subcategories) {
		this.subcategories = subcategories;
	}

}
