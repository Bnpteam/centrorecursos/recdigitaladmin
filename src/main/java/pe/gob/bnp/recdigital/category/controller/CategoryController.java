package pe.gob.bnp.recdigital.category.controller;



//@RestController
//@RequestMapping("/api")
//@Api(value="/api/category")
//@CrossOrigin(origins = "*")
public class CategoryController {

/*	@Autowired
	ResponseHandler responseHandler;
	
	@Autowired
	CategoryService categoryService;

	
	@RequestMapping(method = RequestMethod.POST,path="/category", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "registrar categoria", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> save(
			@ApiParam(value = "Estructura JSON de Categoria", required = true)
			@RequestBody CategoryDto categoryDto) throws Exception {
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = categoryService.save(categoryDto);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}
	
	@RequestMapping(method = RequestMethod.PUT,path="/category/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "actualizar categoria", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> update(
			@ApiParam(value = "Estructura JSON de categoria", required = true)
			@PathVariable Long id,@RequestBody CategoryDto categoryDto) throws Exception {
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = categoryService.update(id,categoryDto);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}
		
	@RequestMapping(method = RequestMethod.GET, path="/categories",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar categorias", response= CategoryResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAll(
			@RequestParam(value = "name", defaultValue = "", required=false) String  name){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = categoryService.searchAll(name);
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros."+ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/subcategories/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar subcategorias x id categoria", response= CategoryResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAll(
			@ApiParam(value = "id categoria", required = true)
			@PathVariable Long id){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = categoryService.searchSubcategoriesAll(id);
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros."+ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	
	/*@RequestMapping(method = RequestMethod.GET, path="/category/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "leer categoria", response= SliderResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> get(
			@ApiParam(value = "id categoria", required = true)
			@PathVariable Long id){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = categoryService.get(id);
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}	*/

}
