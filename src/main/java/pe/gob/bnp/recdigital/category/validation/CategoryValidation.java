package pe.gob.bnp.recdigital.category.validation;

import pe.gob.bnp.recdigital.category.dto.CategoryDto;
import pe.gob.bnp.recdigital.category.dto.CategoryRequest;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class CategoryValidation {
	private static String messageCategorySave = "No se encontraron datos de la Categoria";
	private static String messageCategorySearchAll = "No se encontró el filtro de búsqueda";
	private static String messageNameSave = "Se debe ingresar el nombre de la categoria";
	private static String messageCategoryGet = "El id debe ser mayor a cero";

	public static Notification validation(CategoryDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageCategorySave);
			return notification;
		}

		if (Utility.isEmptyOrNull(dto.getName())) {
			notification.addError(messageNameSave);
		}

		return notification;
	}

	public static Notification validation(CategoryRequest dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageCategorySearchAll);
			return notification;
		}

		return notification;
	}
	
	public static Notification validation(Long id) {
		Notification notification = new Notification();
		if (id <=0L) {
			notification.addError(messageCategoryGet);
			return notification;
		}		
		return notification;
	}	

}
