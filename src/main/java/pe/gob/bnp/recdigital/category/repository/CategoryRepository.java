package pe.gob.bnp.recdigital.category.repository;

import org.springframework.stereotype.Repository;
import pe.gob.bnp.recdigital.category.dto.CategoryRequest;
import pe.gob.bnp.recdigital.category.model.Category;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.repository.BaseRepository;

@Repository
public interface CategoryRepository extends BaseRepository<Category>{	

	public ResponseTransaction persistSubcategory(Long idCategory,Category subcategory); 
	public ResponseTransaction list(CategoryRequest categoryRequest);//listSubcategory
	public ResponseTransaction listSubcategory(Long id);
	public ResponseTransaction read(Long id);
}
