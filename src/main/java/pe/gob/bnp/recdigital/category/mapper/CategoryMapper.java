package pe.gob.bnp.recdigital.category.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import pe.gob.bnp.recdigital.category.dto.CategoryDto;
import pe.gob.bnp.recdigital.category.model.Category;
import pe.gob.bnp.recdigital.utilitary.common.Utility;


@Component
public class CategoryMapper {
	
	public Category reverseMapperSave(CategoryDto categoryDto) {
		Category category= new Category();	
		
		category.setCategoryName(categoryDto.getName());
		category.setCategoryOrder(Utility.parseStringToInt(categoryDto.getOrder()));
		category.setSubcategories(reverseMapperList(categoryDto.getSubcategories()));
		category.setCreatedUser(categoryDto.getCreatedUser());
		return category;
	}
	
	public Category reverseMapperUpdate(Long id,CategoryDto categoryDto) {
		Category category= new Category();	
		
		categoryDto.setId(Utility.parseLongToString(id));
		category.setId(Utility.parseStringToLong(categoryDto.getId()));
		category.setCategoryName(categoryDto.getName());
		category.setCategoryOrder(Utility.parseStringToInt(categoryDto.getOrder()));
		category.setSubcategories(reverseMapperList(categoryDto.getSubcategories()));
		category.setIdEnabled(categoryDto.getEnabled());	
		category.setUpdatedUser(categoryDto.getUpdatedUser());
		return category;
	}
	
	private List<Category> reverseMapperList(List<CategoryDto> categoriesDto){
		List<Category> categories=new ArrayList<>();
	
		
		for(CategoryDto categoryDto:categoriesDto) {
			Category category= new Category();
			category.setCategoryName(categoryDto.getName());
			category.setCategoryOrder(Utility.parseStringToInt(categoryDto.getOrder()));
			category.setSubcategories(reverseMapperList(categoryDto.getSubcategories()));
			category.setCreatedUser(categoryDto.getCreatedUser());
			categories.add(category);
		}		
		
		return categories;
		
	}

}
