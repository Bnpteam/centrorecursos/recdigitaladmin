package pe.gob.bnp.recdigital.category.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigital.category.dto.CategoryRequest;
import pe.gob.bnp.recdigital.category.dto.CategoryResponse;
import pe.gob.bnp.recdigital.category.model.Category;
import pe.gob.bnp.recdigital.category.repository.CategoryRepository;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;
import pe.gob.bnp.recdigital.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class CategoryRepositoryJDBC extends BaseJDBCOperation implements CategoryRepository{
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction persist(Category entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_CATEGORY.SP_PERSIST_CATEGORY(?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setString("P_NAME", Utility.getString(entity.getCategoryName()));
			cstm.setInt("P_ORDER",entity.getCategoryOrder());
			cstm.setLong("P_CREATED_USER", Utility.parseStringToLong(entity.getCreatedUser()));			
			cstm.registerOutParameter("S_CATEGORY_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_CATEGORY_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
	@Override
	public ResponseTransaction persistSubcategory(Long idCategory,Category entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_CATEGORY.SP_PERSIST_SUBCATEGORY(?,?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setLong("P_CATEGORY_ID", idCategory);
			cstm.setString("P_NAME", Utility.getString(entity.getCategoryName()));
			cstm.setInt("P_ORDER",entity.getCategoryOrder());
			cstm.setLong("P_CREATED_USER", Utility.parseStringToLong(entity.getCreatedUser()));			
			cstm.registerOutParameter("S_SUBCATEGORY_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");		
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_SUBCATEGORY_ID"));
				System.out.println("id Subcategory:"+id);
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();
				response.setCodeResponse("0007");		
				response.setResponse("Error: No se pudo insertar el registro de la subcategoria");
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction update(Category entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_CATEGORY.SP_UPDATE_CATEGORY(?,?,?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_ID", entity.getId());
			cstm.setString("P_NAME", Utility.getString(entity.getCategoryName()));		
			cstm.setInt("P_ORDER",entity.getCategoryOrder());
			cstm.setString("P_ENABLED",Utility.getString(entity.getIdEnabled()));
			cstm.setLong("P_UPDATED_USER", Utility.parseStringToLong(entity.getUpdatedUser()));			
			cstm.registerOutParameter("S_CATEGORY_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_CATEGORY_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	
	/*@Override
	public ResponseTransaction list(CategoryRequest categoryRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_CATEGORY.SP_LIST_CATEGORY(?,?,?)}";

        List<Object> categoriesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("P_NAME", Utility.getString(categoryRequest.getName()));			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {					
					ResponseTransaction categoryResponse=  this.read(rs.getLong("CATEGORY_ID"));
					categoriesResponse.add(categoryResponse.getList());
				}
				response.setList(categoriesResponse);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}*/
	
	@Override
	public ResponseTransaction list(CategoryRequest categoryRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_CATEGORY.SP_LIST_CATEGORY(?,?,?)}";

        List<Object> categoriesResponse = new ArrayList<>();       
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("P_NAME", Utility.getString(categoryRequest.getName()));			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					 CategoryResponse categoryResponse = new CategoryResponse();
					categoryResponse.setId(Utility.parseLongToString(rs.getLong("CATEGORY_ID")));					
					categoryResponse.setName(Utility.getString(rs.getString("CATEGORY_NAME")));
					categoriesResponse.add(categoryResponse);
				}
				
				response.setList(categoriesResponse);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	@Override
	public ResponseTransaction listSubcategory(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_CATEGORY.SP_LIST_SUBCATEGORY(?,?,?)}";

        List<Object> categoriesResponse = new ArrayList<>();       
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_CATEGORYID",id);			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					CategoryResponse categoryResponse = new CategoryResponse();
					categoryResponse.setId(Utility.parseLongToString(rs.getLong("SUBCATEGORY_ID")));					
					categoryResponse.setName(Utility.getString(rs.getString("SUBCATEGORY_NAME")));
					categoriesResponse.add(categoryResponse);
				}
				
				response.setList(categoriesResponse);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	@Override
	public ResponseTransaction read(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_CATEGORY.SP_GET_CATEGORY(?,?,?,?)}";
       // CategoryResponse categoryResponse = new CategoryResponse();
        
        CategoryResponse categoryResponse = new CategoryResponse();
        List<Object> categoriesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_CATEGORY_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);		
			cstm.registerOutParameter("S_C_CURSOR_SUBCATEGORY",OracleTypes.CURSOR);		
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {					
					categoryResponse.setId(Utility.parseLongToString(rs.getLong("CATEGORY_ID")));
				//	categoryResponse.setCode(Utility.getString(rs.getString("CATEGORY_CODE")));
					categoryResponse.setName(Utility.getString(rs.getString("CATEGORY_NAME")));
			//		categoryResponse.setOrder(Utility.parseLongToString(rs.getLong("CATEGORY_ORDER")));
			//		categoryResponse.setEnabled(Utility.getString(rs.getString("CATEGORY_ENABLED")));
				}	
				this.closeResultSet(rs);	
				
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_SUBCATEGORY");
				List<CategoryResponse> subcategoryDto = new ArrayList<>();
				while (rs.next()) {
					CategoryResponse entidad = new CategoryResponse();
					entidad.setId(Utility.parseLongToString(rs.getLong("SUBCATEGORY_ID")));
				//	entidad.setCode(Utility.getString(rs.getString("SUBCATEGORY_CODE")));
					entidad.setName(Utility.getString(rs.getString("SUBCATEGORY_NAME")));
				//	entidad.setOrder(Utility.parseLongToString(rs.getLong("SUBCATEGORY_ORDER")));
			//		entidad.setEnabled(Utility.getString(rs.getString("SUBCATEGORY_ENABLED")));
					subcategoryDto.add(entidad);
				}	
				//categoryResponse.setSubcategories(subcategoryDto);
				
				if(categoryResponse!=null && !Utility.isEmptyOrNull(categoryResponse.getName()) ) {
					categoriesResponse.add(categoryResponse);
					
				}
				response.setList(categoriesResponse);	
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}


	
}
