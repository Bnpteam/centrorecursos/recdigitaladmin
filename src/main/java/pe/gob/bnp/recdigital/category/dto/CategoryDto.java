package pe.gob.bnp.recdigital.category.dto;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.recdigital.category.model.Category;
import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class CategoryDto {
	
	private String id;
	private String name;
	private String order;
	private List<CategoryDto> subcategories;
	private String enabled;
	private String createdUser;
	private String updatedUser;	
	
	public CategoryDto() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.name = Constants.EMPTY_STRING;
		this.order = Constants.EMPTY_STRING;
		this.enabled = Constants.EMPTY_STRING;
		this.createdUser=Constants.EMPTY_STRING;
		this.updatedUser=Constants.EMPTY_STRING;
		this.subcategories=new ArrayList<>();
	}	

	public CategoryDto(Category category) {
		super();
		this.id =Utility.parseLongToString(category.getId());
		this.name = category.getCategoryName();		
		this.order = Utility.parseIntToString(category.getCategoryOrder());
		this.enabled = category.getIdEnabled();
		this.createdUser=category.getCreatedUser();
		this.updatedUser=category.getUpdatedUser();
		this.subcategories=new ArrayList<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public List<CategoryDto> getSubcategories() {
		return subcategories;
	}

	public void setSubcategories(List<CategoryDto> subcategories) {
		this.subcategories = subcategories;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

}
