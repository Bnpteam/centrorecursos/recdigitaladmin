package pe.gob.bnp.recdigital.category.dto;

import pe.gob.bnp.recdigital.utilitary.common.Constants;

public class CategoryResponse {
	
	private String id;
	private String name;	
	
	public CategoryResponse() {
		super();
		this.id=Constants.EMPTY_STRING;		
		this.name=Constants.EMPTY_STRING;
	
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
}
