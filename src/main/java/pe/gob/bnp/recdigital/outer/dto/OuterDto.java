package pe.gob.bnp.recdigital.outer.dto;

import pe.gob.bnp.recdigital.outer.model.OuterUser;
import pe.gob.bnp.recdigital.utilitary.common.Constants;

public class OuterDto {
	
	private String email;
	private String enabled;
	private String updatedUser;
	
	public OuterDto() {
		super();		
		this.email = Constants.EMPTY_STRING;
		this.enabled = Constants.EMPTY_STRING;		
		this.updatedUser=Constants.EMPTY_STRING;	
	}	

	public OuterDto(OuterUser outerUser) {
		super();		
		this.email = outerUser.getPublicUserEmail();			
		this.enabled = outerUser.getIdEnabled();		
		this.updatedUser=outerUser.getUpdatedUser();	
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

}
