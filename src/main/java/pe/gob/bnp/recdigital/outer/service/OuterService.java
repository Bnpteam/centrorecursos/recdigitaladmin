package pe.gob.bnp.recdigital.outer.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.bnp.recdigital.outer.dto.OuterDto;
import pe.gob.bnp.recdigital.outer.dto.OuterRequest;
import pe.gob.bnp.recdigital.outer.model.OuterUser;
import pe.gob.bnp.recdigital.outer.repository.OuterRepository;
import pe.gob.bnp.recdigital.outer.validation.OuterValidation;
import pe.gob.bnp.recdigital.resource.exception.CourseException;
import pe.gob.bnp.recdigital.slider.mapper.SliderMapper;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;

@Service
public class OuterService {

	@Autowired
	OuterRepository outerRepository;

	@Autowired
	SliderMapper sliderMapper;
	
	public ResponseTransaction update(Long id, OuterDto courseDto) {

		ResponseTransaction response = new ResponseTransaction();

		Notification notification = new Notification();

		if (courseDto == null) {
			notification.addError("No se encontraron datos para actualizar");
		}
		
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}					
		
		OuterUser outeruser= new OuterUser();	
		
		outeruser.setId(id);
		outeruser.setPublicUserEmail(courseDto.getEmail());;
		outeruser.setIdEnabled(courseDto.getEnabled());	
		outeruser.setUpdatedUser(courseDto.getUpdatedUser());		
		
		response = outerRepository.update(outeruser);		

		return CourseException.setMessageResponseUpdate(response);

	}

	public ResponseTransaction searchAll(String keyword,String page, String size) {
		ResponseTransaction response = new ResponseTransaction();
		
		OuterRequest outerRequest = new OuterRequest();
		outerRequest.setKeyword(keyword);
		outerRequest.setPage(page);
		outerRequest.setSize(size);

		Notification notification = OuterValidation.validation(outerRequest);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			System.out.println("0077");
			return response;
		}

		return outerRepository.list(outerRequest);
	}
	
	public ResponseTransaction get(Long id) throws IOException {
		/*
		 * Notification notification = ButtonValidation.validation(id); if
		 * (notification.hasErrors()) { throw new
		 * IllegalArgumentException(notification.errorMessage()); }
		 */
		ResponseTransaction response = outerRepository.read(id);

		return response;
	}
}
