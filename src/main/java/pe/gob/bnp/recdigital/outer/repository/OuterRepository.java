package pe.gob.bnp.recdigital.outer.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.recdigital.outer.dto.OuterRequest;
import pe.gob.bnp.recdigital.outer.model.OuterUser;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.repository.BaseRepository;


@Repository
public interface OuterRepository extends BaseRepository<OuterUser>{
	
	public ResponseTransaction list(OuterRequest outerRequest);
	public ResponseTransaction read(Long id);

}
