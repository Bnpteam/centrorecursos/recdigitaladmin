package pe.gob.bnp.recdigital.outer.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigital.outer.dto.OuterRequest;
import pe.gob.bnp.recdigital.outer.dto.OuterResponse;
import pe.gob.bnp.recdigital.outer.model.OuterUser;
import pe.gob.bnp.recdigital.outer.repository.OuterRepository;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;
import pe.gob.bnp.recdigital.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class OuterRepositoryJDBC extends BaseJDBCOperation implements OuterRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(OuterRepositoryJDBC.class);
	
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction persist(OuterUser outeruser) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTransaction update(OuterUser entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_OUTER_USER.SP_UPDATE_OUTER(?,?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();       
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_ID", entity.getId());
			cstm.setString("P_EMAIL", Utility.getString(entity.getPublicUserEmail()));			
			cstm.setString("P_ENABLED",Utility.getString(entity.getIdEnabled()));
			cstm.setLong("P_UPDATED_USER", Utility.parseStringToLong(entity.getUpdatedUser()));
			cstm.registerOutParameter("S_OUTER_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);		
			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_OUTER_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error("PKG_API_OUTER_USER.SP_UPDATE_OUTER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction list(OuterRequest outerRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_OUTER_USER.SP_LIST_OUTER(?,?,?,?,?)}";
        
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setString("P_KEYWORD", Utility.getString(outerRequest.getKeyword()));
			cstm.setInt("P_PAGE", Utility.parseStringToInt(outerRequest.getPage()));	
			cstm.setInt("P_SIZE", Utility.parseStringToInt(outerRequest.getSize()));	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					OuterResponse resourceResponse = new OuterResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("USER_ID")));
					resourceResponse.setName(Utility.getString(rs.getString("USER_NAME")));
					resourceResponse.setEmail(Utility.getString(rs.getString("USER_EMAIL")));
					resourceResponse.setBirthDate(Utility.parseSqlDateToString(rs.getDate("USER_BIRTHDATE")));
					resourceResponse.setEnabled(Utility.getString(rs.getString("USER_ENABLED")));	
					resourceResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
					resourceResponse.setLastLoginDate(Utility.parseDateToString(Utility.getDate(rs.getDate("LAST_LOGIN_DATE"))));
					resourceResponse.setTotalRecords(Utility.getString(rs.getString("TOTAL_RECORDS")));
									
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_OUTER_USER.SP_LIST_OUTER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	@Override
	public ResponseTransaction read(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_OUTER_USER.SP_GET_OUTER(?,?,?)}";
        
        List<Object> subscriptionsResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_OUTER_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					OuterResponse resourceResponse = new OuterResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("USER_ID")));
					resourceResponse.setName(Utility.getString(rs.getString("USER_NAME")));
					resourceResponse.setEmail(Utility.getString(rs.getString("USER_EMAIL")));
					resourceResponse.setBirthDate(Utility.parseSqlDateToString(rs.getDate("USER_BIRTHDATE")));
					resourceResponse.setDepartment(Utility.getString(rs.getString("USER_DEPARTMENT")));
					resourceResponse.setProvince(Utility.getString(rs.getString("USER_PROVINCE")));
					resourceResponse.setDistrict(Utility.getString(rs.getString("USER_DISTRICT")));
					resourceResponse.setPosition(Utility.getString(rs.getString("USER_POSITION")));
					resourceResponse.setLibrary(Utility.getString(rs.getString("USER_LIBRARY")));
					resourceResponse.setEnabled(Utility.getString(rs.getString("USER_ENABLED")));
					resourceResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
					resourceResponse.setLastLoginDate(Utility.parseDateToString(Utility.getDate(rs.getDate("LAST_LOGIN_DATE"))));
				
					subscriptionsResponse.add(resourceResponse);
				}	
				response.setList(subscriptionsResponse);					
			}
		}catch(SQLException e) {
			logger.error("PKG_API_OUTER_USER.SP_GET_OUTER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
}
