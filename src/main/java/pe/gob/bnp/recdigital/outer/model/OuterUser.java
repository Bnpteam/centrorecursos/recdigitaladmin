package pe.gob.bnp.recdigital.outer.model;

import java.util.Date;

import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.model.BaseEntity;

public class OuterUser extends BaseEntity{	
	
	private String publicUserName;
	private String publicUserSurname;
	private String publicUserEmail;
	private String publicUserPassword;
	private Date publicUserBirthdate;

	public OuterUser() {
		super();
		this.publicUserName = Constants.EMPTY_STRING;
		this.publicUserSurname = Constants.EMPTY_STRING;
		this.publicUserEmail = Constants.EMPTY_STRING;
		this.publicUserPassword = Constants.EMPTY_STRING;
		this.publicUserBirthdate =new Date();
	}

	public String getPublicUserName() {
		return publicUserName;
	}

	public void setPublicUserName(String publicUserName) {
		this.publicUserName = publicUserName;
	}

	public String getPublicUserSurname() {
		return publicUserSurname;
	}

	public void setPublicUserSurname(String publicUserSurname) {
		this.publicUserSurname = publicUserSurname;
	}

	public String getPublicUserEmail() {
		return publicUserEmail;
	}

	public void setPublicUserEmail(String publicUserEmail) {
		this.publicUserEmail = publicUserEmail;
	}

	public String getPublicUserPassword() {
		return publicUserPassword;
	}

	public void setPublicUserPassword(String publicUserPassword) {
		this.publicUserPassword = publicUserPassword;
	}

	public Date getPublicUserBirthdate() {
		return publicUserBirthdate;
	}

	public void setPublicUserBirthdate(Date publicUserBirthdate) {
		this.publicUserBirthdate = publicUserBirthdate;
	}
		
}
