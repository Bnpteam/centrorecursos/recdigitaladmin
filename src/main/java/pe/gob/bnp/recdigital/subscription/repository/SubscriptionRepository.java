package pe.gob.bnp.recdigital.subscription.repository;


import pe.gob.bnp.recdigital.subscription.dto.SubscriptionRequest;
import pe.gob.bnp.recdigital.subscription.model.Subscription;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.repository.BaseRepository;

public interface SubscriptionRepository extends BaseRepository<Subscription>{	

	public ResponseTransaction list(SubscriptionRequest subscriptionRequest);
	public ResponseTransaction read(Long id);
}
