package pe.gob.bnp.recdigital.subscription.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.bnp.recdigital.subscription.service.SubscriptionService;
import pe.gob.bnp.recdigital.utilitary.common.ResponseHandler;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class SubscriptionController {


	@Autowired
	ResponseHandler responseHandler;
	
	@Autowired
	SubscriptionService subscriptionService;

	
	/*@RequestMapping(method = RequestMethod.POST,path="/subscription", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "registrar suscripcion", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> save(
			@ApiParam(value = "Estructura JSON de suscripcion publico o privado", required = true)
			@RequestBody SubscriptionDto subscriptionDto) throws Exception {
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = subscriptionService.save(subscriptionDto);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}*/
	
	/*@RequestMapping(method = RequestMethod.PUT,path="/subscription/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "actualizar suscripcion", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> update(
			@ApiParam(value = "Estructura JSON de  suscripcion publico o privado", required = true)
			@PathVariable Long id,@RequestBody SubscriptionDto subscriptionDto) throws Exception {
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = subscriptionService.update(id,subscriptionDto);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}*/
		
	/*@RequestMapping(method = RequestMethod.GET, path="/subscriptions",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar suscripciones", response= SubscriptionResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAll(
			@RequestParam(value = "keyword", defaultValue = "", required=false) String  keyword){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = subscriptionService.searchAll(keyword);
			if (response== null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros."+ ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}*/
	
	/*@RequestMapping(method = RequestMethod.GET, path="/subscription/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "leer suscripcion", response= SubscriptionResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> get(
			@ApiParam(value = "id suscripcion", required = true)
			@PathVariable Long id){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = subscriptionService.get(id);
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}*/	
	
}
