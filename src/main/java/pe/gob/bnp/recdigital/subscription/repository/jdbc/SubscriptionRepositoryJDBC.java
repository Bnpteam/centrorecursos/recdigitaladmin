package pe.gob.bnp.recdigital.subscription.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigital.subscription.dto.SubscriptionRequest;
import pe.gob.bnp.recdigital.subscription.dto.SubscriptionResponse;
import pe.gob.bnp.recdigital.subscription.model.Subscription;
import pe.gob.bnp.recdigital.subscription.repository.SubscriptionRepository;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;
import pe.gob.bnp.recdigital.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class SubscriptionRepositoryJDBC extends BaseJDBCOperation implements SubscriptionRepository{
	
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction persist(Subscription entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_SUBSCRIPTION.SP_PERSIST_SUBSCRIPTION(?,?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setString("P_NAME", Utility.getString(entity.getModeName()));
			cstm.setString("P_DESCRIPTION",Utility.getString(entity.getModeDescription()));
			cstm.setInt("P_ORDER",entity.getSubscriptionOrder());
			cstm.setLong("P_CREATED_USER", Utility.parseStringToLong(entity.getCreatedUser()));			
			cstm.registerOutParameter("S_SUBSCRIPTION_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_SUBSCRIPTION_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction update(Subscription entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_SUBSCRIPTION.SP_UPDATE_SUBSCRIPTION(?,?,?,?,?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_ID", entity.getId());
			cstm.setString("P_NAME", Utility.getString(entity.getModeName()));
			cstm.setString("P_DESCRIPTION",Utility.getString(entity.getModeDescription()));
			cstm.setInt("P_ORDER",entity.getSubscriptionOrder());
			cstm.setString("P_ENABLED",Utility.getString(entity.getIdEnabled()));
			cstm.setLong("P_UPDATED_USER", Utility.parseStringToLong(entity.getUpdatedUser()));			
			cstm.registerOutParameter("S_SUBSCRIPTION_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_SUBSCRIPTION_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction list(SubscriptionRequest subscriptionRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_SUBSCRIPTION.SP_LIST_SUBSCRIPTION(?,?,?)}";
        
        List<Object> subscriptionsResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("P_KEYWORD", Utility.getString(subscriptionRequest.getKeyword()));			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					SubscriptionResponse subscriptionResponse = new SubscriptionResponse();
					subscriptionResponse.setId(Utility.parseLongToString(rs.getLong("SUBSCRIPTION_ID")));
					subscriptionResponse.setName(Utility.getString(rs.getString("SUBSCRIPTION_NAME")));
					subscriptionResponse.setDescription(Utility.getString(rs.getString("SUBSCRIPTION_DESCRIPTION")));
					subscriptionResponse.setOrder(Utility.parseLongToString(rs.getLong("SUBSCRIPTION_ORDER")));
					subscriptionResponse.setEnabled(Utility.getString(rs.getString("SUBSCRIPTION_ENABLED")));
				
					subscriptionsResponse.add(subscriptionResponse);
				}
				response.setList(subscriptionsResponse);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
	@Override
	public ResponseTransaction read(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_SUBSCRIPTION.SP_GET_SUBSCRIPTION(?,?,?)}";
        
        List<Object> subscriptionsResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_SUBSCRIPTION_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					SubscriptionResponse subscriptionResponse = new SubscriptionResponse();
					subscriptionResponse.setId(Utility.parseLongToString(rs.getLong("SUBSCRIPTION_ID")));
					subscriptionResponse.setName(Utility.getString(rs.getString("SUBSCRIPTION_NAME")));
					subscriptionResponse.setDescription(Utility.getString(rs.getString("SUBSCRIPTION_DESCRIPTION")));
					subscriptionResponse.setOrder(Utility.parseLongToString(rs.getLong("SUBSCRIPTION_ORDER")));
					subscriptionResponse.setEnabled(Utility.getString(rs.getString("SUBSCRIPTION_ENABLED")));
				
					subscriptionsResponse.add(subscriptionResponse);
				}	
				response.setList(subscriptionsResponse);					
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

}
