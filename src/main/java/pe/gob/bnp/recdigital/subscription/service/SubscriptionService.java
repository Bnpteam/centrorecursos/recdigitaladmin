package pe.gob.bnp.recdigital.subscription.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.recdigital.subscription.dto.SubscriptionDto;
import pe.gob.bnp.recdigital.subscription.dto.SubscriptionRequest;
import pe.gob.bnp.recdigital.subscription.exception.SubscriptionException;
import pe.gob.bnp.recdigital.subscription.mapper.SubscriptionMapper;
import pe.gob.bnp.recdigital.subscription.model.Subscription;
import pe.gob.bnp.recdigital.subscription.repository.SubscriptionRepository;
import pe.gob.bnp.recdigital.subscription.validation.SubscriptionValidation;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;

@Service
public class SubscriptionService {
	
	@Autowired
	SubscriptionRepository subscriptionRepository;

	@Autowired
	SubscriptionMapper subscriptionMapper;

	public ResponseTransaction save(SubscriptionDto subscriptionDto) {

		Notification notification = SubscriptionValidation.validation(subscriptionDto);
		
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}

		Subscription subscription = subscriptionMapper.reverseMapperSave(subscriptionDto);
		ResponseTransaction response = subscriptionRepository.persist(subscription);
		
		return SubscriptionException.setMessageResponseSave(response);

	}
	
	public ResponseTransaction update(Long id,SubscriptionDto subscriptionDto) {
		
    	Notification notification = SubscriptionValidation.validation(subscriptionDto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}

		Subscription subscription = subscriptionMapper.reverseMapperUpdate(id,subscriptionDto);
		ResponseTransaction response = subscriptionRepository.update(subscription);
		
		return SubscriptionException.setMessageResponseUpdate(response);

	}
	
	public ResponseTransaction searchAll(String keyword){
			SubscriptionRequest subscripcionRequest = new SubscriptionRequest();
			subscripcionRequest.setKeyword(keyword);
			
			Notification notification = SubscriptionValidation.validation(subscripcionRequest);
			if (notification.hasErrors()) {
				throw new IllegalArgumentException(notification.errorMessage());
			}		
			return subscriptionRepository.list(subscripcionRequest);
	}
	
	public ResponseTransaction get(Long id) {
		Notification notification = SubscriptionValidation.validation(id);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		return subscriptionRepository.read(id);
	}

}
