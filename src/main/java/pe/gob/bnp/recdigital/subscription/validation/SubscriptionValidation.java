package pe.gob.bnp.recdigital.subscription.validation;


import pe.gob.bnp.recdigital.subscription.dto.SubscriptionDto;
import pe.gob.bnp.recdigital.subscription.dto.SubscriptionRequest;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class SubscriptionValidation {
	
	private static String messageSubscriptionSave = "No se encontraron datos de suscripcion";
	private static String messageSubscriptionSearchAll = "No se encontró el filtro de búsqueda";
	private static String messageNameSave = "Se debe ingresar el nombre de la suscripcion";
	private static String messageSubscriptionGet = "El id debe ser mayor a cero";

	public static Notification validation(SubscriptionDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageSubscriptionSave);
			return notification;
		}

		if (Utility.isEmptyOrNull(dto.getName())) {
			notification.addError(messageNameSave);
		}

		return notification;
	}

	public static Notification validation(SubscriptionRequest dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageSubscriptionSearchAll);
			return notification;
		}

		return notification;
	}
	
	public static Notification validation(Long id) {
		Notification notification = new Notification();
		if (id <=0L) {
			notification.addError(messageSubscriptionGet);
			return notification;
		}		
		return notification;
	}	

}
