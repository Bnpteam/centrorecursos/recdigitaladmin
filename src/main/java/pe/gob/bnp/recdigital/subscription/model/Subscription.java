package pe.gob.bnp.recdigital.subscription.model;

import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.model.BaseEntity;

public class Subscription extends BaseEntity{
	
	private String modeName;
	private String modeDescription;
	private Integer subscriptionOrder;
	
	public Subscription() {
		super();
		this.modeName = Constants.EMPTY_STRING;;
		this.modeDescription = Constants.EMPTY_STRING;;
		this.subscriptionOrder = Constants.ZERO_INTEGER;;
	}

	public String getModeName() {
		return modeName;
	}

	public void setModeName(String modeName) {
		this.modeName = modeName;
	}

	public String getModeDescription() {
		return modeDescription;
	}

	public void setModeDescription(String modeDescription) {
		this.modeDescription = modeDescription;
	}

	public Integer getSubscriptionOrder() {
		return subscriptionOrder;
	}

	public void setSubscriptionOrder(Integer subscriptionOrder) {
		this.subscriptionOrder = subscriptionOrder;
	}

}
