package pe.gob.bnp.recdigital.subscription.dto;

import pe.gob.bnp.recdigital.subscription.model.Subscription;
import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class SubscriptionDto {
	
	private String id;
	private String name;
	private String description;
	private String order;
	private String enabled;
	private String createdUser;
	private String updatedUser;	

	public SubscriptionDto() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.name = Constants.EMPTY_STRING;
		this.description = Constants.EMPTY_STRING;
		this.order = Constants.EMPTY_STRING;
		this.enabled = Constants.EMPTY_STRING;
		this.createdUser=Constants.EMPTY_STRING;
		this.updatedUser=Constants.EMPTY_STRING;
	}	

	public SubscriptionDto(Subscription subscription) {
		super();
		this.id =Utility.parseLongToString(subscription.getId());
		this.name = subscription.getModeName();
		this.description = subscription.getModeDescription();
		this.order = Utility.parseIntToString(subscription.getSubscriptionOrder());
		this.enabled = subscription.getIdEnabled();
		this.createdUser=subscription.getCreatedUser();
		this.updatedUser=subscription.getUpdatedUser();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

}
