package pe.gob.bnp.recdigital.subscription.dto;

public class SubscriptionRequest {
	
	private String keyword;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

}
