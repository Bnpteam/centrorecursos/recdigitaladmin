package pe.gob.bnp.recdigital.subscription.mapper;

import org.springframework.stereotype.Component;

import pe.gob.bnp.recdigital.subscription.dto.SubscriptionDto;
import pe.gob.bnp.recdigital.subscription.model.Subscription;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

@Component
public class SubscriptionMapper {
	
	public Subscription reverseMapperSave(SubscriptionDto subscriptionDto) {
		Subscription subscription = new Subscription();	
		
		subscription.setModeName(subscriptionDto.getName());		
		subscription.setModeDescription(subscriptionDto.getDescription());
		subscription.setSubscriptionOrder(Utility.parseStringToInt(subscriptionDto.getOrder()));
		subscription.setCreatedUser(subscriptionDto.getCreatedUser());
		return subscription;
	}
	
	public Subscription reverseMapperUpdate(Long id,SubscriptionDto subscriptionDto) {
		Subscription subscription = new Subscription();
		
		subscriptionDto.setId(Utility.parseLongToString(id));
		subscription.setId(Utility.parseStringToLong(subscriptionDto.getId()));
		subscription.setModeName(subscriptionDto.getName());
		subscription.setModeDescription(subscriptionDto.getDescription());
		subscription.setSubscriptionOrder(Utility.parseStringToInt(subscriptionDto.getOrder()));
		subscription.setIdEnabled(subscriptionDto.getEnabled());	
		subscription.setUpdatedUser(subscriptionDto.getUpdatedUser());
		return subscription;
	}

}
