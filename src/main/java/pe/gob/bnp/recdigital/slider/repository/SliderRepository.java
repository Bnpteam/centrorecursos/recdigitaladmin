package pe.gob.bnp.recdigital.slider.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.recdigital.slider.model.Button;
import pe.gob.bnp.recdigital.slider.dto.SliderDetailResponse;
import pe.gob.bnp.recdigital.slider.dto.SliderRequest;
import pe.gob.bnp.recdigital.slider.model.Slider;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.repository.BaseRepository;


@Repository
public interface SliderRepository extends BaseRepository<Slider>{	

	public ResponseTransaction list(SliderRequest sliderRequest);
	public SliderDetailResponse readList(Long id);
	public ResponseTransaction read(Long id);
	public ResponseTransaction persistDetail(Long id,Button button);
}
