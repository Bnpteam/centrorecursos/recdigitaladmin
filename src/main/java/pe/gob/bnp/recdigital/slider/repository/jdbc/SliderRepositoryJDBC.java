package pe.gob.bnp.recdigital.slider.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigital.slider.dto.SliderDetailResponse;
import pe.gob.bnp.recdigital.slider.dto.SliderRequest;
import pe.gob.bnp.recdigital.slider.model.Slider;
import pe.gob.bnp.recdigital.slider.repository.SliderRepository;
import pe.gob.bnp.recdigital.slider.dto.ButtonDetailResponse;
import pe.gob.bnp.recdigital.slider.model.Button;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;
import pe.gob.bnp.recdigital.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class SliderRepositoryJDBC extends BaseJDBCOperation implements SliderRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(SliderRepositoryJDBC.class);
	
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction persist(Slider entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_SLIDER.SP_PERSIST_SLIDER(?,?,?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setString("P_TITLE", Utility.getString(entity.getTitle()));
			cstm.setString("P_SUBTITLE",Utility.getString(entity.getSubtitle()));
			cstm.setString("P_DESCRIPTION",Utility.getString(entity.getSliderDescription()));
			cstm.setInt("P_ORDER",entity.getSliderOrder());
			cstm.setLong("P_CREATED_USER", Utility.parseStringToLong(entity.getCreatedUser()));			
			cstm.registerOutParameter("S_SLIDER_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_SLIDER_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error("PKG_API_SLIDER.SP_PERSIST_SLIDER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
	@Override
	public ResponseTransaction persistDetail(Long idSlider, Button button) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_SLIDER.SP_PERSIST_BUTTON(?,?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setLong("P_SLIDER_ID", idSlider);
			cstm.setString("P_NAME", Utility.getString(button.getButtonName()));
			cstm.setString("P_URL", Utility.getString(button.getUrlRoute()));
			cstm.setInt("P_ORDER",button.getButtonOrder());				
			cstm.registerOutParameter("S_BUTTON_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");		
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_BUTTON_ID"));			
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();
				System.out.println("Error: No se pudo insertar el registro detalle del curso");
				response.setCodeResponse("0007");		
				response.setResponse("Error: No se pudo insertar el registro detalle del curso");
			}
		}catch(SQLException e) {
			logger.error("PKG_API_SLIDER.SP_PERSIST_BUTTON: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			System.out.println(e);
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}


	@Override
	public ResponseTransaction update(Slider entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_SLIDER.SP_UPDATE_SLIDER(?,?,?,?,?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_ID", entity.getId());
			cstm.setString("P_TITLE", Utility.getString(entity.getTitle()));
			cstm.setString("P_SUBTITLE",Utility.getString(entity.getSubtitle()));
			cstm.setString("P_DESCRIPTION",Utility.getString(entity.getSliderDescription()));
			cstm.setInt("P_ORDER",entity.getSliderOrder());
			cstm.setString("P_ENABLED",Utility.getString(entity.getIdEnabled()));
			cstm.setLong("P_UPDATED_USER", Utility.parseStringToLong(entity.getUpdatedUser()));			
			cstm.registerOutParameter("S_SLIDER_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_SLIDER_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error("PKG_API_SLIDER.SP_UPDATE_SLIDER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	
	@Override
	public ResponseTransaction list(SliderRequest sliderRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_SLIDER.SP_LIST_SLIDER(?,?,?)}";
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("P_KEYWORD", Utility.getString(sliderRequest.getKeyword()));			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {					
				//	ResponseTransaction categoryResponse=   this.read(rs.getLong("SLIDER_ID"));
					slidersResponse.add( this.readList(rs.getLong("SLIDER_ID")));
				}	
				response.setList(slidersResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_SLIDER.SP_LIST_SLIDER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	@Override
	public SliderDetailResponse readList(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_SLIDER.SP_GET_SLIDER(?,?,?,?)}";
        SliderDetailResponse sliderResponse = new SliderDetailResponse();      
        ResponseTransaction response = new ResponseTransaction();
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_SLIDER_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);		
			cstm.registerOutParameter("S_C_CURSOR_BUTTON",OracleTypes.CURSOR);		
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					sliderResponse.setId(Utility.parseLongToString(rs.getLong("SLIDER_ID")));
					sliderResponse.setTitle(Utility.getString(rs.getString("SLIDER_TITLE")));
					sliderResponse.setSubtitle(Utility.getString(rs.getString("SLIDER_SUBTITLE")));
					sliderResponse.setDescription(Utility.getString(rs.getString("SLIDER_DESCRIPTION")));
					sliderResponse.setOrder(Utility.parseLongToString(rs.getLong("SLIDER_ORDER")));
					sliderResponse.setEnabled(Utility.getString(rs.getString("SLIDER_ENABLED")));
					sliderResponse.setCreatedUser(Utility.getString(rs.getString("CREATED_USER")));
					sliderResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
					
				}	
				this.closeResultSet(rs);	
				
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_BUTTON");
				List<ButtonDetailResponse> buttonDto = new ArrayList<>();
				while (rs.next()) {
					ButtonDetailResponse entidad = new ButtonDetailResponse();
					entidad.setId(Utility.parseLongToString(rs.getLong("BUTTON_ID")));								
					entidad.setName(Utility.getString(rs.getString("BUTTON_NAME")));
					entidad.setUrl(Utility.getString(rs.getString("BUTTON_URL")));					
					entidad.setOrder(Utility.parseLongToString(rs.getLong("BUTTON_ORDER")));					
					buttonDto.add(entidad);
				}	
				sliderResponse.setButtons(buttonDto);				
				
			}
		}catch(SQLException e) {
			logger.error("List PKG_API_SLIDER.SP_GET_SLIDER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return sliderResponse;	
	}
	
	@Override
	public ResponseTransaction read(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_SLIDER.SP_GET_SLIDER(?,?,?,?)}";
        SliderDetailResponse sliderResponse = new SliderDetailResponse();
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_SLIDER_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);		
			cstm.registerOutParameter("S_C_CURSOR_BUTTON",OracleTypes.CURSOR);		
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					sliderResponse.setId(Utility.parseLongToString(rs.getLong("SLIDER_ID")));
					sliderResponse.setTitle(Utility.getString(rs.getString("SLIDER_TITLE")));
					sliderResponse.setSubtitle(Utility.getString(rs.getString("SLIDER_SUBTITLE")));
					sliderResponse.setDescription(Utility.getString(rs.getString("SLIDER_DESCRIPTION")));
					sliderResponse.setOrder(Utility.parseLongToString(rs.getLong("SLIDER_ORDER")));
					sliderResponse.setEnabled(Utility.getString(rs.getString("SLIDER_ENABLED")));
					sliderResponse.setCreatedUser(Utility.getString(rs.getString("CREATED_USER")));
					sliderResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
				}	
				this.closeResultSet(rs);	
				
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_BUTTON");
				List<ButtonDetailResponse> buttonDto = new ArrayList<>();
				while (rs.next()) {
					ButtonDetailResponse entidad = new ButtonDetailResponse();
					entidad.setId(Utility.parseLongToString(rs.getLong("BUTTON_ID")));								
					entidad.setName(Utility.getString(rs.getString("BUTTON_NAME")));
					entidad.setUrl(Utility.getString(rs.getString("BUTTON_URL")));					
					entidad.setOrder(Utility.parseLongToString(rs.getLong("BUTTON_ORDER")));					
					buttonDto.add(entidad);
				}	
				sliderResponse.setButtons(buttonDto);
				
				if(sliderResponse!=null && !Utility.isEmptyOrNull(sliderResponse.getTitle()) ) {
					slidersResponse.add(sliderResponse);
					
				}
				response.setList(slidersResponse);	
			}
		}catch(SQLException e) {
			logger.error("PKG_API_SLIDER.SP_GET_SLIDER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
}
