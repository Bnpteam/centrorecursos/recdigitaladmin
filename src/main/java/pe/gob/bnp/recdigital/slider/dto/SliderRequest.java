package pe.gob.bnp.recdigital.slider.dto;

public class SliderRequest {

	private String keyword;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

}
