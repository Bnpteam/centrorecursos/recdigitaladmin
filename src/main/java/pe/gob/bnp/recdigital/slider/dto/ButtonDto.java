package pe.gob.bnp.recdigital.slider.dto;

import pe.gob.bnp.recdigital.slider.model.Button;
import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class ButtonDto {
	
	private String name;
	private String url;
	private String order;
	
	public ButtonDto() {
		super();
		this.name = Constants.EMPTY_STRING;
		this.url = Constants.EMPTY_STRING;
		this.order = Constants.EMPTY_STRING;
		
	}	

	public ButtonDto(Button button) {
		super();		
		this.name = button.getButtonName();	
		this.url = button.getUrlRoute();
		this.order = Utility.parseIntToString(button.getButtonOrder());	
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
