package pe.gob.bnp.recdigital.slider.validation;

import pe.gob.bnp.recdigital.slider.dto.SliderDto;
import pe.gob.bnp.recdigital.slider.dto.SliderRequest;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class SliderValidation {

	private static String messageSliderSave = "No se encontraron datos del Slider";
	private static String messageSliderSearchAll = "No se encontró el filtro de búsqueda";
	private static String messageTitleSave = "Se debe ingresar el título del Slider";
	private static String messageSliderGet = "El id debe ser mayor a cero";

	public static Notification validation(SliderDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageSliderSave);
			return notification;
		}

		if (Utility.isEmptyOrNull(dto.getTitle())) {
			notification.addError(messageTitleSave);
		}

		return notification;
	}

	public static Notification validation(SliderRequest dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageSliderSearchAll);
			return notification;
		}

		return notification;
	}
	
	public static Notification validation(Long id) {
		Notification notification = new Notification();
		if (id <=0L) {
			notification.addError(messageSliderGet);
			return notification;
		}		
		return notification;
	}	

}
