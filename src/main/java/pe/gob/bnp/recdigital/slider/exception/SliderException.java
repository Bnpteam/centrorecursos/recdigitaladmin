package pe.gob.bnp.recdigital.slider.exception;

import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;

public class SliderException {
	
	private static String error9999 ="Error: No se ejecutó ninguna transacción.";
	private static String error0001 ="Error: El title del Slider ya se encuentra registrado.";
	private static String error0002 ="Error: No se puede realizar la actualización: El id del Slider no pertenece a ningun slider registrado.";


	public static ResponseTransaction setMessageResponseSave(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0001")) {
			response.setResponse(error0001);
		}

		return response;
	}

	public static ResponseTransaction setMessageResponseUpdate(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0001")) {
			response.setResponse(error0001);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}

		return response;
	}

}
