package pe.gob.bnp.recdigital.slider.model;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.recdigital.slider.model.Button;
import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.model.BaseEntity;

public class Slider extends BaseEntity{
	
	private String title;
	private String subtitle;
	private String sliderDescription;
	private Integer sliderOrder;
	private List<Button> buttons;
	
	public List<Button> getButtons() {
		return buttons;
	}

	public void setButtons(List<Button> buttons) {
		this.buttons = buttons;
	}

	public Slider() {
		super();
		this.title = Constants.EMPTY_STRING;
		this.subtitle = Constants.EMPTY_STRING;
		this.sliderDescription = Constants.EMPTY_STRING;
		this.sliderOrder = Constants.ZERO_INTEGER;
		this.buttons = new ArrayList<>();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getSliderDescription() {
		return sliderDescription;
	}

	public void setSliderDescription(String sliderDescription) {
		this.sliderDescription = sliderDescription;
	}

	public Integer getSliderOrder() {
		return sliderOrder;
	}

	public void setSliderOrder(Integer sliderOrder) {
		this.sliderOrder = sliderOrder;
	}	

}
