package pe.gob.bnp.recdigital.slider.dto;

public class ButtonDetailResponse {
	private String id;
	private String name;
	private String url;	
	private String order;
	/*private String icon;
	private String urlRoute;
	private String urlName;
	
	private String enabled;*/
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	

	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
