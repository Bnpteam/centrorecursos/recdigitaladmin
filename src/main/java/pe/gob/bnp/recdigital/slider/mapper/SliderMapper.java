package pe.gob.bnp.recdigital.slider.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import pe.gob.bnp.recdigital.slider.model.Button;
import pe.gob.bnp.recdigital.slider.dto.ButtonDto;
import pe.gob.bnp.recdigital.slider.dto.SliderDto;
import pe.gob.bnp.recdigital.slider.model.Slider;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

@Component
public class SliderMapper {
	
	public Slider reverseMapperSave(SliderDto sliderDto) {
		Slider slider = new Slider();	
		
		slider.setTitle(sliderDto.getTitle());;
		slider.setSubtitle(sliderDto.getSubtitle());
		slider.setSliderDescription(sliderDto.getDescription());
		slider.setSliderOrder(Utility.parseStringToInt(sliderDto.getOrder()));
		slider.setCreatedUser(sliderDto.getCreatedUser());
		slider.setButtons(reverseMapperList(sliderDto.getButtons()));
		return slider;
	}
	
	public Slider reverseMapperUpdate(Long id,SliderDto sliderDto) {
		Slider slider = new Slider();
		
		sliderDto.setId(Utility.parseLongToString(id));
		slider.setId(Utility.parseStringToLong(sliderDto.getId()));
		slider.setTitle(sliderDto.getTitle());;
		slider.setSubtitle(sliderDto.getSubtitle());
		slider.setSliderDescription(sliderDto.getDescription());
		slider.setSliderOrder(Utility.parseStringToInt(sliderDto.getOrder()));
		slider.setIdEnabled(sliderDto.getEnabled());	
		slider.setUpdatedUser(sliderDto.getUpdatedUser());
		slider.setButtons(reverseMapperList(sliderDto.getButtons()));
		return slider;
	}
	
	private List<Button> reverseMapperList(List<ButtonDto> detailsDto){
		List<Button> details=new ArrayList<>();	
		
		for(ButtonDto detail:detailsDto) {
			Button resourceDetail= new Button();
			resourceDetail.setButtonName(detail.getName());
			resourceDetail.setUrlRoute(detail.getUrl());
			resourceDetail.setButtonOrder(Utility.parseStringToInt(detail.getOrder()));
			details.add(resourceDetail);
		}		
		
		return details;		
	}

}
