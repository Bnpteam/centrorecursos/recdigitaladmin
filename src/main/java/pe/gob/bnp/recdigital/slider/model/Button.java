package pe.gob.bnp.recdigital.slider.model;

import pe.gob.bnp.recdigital.slider.model.Slider;
import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.model.BaseEntity;

public class Button extends BaseEntity{
	
	private String buttonName;
	private String buttonDescription;
	private String icon;
	private String urlRoute;
	private String urlName;
	private Integer buttonOrder;
	private Slider slider;

	public Button() {
		super();
		this.buttonName = Constants.EMPTY_STRING;;
		this.buttonDescription = Constants.EMPTY_STRING;;
		this.icon = Constants.EMPTY_STRING;;
		this.urlRoute = Constants.EMPTY_STRING;
		this.urlName=Constants.EMPTY_STRING;
		this.buttonOrder=Constants.ZERO_INTEGER;
		this.slider=new Slider();
	}

	public String getButtonName() {
		return buttonName;
	}

	public void setButtonName(String buttonName) {
		this.buttonName = buttonName;
	}

	public String getButtonDescription() {
		return buttonDescription;
	}

	public void setButtonDescription(String buttonDescription) {
		this.buttonDescription = buttonDescription;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getUrlRoute() {
		return urlRoute;
	}

	public void setUrlRoute(String urlRoute) {
		this.urlRoute = urlRoute;
	}

	public String getUrlName() {
		return urlName;
	}

	public void setUrlName(String urlName) {
		this.urlName = urlName;
	}

	public Integer getButtonOrder() {
		return buttonOrder;
	}

	public void setButtonOrder(Integer buttonOrder) {
		this.buttonOrder = buttonOrder;
	}
	
	public Slider getSlider() {
		return slider;
	}

	public void setSlider(Slider slider) {
		this.slider = slider;
	}
}
