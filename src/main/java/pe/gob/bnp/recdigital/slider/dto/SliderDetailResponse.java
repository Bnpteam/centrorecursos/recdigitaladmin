package pe.gob.bnp.recdigital.slider.dto;

import java.util.List;

import pe.gob.bnp.recdigital.slider.dto.ButtonDetailResponse;

public class SliderDetailResponse {
	
	private String id;
	private String title;
	private String subtitle;
	private String description;
	private String order;
	private String enabled;	
	private String createdUser;
	private String createdDate;
	private List<ButtonDetailResponse> buttons;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	
	public String getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	
	public List<ButtonDetailResponse> getButtons() {
		return buttons;
	}
	public void setButtons(List<ButtonDetailResponse> buttons) {
		this.buttons = buttons;
	}
	
}
