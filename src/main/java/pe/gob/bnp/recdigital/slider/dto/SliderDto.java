package pe.gob.bnp.recdigital.slider.dto;

import java.util.ArrayList;
import java.util.List;
import pe.gob.bnp.recdigital.slider.model.Slider;
import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class SliderDto{

	private String id;
	private String title;
	private String subtitle;
	private String description;
	private String order;
	private String enabled;
	private String createdUser;
	private String updatedUser;	
	private List<ButtonDto> buttons;	

	public SliderDto() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.title = Constants.EMPTY_STRING;
		this.subtitle = Constants.EMPTY_STRING;
		this.description = Constants.EMPTY_STRING;
		this.order = Constants.EMPTY_STRING;
		this.enabled = Constants.EMPTY_STRING;
		this.createdUser=Constants.EMPTY_STRING;
		this.updatedUser=Constants.EMPTY_STRING;
		this.buttons=new ArrayList<>();
	}	

	public SliderDto(Slider slider) {
		super();
		this.id =Utility.parseLongToString(slider.getId());
		this.title = slider.getTitle();
		this.subtitle = slider.getSubtitle();
		this.description = slider.getSliderDescription();
		this.order = Utility.parseIntToString(slider.getSliderOrder());
		this.enabled = slider.getIdEnabled();
		this.createdUser=slider.getCreatedUser();
		this.updatedUser=slider.getUpdatedUser();
		this.buttons=new ArrayList<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	
	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}
	
	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}
	
	public List<ButtonDto> getButtons() {
		return buttons;
	}

	public void setButtons(List<ButtonDto> buttons) {
		this.buttons = buttons;
	}
	
}
