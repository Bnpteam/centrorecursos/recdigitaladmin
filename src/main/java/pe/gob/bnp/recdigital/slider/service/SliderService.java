package pe.gob.bnp.recdigital.slider.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.recdigital.slider.model.Button;
import pe.gob.bnp.recdigital.slider.dto.SliderDto;
import pe.gob.bnp.recdigital.slider.dto.SliderRequest;
import pe.gob.bnp.recdigital.slider.exception.SliderException;
import pe.gob.bnp.recdigital.slider.mapper.SliderMapper;
import pe.gob.bnp.recdigital.slider.model.Slider;
import pe.gob.bnp.recdigital.slider.repository.SliderRepository;
import pe.gob.bnp.recdigital.slider.validation.SliderValidation;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

@Service
public class SliderService {

	private String codeSuccess = "0000";

	@Autowired
	SliderRepository sliderRepository;

	@Autowired
	SliderMapper sliderMapper;

	public ResponseTransaction save(SliderDto sliderDto) {
		ResponseTransaction response = new ResponseTransaction();

		Notification notification = SliderValidation.validation(sliderDto);
		
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		Slider slider = sliderMapper.reverseMapperSave(sliderDto);
		response = sliderRepository.persist(slider);
		
		if (response!= null && response.getCodeResponse()!=null && response.getCodeResponse().equals(codeSuccess)){
			String idSlider=response.getId();
			for (Button  buttonDetail: slider.getButtons()) {
				sliderRepository.persistDetail(Utility.parseStringToLong(idSlider),buttonDetail);			  
			}
		}	
		
		return SliderException.setMessageResponseSave(response);

	}
	
	public ResponseTransaction update(Long id,SliderDto sliderDto) {
		
		ResponseTransaction response = new ResponseTransaction();
		
    	Notification notification = SliderValidation.validation(sliderDto);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		Slider slider = sliderMapper.reverseMapperUpdate(id,sliderDto);
		response = sliderRepository.update(slider);
		
		if (response!= null && response.getCodeResponse()!=null && response.getCodeResponse().equals(codeSuccess)){
			String idSlider=response.getId();
			for (Button  buttonDetail: slider.getButtons()) {
				sliderRepository.persistDetail(Utility.parseStringToLong(idSlider),buttonDetail);			  
			}
		}			
		
		return SliderException.setMessageResponseUpdate(response);

	}
	
	public ResponseTransaction searchAll(String keyword){
			SliderRequest sliderRequest = new SliderRequest();
			sliderRequest.setKeyword(keyword);
			
			Notification notification = SliderValidation.validation(sliderRequest);
			if (notification.hasErrors()) {
				throw new IllegalArgumentException(notification.errorMessage());
			}		
			return sliderRepository.list(sliderRequest);
	}
	
	public ResponseTransaction get(Long id) {
		ResponseTransaction response = new ResponseTransaction();
		Notification notification = SliderValidation.validation(id);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}
		return sliderRepository.read(id);
	}

}
