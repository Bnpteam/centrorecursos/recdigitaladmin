package pe.gob.bnp.recdigital.publication.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.bnp.recdigital.publication.dto.PublicationRequest;
import pe.gob.bnp.recdigital.publication.dto.PublishedDto;
import pe.gob.bnp.recdigital.publication.exception.PublicationException;
import pe.gob.bnp.recdigital.publication.repository.PublicationRepository;
import pe.gob.bnp.recdigital.publication.validation.PublicationValidation;
import pe.gob.bnp.recdigital.resource.mapper.ResourceMapper;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

@Service
public class PublicationService {
	
	//private String codeSuccess = "0000";

	@Autowired
	PublicationRepository publicationRepository;	

	@Autowired
	ResourceMapper resourceMapper;	
	
	public ResponseTransaction searchAll(String keyword, String type,String state,String page, String size) {
		ResponseTransaction response = new ResponseTransaction();
		
		PublicationRequest publicationRequest = new PublicationRequest();
		publicationRequest.setKeyword(keyword);
		publicationRequest.setType(type);
		publicationRequest.setState(state);
		publicationRequest.setPage(page);
		publicationRequest.setSize(size);

		Notification notification = PublicationValidation.validation(publicationRequest);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());			
			return response;
		}					

		return publicationRepository.list(publicationRequest);
	}
	
	public ResponseTransaction published(Long id,PublishedDto userDto) {
		
    	/*Notification notification = UserValidation.validation(userDto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}*/

		//User user = userMapper.reverseMapperUpdate(id,userDto);
		
		if(Utility.isEmptyOrNull(userDto.getCreatedUser()))userDto.setCreatedUser("0");
		
		ResponseTransaction response = publicationRepository.published(id,Utility.parseStringToLong(userDto.getCreatedUser()));
		
		return PublicationException.setMessageResponsepublished(response);

	}
	
  public ResponseTransaction unpublished(Long id,PublishedDto userDto) {
		
    	/*Notification notification = UserValidation.validation(userDto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}*/

		//User user = userMapper.reverseMapperUpdate(id,userDto);
	
	if(Utility.isEmptyOrNull(userDto.getCreatedUser()))userDto.setCreatedUser("0");
	
		ResponseTransaction response = publicationRepository.unpublished(id,Utility.parseStringToLong(userDto.getCreatedUser()));
		
		
		return PublicationException.setMessageResponseUnpublished(response);

	}
	

}
