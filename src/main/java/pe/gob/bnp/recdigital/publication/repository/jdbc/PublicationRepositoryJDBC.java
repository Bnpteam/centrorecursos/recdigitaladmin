package pe.gob.bnp.recdigital.publication.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigital.publication.dto.PublicationRequest;
import pe.gob.bnp.recdigital.publication.dto.PublicationResponse;
import pe.gob.bnp.recdigital.publication.repository.PublicationRepository;
import pe.gob.bnp.recdigital.resource.model.Resource;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;
import pe.gob.bnp.recdigital.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class PublicationRepositoryJDBC  extends BaseJDBCOperation implements PublicationRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(PublicationRepositoryJDBC.class);
	
	private String messageSuccess="Transacción exitosa.";
	
	@Override
	public ResponseTransaction list(PublicationRequest resourceRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_RESOURCE.SP_LIST_PUBLICATION(?,?,?,?,?,?,?)}";/*7*/
        
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setString("P_KEYWORD", Utility.getString(resourceRequest.getKeyword()));	
			cstm.setString("P_TYPE", Utility.getString(resourceRequest.getType()));	
			cstm.setString("P_STATE", Utility.getString(resourceRequest.getState()));	
			cstm.setInt("P_PAGE", Utility.parseStringToInt(resourceRequest.getPage()));	
			cstm.setInt("P_SIZE", Utility.parseStringToInt(resourceRequest.getSize()));	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					PublicationResponse resourceResponse = new PublicationResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("RESOURCE_ID")));
					resourceResponse.setTitle(Utility.getString(rs.getString("RESOURCE_TITLE")));
					resourceResponse.setDescription(Utility.getString(rs.getString("RESOURCE_DESCRIPTION")));
				//	resourceResponse.setCategoryId(Utility.getString(rs.getString("CATEGORY_ID")));
				//	resourceResponse.setSubcategoryId(Utility.getString(rs.getString("SUBCATEGORY_ID")));
					resourceResponse.setCategory(Utility.getString(rs.getString("RESOURCE_CATEGORY")));
					resourceResponse.setTypeId(Utility.getString(rs.getString("RESOURCE_TYPE_ID")));
					resourceResponse.setType(Utility.getString(rs.getString("RESOURCE_TYPE")));
					resourceResponse.setStateId(Utility.getString(rs.getString("RESOURCE_STATE_ID")));
					resourceResponse.setState(Utility.getString(rs.getString("RESOURCE_STATE")));
					resourceResponse.setLinkCar(Utility.getString(rs.getString("CAR_LINK")));
					resourceResponse.setCreatedUser(Utility.getString(rs.getString("CREATED_USER")));
					resourceResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
					resourceResponse.setTotalRecords(Utility.getString(rs.getString("TOTAL_RECORDS")));
				
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_RESOURCE.SP_LIST_PUBLICATION: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTransaction persist(Resource entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTransaction update(Resource entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTransaction published(Long idResource, Long createdUser) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_RESOURCE.SP_PUBLISHED(?,?,?,?)}";/*4*/
        ResponseTransaction response = new ResponseTransaction();       
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_ID", idResource);
			cstm.setLong("P_CREATED_USER",createdUser);			
			cstm.registerOutParameter("S_RESOURSE_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);		
			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_RESOURSE_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error("PKG_API_RESOURCE.SP_PUBLISHED: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction unpublished(Long idResource, Long createdUser) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_RESOURCE.SP_UNPUBLISHED(?,?,?,?)}";/*4*/
        ResponseTransaction response = new ResponseTransaction();       
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_ID", idResource);
			cstm.setLong("P_CREATED_USER",createdUser);			
			cstm.registerOutParameter("S_RESOURSE_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);		
			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_RESOURSE_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error("PKG_API_RESOURCE.SP_UNPUBLISHED: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

}
