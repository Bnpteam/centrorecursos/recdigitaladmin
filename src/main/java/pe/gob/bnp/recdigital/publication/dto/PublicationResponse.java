package pe.gob.bnp.recdigital.publication.dto;

import pe.gob.bnp.recdigital.utilitary.common.Constants;

public class PublicationResponse {
	
	private String id;
	private String title;
	private String description;
	//private String categoryId;
	//private String subcategoryId;
	private String category;
	private String typeId;
	private String type;
	private String stateId;	
	private String state;
	private String linkCar;
	private String createdUser;
	private String createdDate;
	private String totalRecords;
	

	public String getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(String totalRecords) {
		this.totalRecords = totalRecords;
	}

	public PublicationResponse() {
		super();
		this.id=Constants.EMPTY_STRING;
		this.title=Constants.EMPTY_STRING;
		this.description=Constants.EMPTY_STRING;
		//this.categoryId=Constants.EMPTY_STRING;
		//this.subcategoryId=Constants.EMPTY_STRING;		
		this.category=Constants.EMPTY_STRING;
		this.typeId=Constants.EMPTY_STRING;
		this.type=Constants.EMPTY_STRING;
		this.stateId=Constants.EMPTY_STRING;		
		this.state=Constants.EMPTY_STRING;
		this.linkCar=Constants.EMPTY_STRING;
		this.createdUser=Constants.EMPTY_STRING;
		this.createdDate=Constants.EMPTY_STRING;	
		this.totalRecords=Constants.EMPTY_STRING;	
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getSubcategoryId() {
		return subcategoryId;
	}
	
	public void setSubcategoryId(String subcategoryId) {
		this.subcategoryId = subcategoryId;
	}*/

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getLinkCar() {
		return linkCar;
	}

	public void setLinkCar(String linkCar) {
		this.linkCar = linkCar;
	}
	
	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

}
