package pe.gob.bnp.recdigital.publication.exception;

import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;

public class PublicationException {

	public static final  String error9999 ="Error 9999: No se ejecutó ninguna transacción.";
	public static final  String error0001 ="Error 0001: No se puede publicar porque la imagen del car esta vacia.";
	public static final  String error0003 ="Error 0003: No se puede publicar porque ya esta publicado.";
	public static final  String error0004 ="Error 0004: No se puede realizar la publicacion: El id del recurso o curso no existe.";
	
	public static final  String errorUn0001 ="Error 0001: No se puede despublicar porque la imagen del car esta vacia.";
	public static final  String errorUn0003 ="Error 0003: No se puede despublicar porque ya esta despublicado.";
	public static final  String errorUn0004 ="Error 0004: No se puede realizar la despublicacion: El id del recurso o curso no existe.";


	public static ResponseTransaction setMessageResponsepublished(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0001")) {
			response.setResponse(error0001);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003);
		}

		if (response.getCodeResponse().equals("0004")) {
			response.setResponse(error0004);
		}

		return response;
	}

	public static ResponseTransaction setMessageResponseUnpublished(ResponseTransaction response) {
		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0001")) {
			response.setResponse(errorUn0001);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(errorUn0003);
		}

		if (response.getCodeResponse().equals("0004")) {
			response.setResponse(errorUn0004);
		}

		return response;
	}

}
