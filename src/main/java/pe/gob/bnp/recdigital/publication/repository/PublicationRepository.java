package pe.gob.bnp.recdigital.publication.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.recdigital.publication.dto.PublicationRequest;
import pe.gob.bnp.recdigital.resource.model.Resource;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.repository.BaseRepository;

@Repository
public interface PublicationRepository extends BaseRepository<Resource>{
	
	public ResponseTransaction list(PublicationRequest publicationRequest);
	
	public ResponseTransaction published(Long id,Long createdUser);
	
	public ResponseTransaction unpublished(Long id,Long createdUser);

}
