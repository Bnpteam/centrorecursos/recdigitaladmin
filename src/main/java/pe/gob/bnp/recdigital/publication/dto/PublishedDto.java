package pe.gob.bnp.recdigital.publication.dto;

public class PublishedDto {
	
	private String createdUser;

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

}
