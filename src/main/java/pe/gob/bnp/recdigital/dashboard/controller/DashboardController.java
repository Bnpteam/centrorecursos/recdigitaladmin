package pe.gob.bnp.recdigital.dashboard.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.recdigital.admin.dto.DashboardResponse;
import pe.gob.bnp.recdigital.admin.service.AuthService;
import pe.gob.bnp.recdigital.utilitary.common.ResponseHandler;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.exception.EntityNotFoundResultException;


@RestController
@RequestMapping("/api")
@Api(value="/api/dashboard")
@CrossOrigin(origins = "*")
public class DashboardController {
	
	@Autowired
	ResponseHandler responseHandler;
	
	@Autowired
	AuthService authService;
	
	
	@RequestMapping(method = RequestMethod.GET, path="/dashboard",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "menu principal BNP", response= DashboardResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAll(){
		ResponseTransaction response=new ResponseTransaction();
		try {
			response = authService.searchDashboard();
			if (response == null || response.getList().size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");//);
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros."+ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}

}
