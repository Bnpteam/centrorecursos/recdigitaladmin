package pe.gob.bnp.recdigital.resource.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Base64;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import pe.gob.bnp.recdigital.config.shared.dto.SharedResponse;
import pe.gob.bnp.recdigital.config.shared.repository.SharedRepository;
import pe.gob.bnp.recdigital.resource.dto.CourseDto;
import pe.gob.bnp.recdigital.resource.dto.CourseDetailDto;
import pe.gob.bnp.recdigital.resource.dto.ResourceRequest;
import pe.gob.bnp.recdigital.resource.exception.CourseException;
import pe.gob.bnp.recdigital.resource.mapper.ResourceMapper;
import pe.gob.bnp.recdigital.resource.model.Resource;
import pe.gob.bnp.recdigital.resource.model.ResourceDetail;
import pe.gob.bnp.recdigital.resource.repository.CourseRepository;
import pe.gob.bnp.recdigital.resource.validation.CourseValidation;
import pe.gob.bnp.recdigital.resource.validation.ResourceValidation;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

@Service
public class CourseService {
	
	private static final Logger logger = LoggerFactory.getLogger(CourseService.class);
	
	private String codeSuccess = "0000";

	@Autowired
	CourseRepository courseRepository;

	@Autowired
	SharedRepository sharedRepository;

	@Autowired
	ResourceMapper resourceMapper;
	
	private SharedResponse sharedConfig;
	
	private String routeFile="",nameFileCar="",nameFileBanner="",extensionFile="";
	
	private String nameServerCar="imageCar",nameServerBanner="imageBanner";
	
	private void readConfig() {
		try {
			ResponseTransaction response = new ResponseTransaction();		
			// leer configuracion
			response = sharedRepository.read();
			
			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess)) {
				List<Object> sharedList = response.getList();
				if (!sharedList.isEmpty()) {
					Object sharedObject = sharedList.get(0);
					if (sharedObject instanceof SharedResponse) {
						sharedConfig = (SharedResponse) sharedObject;
						//System.out.println("entro postcontructor.");
					}				
				}

			}	
			
		}catch(Exception ex) {
			System.out.println("error postconstructor:"+ex);			
		}	
	}
	
	public ResponseTransaction save(CourseDto courseDto) {

		routeFile="";
		nameFileCar="";
		nameFileBanner="";
		extensionFile="";
		
		ResponseTransaction response = new ResponseTransaction();

		Notification notification = CourseValidation.validation(courseDto);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}
		
		readConfig();
		
		if (sharedConfig==null) {
			response.setCodeResponse("0090");
			response.setResponse("Error al leer la configuracion del servidor compartido");
			return response;
		}
		
		byte[] fileLinkCar = Base64.getDecoder().decode(courseDto.getLinkCar());
		
		byte[] fileLinkBanner = Base64.getDecoder().decode(courseDto.getLinkBanner());

		Resource resource = resourceMapper.reverseMapperCourseSave(courseDto);
		response = courseRepository.persist(resource);
		
		if (response!= null && response.getCodeResponse()!=null && response.getCodeResponse().equals(codeSuccess)){
			String idResource=response.getId();
			for (ResourceDetail  detail: resource.getResourceDetail()) {
					courseRepository.persistDetail(Utility.parseStringToLong(idResource),detail);			  
			}
			
			response=saveImage(fileLinkCar,fileLinkBanner,idResource);
		}		

		return CourseException.setMessageResponseSave(response);

	}

	public ResponseTransaction update(Long id, CourseDto courseDto) {
		
		routeFile="";
		nameFileCar="";
		nameFileBanner="";
		extensionFile="";

		ResponseTransaction response = new ResponseTransaction();

		Notification notification = CourseValidation.validation(courseDto);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}	
		
		readConfig();
		
		if (sharedConfig==null) {
			response.setCodeResponse("0090");
			response.setResponse("Error al leer la configuracion del servidor compartido");
			return response;
		}
		
		logger.info("curso a actualizar "+id);
		
		logger.info("id. "+id);
		logger.info("title. "+ courseDto.getTitle());
		logger.info("subtitle. " + courseDto.getSubtitle());
		logger.info("description. " + courseDto.getDescription());
		logger.info("link. " + courseDto.getLink());
		logger.info("link started. " + courseDto.getLinkStarted());
		logger.info("duration. " + courseDto.getDuration());
		logger.info("linkCar. " + courseDto.getLinkCar());
		logger.info("linkBanner. " + courseDto.getLinkBanner());
		logger.info("order. " + courseDto.getOrder());
		logger.info("enabled. " + courseDto.getEnabled());
		logger.info("createdUser. " + courseDto.getCreatedUser());
		logger.info("updatedUser. " + courseDto.getUpdatedUser());
		//logger.info("categoryId. " + courseDto.getCategoryId());
		//logger.info("subscriptionId. " + courseDto.getSubscriptionId());
		
		logger.info("detalle curso a actualizar "+id);
		 
		for (CourseDetailDto  detail: courseDto.getDetails()) {
			logger.info("title. "+detail.getTitle());	
			logger.info("description. "+detail.getDescription());	
			logger.info("order. "+detail.getOrder());	
		}	
		
		
		if(!Utility.isEmptyOrNull(courseDto.getLinkCar()) || !Utility.isEmptyOrNull(courseDto.getLinkBanner())) {
			
			byte[] bytesFileCar = (!Utility.isEmptyOrNull(courseDto.getLinkCar()))? Base64.getDecoder().decode(courseDto.getLinkCar()):null;
			
			nameFileCar= (!Utility.isEmptyOrNull(courseDto.getLinkCar()))?nameServerCar+id:nameFileCar;
			
			byte[] bytesFileBanner = (!Utility.isEmptyOrNull(courseDto.getLinkBanner()))? Base64.getDecoder().decode(courseDto.getLinkBanner()):null;
			
			nameFileBanner= (!Utility.isEmptyOrNull(courseDto.getLinkBanner()))? nameServerBanner+id:nameFileBanner;
			
			response=updateSharedServer(Utility.parseLongToString(id),sharedConfig.getCourseFolder(),bytesFileCar,nameFileCar,bytesFileBanner,nameFileBanner);
			
			if(!response.getCodeResponse().equalsIgnoreCase(codeSuccess)) {
				return response;
			}			
		}		
		
		Resource resource = resourceMapper.reverseMapperUpdate(id,nameFileCar,nameFileBanner,extensionFile,routeFile,courseDto);
		response = courseRepository.update(resource);
		
		if (response!= null && response.getCodeResponse()!=null && response.getCodeResponse().equals(codeSuccess)){
			logger.info("update detail"+resource.getResourceDetail().size());	
			for (ResourceDetail  detail: resource.getResourceDetail()) {
				courseRepository.persistDetail(Utility.parseStringToLong(response.getId()),detail);			  
			}
		}

		return CourseException.setMessageResponseUpdate(response);

	}
	

	private ResponseTransaction saveImage(byte[] bytesFileCar,byte[] bytesFileBanner, String id) {

		ResponseTransaction response = new ResponseTransaction();			
		
		nameFileCar=nameServerCar+id;
		nameFileBanner=nameServerBanner+id;
		
		response=saveSharedServer(id,sharedConfig.getCourseFolder(),bytesFileCar,nameFileCar,bytesFileBanner,nameFileBanner);
		
		if(!response.getCodeResponse().equalsIgnoreCase(codeSuccess)) {
			return response;
		}
		
		
		
	/*	response=saveSharedServer(id,sharedConfig.getCourseFolder(),bytesFileBanner,nameFileBanner);
		
		if(!response.getCodeResponse().equalsIgnoreCase(codeSuccess)) {
			return response;
		}*/
		
		//save repository
		Resource resource = resourceMapper.reverseMapperSaveImage(nameFileCar,nameFileBanner,extensionFile,routeFile,id);
		response = courseRepository.persistImage(resource);


		return CourseException.setMessageResponseSaveImageCar(response);
	}

	public ResponseTransaction searchAll(String keyword, String state,String page, String size) {
		
		ResponseTransaction response = new ResponseTransaction();
		
		ResourceRequest resourceRequest = new ResourceRequest();
		resourceRequest.setKeyword(keyword);
		resourceRequest.setState(state);
		resourceRequest.setPage(page);
		resourceRequest.setSize(size);
		
		
		Notification notification = ResourceValidation.validation(resourceRequest);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());			
			return response;
		}	

		return courseRepository.list(resourceRequest);
	}

	public ResponseTransaction get(Long id) throws IOException {
		/*
		 * Notification notification = ButtonValidation.validation(id); if
		 * (notification.hasErrors()) { throw new
		 * IllegalArgumentException(notification.errorMessage()); }
		 */
		ResponseTransaction response = courseRepository.read(id);

		return response;
	}

	private ResponseTransaction saveSharedServer(String idResource, String sharedDirectory, byte[] bytesFileCar,String nameFileCar,byte[] bytesFileBanner,String nameFileBanner) {
		ResponseTransaction response = new ResponseTransaction();
		NtlmPasswordAuthentication authentication = null;
		
		try {
			authentication = new NtlmPasswordAuthentication(sharedConfig.getDomain(), sharedConfig.getUser(),sharedConfig.getPassword());
		} catch (Exception ex) {
			response.setCodeResponse("0079");
			response.setResponse("Error al autenticar:" + ex);
			return response;
		}

		try {
			//nameFile=nameServerCar+idResource;
			extensionFile="jpg";
			routeFile=sharedConfig.getHttp()+ sharedConfig.getCourseFolder()+ idResource;				
			String sharedFolder = sharedConfig.getFolder() + sharedConfig.getCourseFolder() + idResource;
			String pathDirectory = "smb://" + sharedConfig.getServer() + sharedFolder;
			SmbFile smbDirectoy = new SmbFile(pathDirectory, authentication);
			if (!smbDirectoy.exists()) {
				smbDirectoy.mkdirs();
			}
			
			//String nameFile, String pathDirectory, byte[] bytesFile,NtlmPasswordAuthentication authentication) 
			
			ImagesSaveProccess cajera1 = new ImagesSaveProccess(nameFileCar, pathDirectory,bytesFileCar, authentication);
			ImagesSaveProccess cajera2 = new ImagesSaveProccess(nameFileBanner, pathDirectory,bytesFileBanner, authentication);

			cajera1.start();
			cajera2.start();
			
			
			
			//car
		/*	String fileName = "/"+nameFileCar+ "."+extensionFile;
			String pathFile = pathDirectory + fileName;
			SmbFile smbFileCar = new SmbFile(pathFile, authentication);
			SmbFileOutputStream smbfos;
			if (!smbFileCar.exists()) {
				smbfos = new SmbFileOutputStream(smbFileCar);
				smbfos.write(bytesFileCar);
				smbfos.flush();
				smbfos.close();
			}
			
			//banner
			fileName = "/"+nameFileBanner+ "."+extensionFile;
			pathFile = pathDirectory + fileName;
			SmbFile smbFileBanner = new SmbFile(pathFile, authentication);
			SmbFileOutputStream smbfos2;
			if (!smbFileBanner.exists()) {
				smbfos2 = new SmbFileOutputStream(smbFileBanner);
				smbfos2.write(bytesFileBanner);
				smbfos2.flush();
				smbfos2.close();
			}*/
			
		} catch (MalformedURLException ex) {
			response.setCodeResponse("0081");
			response.setResponse("Error MalformedURLException:" + ex);
			return response;
		} catch (Exception ex) {
			response.setCodeResponse("0083");
			response.setResponse("Error Server Window Save:" + ex);
			return response;
		}		

		response.setCodeResponse(codeSuccess);
		response.setResponse("Shared Server Succesfull.");
		return response;

	}
	
	private ResponseTransaction updateSharedServer(String idResource, String sharedDirectory, byte[] bytesFileCar,String nameFileCar,byte[] bytesFileBanner,String nameFileBanner) {
		ResponseTransaction response = new ResponseTransaction();
		NtlmPasswordAuthentication authentication = null;
		
		try {
			authentication = new NtlmPasswordAuthentication(sharedConfig.getDomain(), sharedConfig.getUser(),sharedConfig.getPassword());
		} catch (Exception ex) {
			response.setCodeResponse("0079");
			response.setResponse("Error al autenticar:" + ex);
			return response;
		}
		
		logger.info("actualizar en el directorio de las imagenes. ");

		try {			
			extensionFile="jpg";
			routeFile=sharedConfig.getHttp()+ sharedConfig.getCourseFolder()+ idResource;				
			String sharedFolder = sharedConfig.getFolder() + sharedConfig.getCourseFolder() + idResource;
			String pathDirectory = "smb://" + sharedConfig.getServer() + sharedFolder;
			SmbFile smbDirectoy = new SmbFile(pathDirectory, authentication);
			if (!smbDirectoy.exists()) {
				response.setCodeResponse("0084");
				response.setResponse("Error el directorio"+pathDirectory+" no existe");
				return response;
			}
			
			//car			
			if(!Utility.isEmptyOrNull(nameFileCar)) {				
				ImagesUpdateProccess cajera1 = new ImagesUpdateProccess(nameFileCar, pathDirectory,bytesFileCar, authentication);
				cajera1.start();
			}			
			
			//banner
			if(!Utility.isEmptyOrNull(nameFileBanner)) {
				ImagesUpdateProccess cajera2 = new ImagesUpdateProccess(nameFileBanner, pathDirectory,bytesFileBanner, authentication);			
				cajera2.start();
			}						
			
		} catch (MalformedURLException ex) {
			response.setCodeResponse("0081");
			response.setResponse("Error MalformedURLException:" + ex);
			return response;
		} catch (Exception ex) {
			response.setCodeResponse("0083");
			response.setResponse("Error Server Window Save:" + ex);
			return response;
		}		

		response.setCodeResponse(codeSuccess);
		response.setResponse("Shared Server Succesfull.");
		return response;

	}	

}
