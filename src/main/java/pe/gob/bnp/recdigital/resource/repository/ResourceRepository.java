package pe.gob.bnp.recdigital.resource.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.recdigital.resource.dto.ResourceRequest;
import pe.gob.bnp.recdigital.resource.model.Resource;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.repository.BaseRepository;

@Repository
public interface ResourceRepository extends BaseRepository<Resource>{
	
	public ResponseTransaction persistImage(Resource entity);
	public ResponseTransaction list(ResourceRequest userRequest);
	public ResponseTransaction read(Long id);
	public ResponseTransaction nextResourceId();
	public ResponseTransaction getResourceId();

}
