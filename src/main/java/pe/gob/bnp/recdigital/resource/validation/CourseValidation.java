package pe.gob.bnp.recdigital.resource.validation;

import org.springframework.web.multipart.MultipartFile;

import pe.gob.bnp.recdigital.resource.dto.CourseDto;
import pe.gob.bnp.recdigital.resource.dto.ResourceRequest;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class CourseValidation {
	
	private static String messageResourceSave = "No se encontraron datos del curso";
	private static String messageResourceSearchAll = "No se encontró el filtro de búsqueda";
	//private static String messageSubscriptionIdSave = "No se encontro id de subscripcion";
	//private static String messageCategoryIdSave = "No se encontro id de categoria";
	private static String messageTopicIdSave = "No se encontro id de tema";
	private static String messageSpecialityIdSave = "No se encontro id de specialidad";
	private static String messageTitleSave = "No se encontro titulo de curso";
	private static String messageResourceGet = "El id debe ser mayor a cero";
	private static String messageResourceImageCarSave = "No se encontro imagen car";
	private static String messageResourceImageBannerSave = "No se encontro imagen banner";
	private static String messageResourceIdSave = "No se encontro id del curso";

	public static Notification validation(CourseDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageResourceSave);
			return notification;
		}

		/*if (Utility.isEmptyOrNull(dto.getSubscriptionId())) {
			notification.addError(messageSubscriptionIdSave);
		}*/
		
		if (Utility.isEmptyOrNull(dto.getTopicId())) {
			notification.addError(messageTopicIdSave);
		}
		
		if (Utility.isEmptyOrNull(dto.getSpecialityId())) {
			notification.addError(messageSpecialityIdSave);
		}
		
		if (Utility.isEmptyOrNull(dto.getTitle())) {
			notification.addError(messageTitleSave);
		}

		return notification;
	}

	public static Notification validation(ResourceRequest dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageResourceSearchAll);
			return notification;
		}

		return notification;
	}
	
	public static Notification validation(MultipartFile file,String id) {

		Notification notification = new Notification();

		if (file == null || file.isEmpty()) {
			notification.addError(messageResourceImageCarSave);
			return notification;
		}
		if (file == null || file.isEmpty()) {
			notification.addError(messageResourceImageBannerSave);
			return notification;
		}

		if (Utility.isEmptyOrNull(id)) {
			notification.addError(messageResourceIdSave);
		}	
	

		return notification;
	}
	
	public static Notification validation(Long id) {
		Notification notification = new Notification();
		if (id <=0L) {
			notification.addError(messageResourceGet);
			return notification;
		}		
		return notification;
	}	

}
