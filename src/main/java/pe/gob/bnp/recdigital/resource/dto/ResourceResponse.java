package pe.gob.bnp.recdigital.resource.dto;

import pe.gob.bnp.recdigital.utilitary.common.Constants;

public class ResourceResponse {
	
	private String id;
	private String title;
	private String description;
	private String categoryId;	
	private String category;
	private String subcategoryId;	
	private String subcategory;
		private String formatId;
	private String format;	
	private String extensionId;
	private String extension;	
	private String stateId;
	private String state;
	//private String subscriptionId;
	//private String subscription;	
	private String link;
	private String enabled;	
	private String createdUser;
	private String createdDate;
	private String linkCar;	
	private String linkCar2;	
	private String linkCar3;	
	private String routeFolder;
	private String nameFolder;	
	private String observation;

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}
	public String getSubcategoryId() {
		return subcategoryId;
	}

	public void setSubcategoryId(String subcategoryId) {
		this.subcategoryId = subcategoryId;
	}

	public String getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

	public ResourceResponse() {
		super();
		this.id=Constants.EMPTY_STRING;
		this.title=Constants.EMPTY_STRING;
		this.description=Constants.EMPTY_STRING;	
		this.category=Constants.EMPTY_STRING;
		this.format=Constants.EMPTY_STRING;
		this.stateId=Constants.EMPTY_STRING;
		this.state=Constants.EMPTY_STRING;
		//this.subscriptionId=Constants.EMPTY_STRING;
		//this.subscription=Constants.EMPTY_STRING;
		this.link=Constants.EMPTY_STRING;		
		this.enabled=Constants.EMPTY_STRING;
		this.createdUser=Constants.EMPTY_STRING;
		this.createdDate=Constants.EMPTY_STRING;
		this.linkCar=Constants.EMPTY_STRING;	
		this.linkCar2=Constants.EMPTY_STRING;	
		this.linkCar3=Constants.EMPTY_STRING;	
		this.categoryId=Constants.EMPTY_STRING;	
		this.subcategoryId=Constants.EMPTY_STRING;	
		this.subcategory=Constants.EMPTY_STRING;
		this.formatId=Constants.EMPTY_STRING;	
		this.routeFolder=Constants.EMPTY_STRING;	
		this.nameFolder=Constants.EMPTY_STRING;	
		this.extension=Constants.EMPTY_STRING;
		this.extensionId=Constants.EMPTY_STRING;
		this.observation=Constants.EMPTY_STRING;
	}
	
	public String getExtensionId() {
		return extensionId;
	}

	public void setExtensionId(String extensionId) {
		this.extensionId = extensionId;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getRouteFolder() {
		return routeFolder;
	}

	public void setRouteFolder(String routeFolder) {
		this.routeFolder = routeFolder;
	}

	public String getNameFolder() {
		return nameFolder;
	}

	public void setNameFolder(String nameFolder) {
		this.nameFolder = nameFolder;
	}
	
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getFormatId() {
		return formatId;
	}
	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}
	
	public String getStateId() {
		return stateId;
	}
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}	
	/*public String getSubscriptionId() {
		return subscriptionId;
	}
	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	
	public String getSubscription() {
		return subscription;
	}
	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}	
	*/
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getLinkCar() {
		return linkCar;
	}
	public void setLinkCar(String linkCar) {
		this.linkCar = linkCar;
	}
	
	public String getLinkCar2() {
		return linkCar2;
	}

	public void setLinkCar2(String linkCar2) {
		this.linkCar2 = linkCar2;
	}

	public String getLinkCar3() {
		return linkCar3;
	}

	public void setLinkCar3(String linkCar3) {
		this.linkCar3 = linkCar3;
	}
	
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	public String getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

}
