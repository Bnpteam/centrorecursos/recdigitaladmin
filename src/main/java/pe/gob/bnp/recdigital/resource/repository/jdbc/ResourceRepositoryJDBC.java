package pe.gob.bnp.recdigital.resource.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigital.resource.dto.ListResourceResponse;
import pe.gob.bnp.recdigital.resource.dto.ResourceMultimediaResponse;
import pe.gob.bnp.recdigital.resource.dto.ResourceRequest;
import pe.gob.bnp.recdigital.resource.dto.ResourceResponse;
import pe.gob.bnp.recdigital.resource.model.Resource;
import pe.gob.bnp.recdigital.resource.repository.ResourceRepository;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;
import pe.gob.bnp.recdigital.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class ResourceRepositoryJDBC extends BaseJDBCOperation implements ResourceRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(ResourceRepositoryJDBC.class);
	
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction persist(Resource entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_RESOURCE.SP_PERSIST_RESOURCE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";/*17*/
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		 List<Object> resourcesResponse = new ArrayList<>();
		 
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setLong("P_ID", entity.getId());
			cstm.setString("P_TITLE", Utility.getString(entity.getTitle()));			
			cstm.setString("P_SUBTITLE",Utility.getString(entity.getSubtitle()));
			cstm.setString("P_DESCRIPTION",Utility.getString(entity.getResourceDescription()));
			//cstm.setString("P_LINK",Utility.getString(entity.getResourceLink()));
			cstm.setInt("P_ORDER",entity.getResourceOrder());
			cstm.setLong("P_CREATED_USER", Utility.parseStringToLong(entity.getCreatedUser()));
			cstm.setLong("P_CATEGORY_ID", entity.getCategoryId());
			cstm.setLong("P_SUBCATEGORY_ID", entity.getSubcategoryId());
			cstm.setLong("P_FORMAT_ID", entity.getFormatId());
			cstm.setLong("P_EXTENSION_ID", entity.getExtensionId());
			cstm.setString("P_FLAG_IMAGE",Utility.getString(entity.getFlagImage()));
			cstm.setString("P_FLAG_IMAGE2",Utility.getString(entity.getFlagImage2()));
			cstm.setString("P_FLAG_IMAGE3",Utility.getString(entity.getFlagImage3()));
			//cstm.setLong("P_SUBSCRIPTION_ID", entity.getSubscription().getId());
			cstm.setString("P_OBSERVATION",Utility.getString(entity.getObservation()));
			
			cstm.registerOutParameter("S_RESOURCE_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_RESOURCE_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
				
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					ResourceMultimediaResponse resourceResponse = new ResourceMultimediaResponse();					
					resourceResponse.setLink(Utility.getString(rs.getString("RESOURCE_LINK")));
					resourceResponse.setRouteFolder(Utility.getString(rs.getString("RESOURCE_ROUTE_FOLDER")));		
					resourceResponse.setNameFolder(Utility.getString(rs.getString("RESOURCE_NAME_FOLDER")));		
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
				
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error("PKG_API_RESOURCE.SP_PERSIST_RESOURCE: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				//e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
	@Override
	public ResponseTransaction update(Resource entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_RESOURCE.SP_UPDATE_RESOURCE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";/*18*/
        ResponseTransaction response = new ResponseTransaction();       
		CallableStatement cstm = null;
		ResultSet rs = null;
		 List<Object> resourcesResponse = new ArrayList<>();
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_ID", entity.getId());
			cstm.setString("P_TITLE", Utility.getString(entity.getTitle()));			
			cstm.setString("P_SUBTITLE",Utility.getString(entity.getSubtitle()));
			cstm.setString("P_DESCRIPTION",Utility.getString(entity.getResourceDescription()));		
			//cstm.setString("P_NAME",Utility.getString(entity.getNameCar()));
			//cstm.setString("P_EXTENSION",Utility.getString(entity.getExtensionCar()));
			//cstm.setString("P_ROUTE",Utility.getString(entity.getRouteCar()));	
			cstm.setInt("P_ORDER",entity.getResourceOrder());
			cstm.setString("P_ENABLED",Utility.getString(entity.getIdEnabled()));
			cstm.setLong("P_UPDATED_USER", Utility.parseStringToLong(entity.getUpdatedUser()));
			cstm.setLong("P_CATEGORY_ID", entity.getCategoryId());
			cstm.setLong("P_SUBCATEGORY_ID", entity.getSubcategoryId());
			cstm.setLong("P_FORMAT_ID", entity.getFormatId());
			cstm.setLong("P_EXTENSION_ID", entity.getExtensionId());
			cstm.setString("P_FLAG_IMAGE",Utility.getString(entity.getFlagImage()));
			cstm.setString("P_FLAG_IMAGE2",Utility.getString(entity.getFlagImage2()));
			cstm.setString("P_FLAG_IMAGE3",Utility.getString(entity.getFlagImage3()));
			cstm.setString("P_OBSERVATION",Utility.getString(entity.getObservation()));
			
			//cstm.setLong("P_SUBSCRIPTION_ID", entity.getSubscription().getId());
			cstm.registerOutParameter("S_RESOURCE_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_RESOURCE_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					ResourceMultimediaResponse resourceResponse = new ResourceMultimediaResponse();					
					resourceResponse.setLink(Utility.getString(rs.getString("RESOURCE_LINK")));
					resourceResponse.setRouteFolder(Utility.getString(rs.getString("RESOURCE_ROUTE_FOLDER")));		
					resourceResponse.setNameFolder(Utility.getString(rs.getString("RESOURCE_NAME_FOLDER")));					
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error("PKG_API_RESOURCE.SP_UPDATE_RESOURCE: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
	@Override
	public ResponseTransaction persistImage(Resource entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_RESOURCE.SP_PERSIST_IMAGECAR(?,?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setLong("P_ID", entity.getId());			
			cstm.setString("P_NAME",Utility.getString(entity.getNameCar()));
			cstm.setString("P_EXTENSION",Utility.getString(entity.getExtensionCar()));
			cstm.setString("P_ROUTE",Utility.getString(entity.getRouteCar()));				
			cstm.registerOutParameter("S_RESOURCE_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_RESOURCE_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error("PKG_API_RESOURCE.SP_PERSIST_IMAGECAR: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

		
	@Override
	public ResponseTransaction list(ResourceRequest resourceRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_RESOURCE.SP_LIST_RESOURCE(?,?,?,?,?,?)}";
        
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setString("P_KEYWORD", Utility.getString(resourceRequest.getKeyword()));	
			cstm.setString("P_STATE", Utility.getString(resourceRequest.getState()));
			cstm.setInt("P_PAGE", Utility.parseStringToInt(resourceRequest.getPage()));	
			cstm.setInt("P_SIZE", Utility.parseStringToInt(resourceRequest.getSize()));	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					ListResourceResponse resourceResponse = new ListResourceResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("RESOURCE_ID")));
					resourceResponse.setTitle(Utility.getString(rs.getString("RESOURCE_TITLE")));
					resourceResponse.setDescription(Utility.getString(rs.getString("RESOURCE_DESCRIPTION")));
					resourceResponse.setCategoryId(Utility.parseLongToString(rs.getLong("RESOURCE_CATEGORY_ID")));
					resourceResponse.setCategory(Utility.getString(rs.getString("RESOURCE_CATEGORY")));
					resourceResponse.setFormatId(Utility.parseLongToString(rs.getLong("RESOURCE_FORMAT_ID")));
					resourceResponse.setFormat(Utility.getString(rs.getString("RESOURCE_FORMAT")));
					resourceResponse.setStateId(Utility.getString(rs.getString("RESOURCE_STATE_ID")));
					resourceResponse.setState(Utility.getString(rs.getString("RESOURCE_STATE")));
					resourceResponse.setEnabled(Utility.getString(rs.getString("RESOURCE_ENABLED")));
					resourceResponse.setCreatedUser(Utility.getString(rs.getString("CREATED_USER")));
					resourceResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
					resourceResponse.setTotalRecords(Utility.getString(rs.getString("TOTAL_RECORDS")));
					
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_RESOURCE.SP_LIST_RESOURCE: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTransaction read(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_RESOURCE.SP_GET_RESOURCE(?,?,?)}";
             
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_RESOURCE_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {					
					ResourceResponse resourceResponse = new ResourceResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("RESOURCE_ID")));
					resourceResponse.setTitle(Utility.getString(rs.getString("RESOURCE_TITLE")));
					resourceResponse.setDescription(Utility.getString(rs.getString("RESOURCE_DESCRIPTION")));
					resourceResponse.setCategoryId(Utility.parseLongToString(rs.getLong("RESOURCE_CATEGORY_ID")));
					resourceResponse.setCategory(Utility.getString(rs.getString("RESOURCE_CATEGORY")));
					resourceResponse.setSubcategoryId((rs.getLong("RESOURCE_SUBCAT_ID"))+"");
					resourceResponse.setSubcategory(Utility.getString(rs.getString("RESOURCE_SUBCAT")));					
					resourceResponse.setFormatId(Utility.parseLongToString(rs.getLong("RESOURCE_FORMAT_ID")));
					resourceResponse.setFormat(Utility.getString(rs.getString("RESOURCE_FORMAT")));
					resourceResponse.setExtensionId(Utility.parseLongToString(rs.getLong("RESOURCE_EXTENSION_ID")));
					resourceResponse.setExtension(Utility.getString(rs.getString("RESOURCE_EXTENSION")));
					resourceResponse.setLink(Utility.getString(rs.getString("RESOURCE_LINK")));
					resourceResponse.setRouteFolder(Utility.getString(rs.getString("RESOURCE_ROUTE_FOLDER")));
					resourceResponse.setNameFolder(Utility.getString(rs.getString("RESOURCE_NAME_FOLDER")));
					resourceResponse.setLinkCar(Utility.getString(rs.getString("CAR_LINK")));
					resourceResponse.setLinkCar2(Utility.getString(rs.getString("CAR_LINK2")));
					resourceResponse.setLinkCar3(Utility.getString(rs.getString("CAR_LINK3")));
					resourceResponse.setObservation(Utility.getString(rs.getString("RESOURCE_OBSERVATION")));
					resourceResponse.setStateId(Utility.getString(rs.getString("RESOURCE_STATE_ID")));
					resourceResponse.setState(Utility.getString(rs.getString("RESOURCE_STATE")));
					//resourceResponse.setSubscriptionId(Utility.parseLongToString(rs.getLong("SUBSCRIPTION_ID")));
					//resourceResponse.setSubscription(Utility.getString(rs.getString("SUBSCRIPTION_NAME")));
					resourceResponse.setEnabled(Utility.getString(rs.getString("RESOURCE_ENABLED")));
					resourceResponse.setCreatedUser(Utility.getString(rs.getString("CREATED_USER")));
					resourceResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
				
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_RESOURCE.SP_GET_RESOURCE: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTransaction nextResourceId() {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_UTILITY.GET_NEXT_ID_RESOURCE()}";
             
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);		
			cstm.registerOutParameter(1, OracleTypes.VARCHAR);;			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getString(1);
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {					
					ResourceResponse resourceResponse = new ResourceResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("RESOURCE_ID")));
					resourceResponse.setTitle(Utility.getString(rs.getString("RESOURCE_TITLE")));
					resourceResponse.setDescription(Utility.getString(rs.getString("RESOURCE_DESCRIPTION")));
					resourceResponse.setCategoryId(Utility.parseLongToString(rs.getLong("RESOURCE_CATEGORY_ID")));
					resourceResponse.setCategory(Utility.getString(rs.getString("RESOURCE_CATEGORY")));
					resourceResponse.setFormatId(Utility.parseLongToString(rs.getLong("RESOURCE_FORMAT_ID")));
					resourceResponse.setFormat(Utility.getString(rs.getString("RESOURCE_FORMAT")));
					resourceResponse.setLink(Utility.getString(rs.getString("RESOURCE_LINK")));
					resourceResponse.setLinkCar(Utility.getString(rs.getString("CAR_LINK")));
					resourceResponse.setStateId(Utility.getString(rs.getString("RESOURCE_STATE_ID")));
					resourceResponse.setState(Utility.getString(rs.getString("RESOURCE_STATE")));
					//resourceResponse.setSubscriptionId(Utility.parseLongToString(rs.getLong("SUBSCRIPTION_ID")));
					//resourceResponse.setSubscription(Utility.getString(rs.getString("SUBSCRIPTION_NAME")));
					resourceResponse.setEnabled(Utility.getString(rs.getString("RESOURCE_ENABLED")));
					resourceResponse.setCreatedUser(Utility.getString(rs.getString("CREATED_USER")));
					resourceResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
				
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_RESOURCE.SP_GET_RESOURCE: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	@Override
	public ResponseTransaction getResourceId() {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_GET_RESOURCE_ID(?,?)}";
        ResponseTransaction response = new ResponseTransaction();       
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);			
			cstm.registerOutParameter("S_RESOURCE_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);		
			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_RESOURCE_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_UTILITY.SP_GET_RESOURCE_ID: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
			
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
}
