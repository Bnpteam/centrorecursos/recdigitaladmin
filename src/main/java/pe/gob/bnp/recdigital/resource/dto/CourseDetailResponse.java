package pe.gob.bnp.recdigital.resource.dto;

import pe.gob.bnp.recdigital.utilitary.common.Constants;

public class CourseDetailResponse {
	
	private String title;
	
	private String description;
	
	private String order;	
	
	public CourseDetailResponse() {
		super();
		this.title=Constants.EMPTY_STRING;
		this.description=Constants.EMPTY_STRING;
		this.order=Constants.EMPTY_STRING;
	}	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}
	
}
