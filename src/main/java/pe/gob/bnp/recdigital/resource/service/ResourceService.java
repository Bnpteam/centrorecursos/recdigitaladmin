package pe.gob.bnp.recdigital.resource.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Base64;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import pe.gob.bnp.recdigital.config.shared.dto.SharedResponse;
import pe.gob.bnp.recdigital.config.shared.repository.SharedRepository;
import pe.gob.bnp.recdigital.resource.dto.ResourceDto;
import pe.gob.bnp.recdigital.resource.dto.ResourceRequest;
import pe.gob.bnp.recdigital.resource.exception.ResourceException;
import pe.gob.bnp.recdigital.resource.mapper.ResourceMapper;
import pe.gob.bnp.recdigital.resource.model.Resource;
import pe.gob.bnp.recdigital.resource.repository.ResourceRepository;
import pe.gob.bnp.recdigital.resource.validation.ResourceValidation;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

@Service
public class ResourceService {

	private static final Logger log = LoggerFactory.getLogger(ResourceService.class);

	private String codeSuccess = "0000";

	@Autowired
	ResourceRepository resourceRepository;

	@Autowired
	SharedRepository sharedRepository;

	@Autowired
	ResourceMapper resourceMapper;

	private SharedResponse sharedConfig;

	//private String routeFile = "", nameFile = "", extensionFile = "";

	//private String nameServer = "imageCar";
	
	private static final String NOMBRE_FOTO= "imageCar",NOMBRE_FOTO2= "imageCarTwo",NOMBRE_FOTO3= "imageCarThree";
	
	private ResponseTransaction response;
	
	private String resourceId,nombreFoto,nombreFoto2,nombreFoto3;
	
	private byte[] fotoBytes,fotoBytes2,fotoBytes3;

	private void readConfig() {
		try {
			// leer configuracion
			response = sharedRepository.read();

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess)) {
				List<Object> sharedList = response.getList();
				if (!sharedList.isEmpty()) {
					Object sharedObject = sharedList.get(0);
					if (sharedObject instanceof SharedResponse) {
						sharedConfig = (SharedResponse) sharedObject;
						// System.out.println("entro postcontructor.");
					}
				}

			}

		} catch (Exception ex) {
			response.setCodeResponse("0666");			
			response.setResponse(ex.getMessage());
			log.error("0666 error postconstructor:" + ex);
		}
	}

	private void inicializarVariables() {

		response = new ResponseTransaction();
		resourceId = "";
		nombreFoto = "";
		nombreFoto2 = "";
		nombreFoto3 = "";
	}
	
	private void inicializarConfiguracion() {
		
		readConfig();

		if (sharedConfig == null) {
			response.setCodeResponse("0090");
			response.setResponse("Error al leer la configuración del servidor compartido");			
			log.error("Error al leer la configuración del servidor compartido");
		}	
	}
	
	private void inicializarValidacionesDto(ResourceDto eventosDto) {
		
		Notification notification = ResourceValidation.validation(eventosDto);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());		
			log.error("Error validaciones:"+ notification.errorMessage());
		}		
	}
	
	private void inicializarValidacionesFoto(ResourceDto eventosDto) {
		
		try {
			fotoBytes = Base64.getDecoder().decode(eventosDto.getLinkCar());
		}catch(Exception ex) {
			response.setCodeResponse("0091");
			response.setResponse("Error al decodificar la imagen..."+ex);
			log.error("Error al decodificar la imagen..."+ex);
		}
	}
	
	private void inicializarValidacionesFoto2(ResourceDto eventosDto) {
		
		try {
			fotoBytes2 = Base64.getDecoder().decode(eventosDto.getLinkCar2());
		}catch(Exception ex) {
			response.setCodeResponse("0091");
			response.setResponse("Error al decodificar la imagen..."+ex);
			log.error("Error al decodificar la imagen 2..."+ex);
		}
	}

	private void inicializarValidacionesFoto3(ResourceDto eventosDto) {
	
	try {
		fotoBytes3 = Base64.getDecoder().decode(eventosDto.getLinkCar3());
	}catch(Exception ex) {
		response.setCodeResponse("0091");
		response.setResponse("Error al decodificar la imagen..."+ex);
		log.error("Error al decodificar la imagen 3..."+ex);
		}
	}
	
	private void obtenerResourceId() {
		
		response = resourceRepository.getResourceId();
		
		if(response != null && response.getCodeResponse() != null && response.getCodeResponse().equals(codeSuccess)) {
			
			resourceId=response.getId();
			
		}
	}
	
	private void guardarFotoServidor() {
		
		nombreFoto=NOMBRE_FOTO+resourceId;
		
		nombreFoto2=NOMBRE_FOTO2+resourceId;
		
		nombreFoto3=NOMBRE_FOTO3+resourceId;
		
		response=saveSharedServer();
	}
	
	private ResponseTransaction saveSharedServer(){
		
		NtlmPasswordAuthentication authentication = null;
		
		try {
			authentication = new NtlmPasswordAuthentication(sharedConfig.getDomain(), sharedConfig.getUser(), sharedConfig.getPassword());
		} catch (Exception ex) {
			response.setCodeResponse("0079");
			response.setResponse("Error al autenticar:" + ex);
			log.error("Error al autenticar:" + ex);
			return response;
		}

		try {	
			
			String sharedFolder1 = sharedConfig.getFolder() + sharedConfig.getResourceMultimedia() + resourceId;
			String pathDirectory1 = "smb://" + sharedConfig.getServer() + sharedFolder1;
			SmbFile smbDirectoy1 = new SmbFile(pathDirectory1, authentication);
			if (!smbDirectoy1.exists()) {
				smbDirectoy1.mkdirs();
				log.info("creado directorio multimedia");
			}
			
			
			
			String sharedFolder = sharedConfig.getFolder() + sharedConfig.getResourceFolder() + resourceId;
			String pathDirectory = "smb://" + sharedConfig.getServer() + sharedFolder;
			SmbFile smbDirectoy = new SmbFile(pathDirectory, authentication);
			if (!smbDirectoy.exists()) {
				smbDirectoy.mkdirs();
				log.info("creado directorio resource");
			}
			
			if (fotoBytes!=null && fotoBytes.length>0) {
				log.info("guardado server con imagen");
				
				ImagesSaveProccess fotoProccess = new ImagesSaveProccess(nombreFoto, pathDirectory,fotoBytes, authentication);				

				fotoProccess.start();
			}
			
			//if(Utility.isEmptyOrNull(nombreFoto) && Utility.isEmptyOrNull(nombreFoto2) && Utility.isEmptyOrNull(nombreFoto3)) {
			if (fotoBytes2!=null && fotoBytes2.length>0) {
				log.info("guardado server con imagen 2");
					
				ImagesSaveProccess fotoProccess2 = new ImagesSaveProccess(nombreFoto2, pathDirectory,fotoBytes2, authentication);				

				fotoProccess2.start();
			}
			if (fotoBytes3!=null && fotoBytes3.length>0) {
				log.info("guardado server con imagen 3");
						
				ImagesSaveProccess fotoProccess3 = new ImagesSaveProccess(nombreFoto3, pathDirectory,fotoBytes3, authentication);				

				fotoProccess3.start();
				}
			if (fotoBytes==null && fotoBytes2==null && fotoBytes3==null) {
				log.info("guardado server sin imagen");
			}
				
			
		} catch (MalformedURLException ex) {
			response.setCodeResponse("0081");
			response.setResponse("Error MalformedURLException guardar:" + ex);
			log.error("Error MalformedURLException guardar:" + ex);
			return response;
		} catch (Exception ex) {
			response.setCodeResponse("0083");
			response.setResponse("Error Window Server guardar:" + ex);
			log.error("Error Window Server guardar:" + ex);
			return response;
		}		

		response.setCodeResponse(codeSuccess);
		response.setResponse("Servidor Compartido Exitoso.");
		log.info("Servidor Compartido Exitoso.");
		return response;
		
	

	}
		
	private void guardar(ResourceDto resourceDto) {
		
		resourceDto.setId(resourceId);		
		
		Resource resource = resourceMapper.reverseMapperSave(resourceDto);
		
		if (!Utility.isEmptyOrNull(resourceDto.getLinkCar())) {
			resource.setFlagImage("1");
			log.info("El recurso se guarda con imagen principal...");
		}	
		else {
			resource.setFlagImage("0");
			log.info("El recurso se guarda sin imagen principal...");
		}
			
		/////////////////////////////////
		if (!Utility.isEmptyOrNull(resourceDto.getLinkCar2())) {
			resource.setFlagImage2("1");
			log.info("El recurso se guarda con imagen principal 2...");
		}
		else {
			resource.setFlagImage2("0");
			log.info("El recurso se guarda sin imagen principal 2...");
		}
		
		////////////////////////////////
		if (!Utility.isEmptyOrNull(resourceDto.getLinkCar3())) {
			resource.setFlagImage3("1");
			log.info("El recurso se guarda con imagen principal 3...");
		}
		else {
			resource.setFlagImage3("0");
			log.info("El recurso se guarda sin imagen principal 3...");
		}
		
		response = resourceRepository.persist(resource);
		
	}	
	
	public ResponseTransaction save(ResourceDto resourceDto) {
	
	try {

		inicializarVariables();
		
		inicializarConfiguracion();
		
		if(response.getCodeResponse().equalsIgnoreCase(codeSuccess)) inicializarValidacionesDto(resourceDto);
		
		if(response.getCodeResponse().equalsIgnoreCase(codeSuccess)) {
			if (!Utility.isEmptyOrNull(resourceDto.getLinkCar())) {
				inicializarValidacionesFoto(resourceDto);
			}
			if (!Utility.isEmptyOrNull(resourceDto.getLinkCar2())) {
				inicializarValidacionesFoto2(resourceDto);
			}
			if (!Utility.isEmptyOrNull(resourceDto.getLinkCar3())) {
				inicializarValidacionesFoto3(resourceDto);
			}
		}
			
		
		if(response.getCodeResponse().equalsIgnoreCase(codeSuccess)) obtenerResourceId();
		
		if(response.getCodeResponse().equalsIgnoreCase(codeSuccess)) guardarFotoServidor();
		
		if(response.getCodeResponse().equalsIgnoreCase(codeSuccess)) guardar(resourceDto);		
		
	}catch(Exception ex) {
		response.setCodeResponse("0666");			
		response.setResponse(ex.getMessage());
		log.error("Erro save resource:"+ex.getMessage());
		return response;
	}	

		return ResourceException.setMessageResponseSave(response);		
		
	}
	
	private void actualizar(ResourceDto resourceDto) {

		resourceDto.setId(resourceId);
		
		Resource resource = resourceMapper.reverseMapperUpdate(resourceDto);
		
		if (!Utility.isEmptyOrNull(resourceDto.getLinkCar())) {
			resource.setFlagImage("1");
			log.info("El recurso se actualiza con imagen principal...");
		}else {
			resource.setFlagImage("0");
			log.info("El recurso se actualiza sin imagen principal...");
		}
		
		////////////////////
		if (!Utility.isEmptyOrNull(resourceDto.getLinkCar2())) {
			resource.setFlagImage2("1");
			log.info("El recurso se actualiza con imagen principal 2...");
		}else {
			resource.setFlagImage2("0");
			log.info("El recurso se actualiza sin imagen principal 2...");
		}
		
		///////////////////
		if (!Utility.isEmptyOrNull(resourceDto.getLinkCar3())) {
			resource.setFlagImage3("1");
			log.info("El recurso se actualiza con imagen principal 3...");
		}else {
			resource.setFlagImage3("0");
			log.info("El recurso se actualiza sin imagen principal 3...");
		}
		
		response = resourceRepository.update(resource);

	}
	
	private void actualizarFotoServidor() {

		nombreFoto = NOMBRE_FOTO + resourceId;
		
		nombreFoto2 = NOMBRE_FOTO2 + resourceId;
		
		nombreFoto3 = NOMBRE_FOTO3 + resourceId;

		response = updateSharedServer();
	}
	
	private ResponseTransaction updateSharedServer() {

		NtlmPasswordAuthentication authentication = null;

		try {
			authentication = new NtlmPasswordAuthentication(sharedConfig.getDomain(), sharedConfig.getUser(),sharedConfig.getPassword());
		} catch (Exception ex) {
			response.setCodeResponse("0079");
			response.setResponse("Error al autenticar:" + ex);
			log.error("Error al autenticar:" + ex);
			return response;
		}

		try {
			String sharedFolder = sharedConfig.getFolder() + sharedConfig.getResourceFolder() + resourceId;
			String pathDirectory = "smb://" + sharedConfig.getServer() + sharedFolder;
			SmbFile smbDirectoy = new SmbFile(pathDirectory, authentication);
			if (!smbDirectoy.exists()) {
				response.setCodeResponse("0084");
				response.setResponse("Error el directorio" + pathDirectory + " no existe");
				log.error("Error el directorio" + pathDirectory + " no existe");
				smbDirectoy.mkdirs();
				log.info("creando directorio resource");
				return response;
			}

			if (fotoBytes!=null && fotoBytes.length>0) {
				log.info("se actualiza servidor con imagen");
				ImagesUpdateProccess imagen1 = new ImagesUpdateProccess(nombreFoto, pathDirectory, fotoBytes,authentication);
				imagen1.start();
			}
			if (fotoBytes2!=null && fotoBytes2.length>0) {
				log.info("se actualiza servidor con imagen 2");
				ImagesUpdateProccess imagen2 = new ImagesUpdateProccess(nombreFoto2, pathDirectory, fotoBytes2,authentication);
				imagen2.start();
			}
			if (fotoBytes3!=null && fotoBytes3.length>0) {
				log.info("se actualiza servidor con imagen 3");
				ImagesUpdateProccess imagen3 = new ImagesUpdateProccess(nombreFoto3, pathDirectory, fotoBytes3,authentication);
				imagen3.start();
			}			
			if(fotoBytes==null && fotoBytes2==null && fotoBytes3==null) {
				log.info("se actualiza servidor sin imagen");
			}
				

		} catch (MalformedURLException ex) {
			response.setCodeResponse("0081");
			response.setResponse("Error MalformedURLException actualizar:" + ex);
			log.error("Error MalformedURLException actualizar:" + ex);
			return response;
		} catch (Exception ex) {
			response.setCodeResponse("0083");
			response.setResponse("Error Window Server actualizar:" + ex);
			log.error("Error Window Server actualizar:" + ex);
			return response;
		}

		response.setCodeResponse(codeSuccess);
		response.setResponse("Servidor Compartido Exitoso.");
		log.info("Actualizacion Servidor Compartido Exitoso.");
		return response;

	}

	public ResponseTransaction update(Long id, ResourceDto resourceDto) {				
		
		try {
			
			inicializarVariables();
			
			inicializarConfiguracion();
			
			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				inicializarValidacionesDto(resourceDto);
			
			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess)) {
				if (!Utility.isEmptyOrNull(resourceDto.getLinkCar())) {
					inicializarValidacionesFoto(resourceDto);
				}
				if (!Utility.isEmptyOrNull(resourceDto.getLinkCar2())) {
					inicializarValidacionesFoto2(resourceDto);
				}
				if (!Utility.isEmptyOrNull(resourceDto.getLinkCar3())) {
					inicializarValidacionesFoto3(resourceDto);
				}
			}
				
			
			resourceId = String.valueOf(id);
			
			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				if (!Utility.isEmptyOrNull(resourceDto.getLinkCar()) || !Utility.isEmptyOrNull(resourceDto.getLinkCar2()) || 
					!Utility.isEmptyOrNull(resourceDto.getLinkCar3())) {
					actualizarFotoServidor();
				}

			if (response.getCodeResponse().equalsIgnoreCase(codeSuccess))
				actualizar(resourceDto);
		
		
		}catch(Exception ex) {
			response.setCodeResponse("0666");			
			response.setResponse(ex.getMessage());
			log.error("Erro update resource:"+ex.getMessage());
			return response;
		}	

			return ResourceException.setMessageResponseUpdate(response);			
	}

	public ResponseTransaction searchAll(String keyword, String state, String page, String size) {

		ResponseTransaction response = new ResponseTransaction();

		ResourceRequest resourceRequest = new ResourceRequest();
		resourceRequest.setKeyword(keyword);
		resourceRequest.setState(state);
		resourceRequest.setPage(page);
		resourceRequest.setSize(size);

		Notification notification = ResourceValidation.validation(resourceRequest);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		return resourceRepository.list(resourceRequest);
	}

	public ResponseTransaction get(Long id) throws IOException {
		/*
		 * Notification notification = ButtonValidation.validation(id); if
		 * (notification.hasErrors()) { throw new
		 * IllegalArgumentException(notification.errorMessage()); }
		 */
		ResponseTransaction response = resourceRepository.read(id);

		return response;
	}

	

	/*	private ResponseTransaction deleteFolderServer(Long idResource) {
		ResponseTransaction response = new ResponseTransaction();
		NtlmPasswordAuthentication authentication = null;

		try {
			authentication = new NtlmPasswordAuthentication(sharedConfig.getDomain(), sharedConfig.getUser(),
					sharedConfig.getPassword());
		} catch (Exception ex) {
			response.setCodeResponse("0079");
			response.setResponse("Error al autenticar:" + ex);
			return response;
		}

		try {
			String sharedFolder = sharedConfig.getFolder() + sharedConfig.getResourceMultimedia() + idResource;
			String pathDirectory = "smb://" + sharedConfig.getServer() + sharedFolder;
			SmbFile smbDirectoy = new SmbFile(pathDirectory, authentication);
			if (!smbDirectoy.exists()) {
				smbDirectoy.mkdirs();
			} else {
				smbDirectoy.delete();
				smbDirectoy.mkdirs();
			}

		} catch (MalformedURLException ex) {
			response.setCodeResponse("0081");
			response.setResponse("Error MalformedURLException:" + ex);
			return response;
		} catch (Exception ex) {
			response.setCodeResponse("0083");
			response.setResponse("Error Server Window Save:" + ex);
			return response;
		}

		response.setCodeResponse(codeSuccess);
		response.setResponse("Shared Multimedia Server Succesfull.");
		return response;

	}
	
	*/

	

	/*private ResponseTransaction updateSharedServer(String idResource, byte[] bytesFile) {
		ResponseTransaction response = new ResponseTransaction();
		NtlmPasswordAuthentication authentication = null;

		try {
			authentication = new NtlmPasswordAuthentication(sharedConfig.getDomain(), sharedConfig.getUser(),
					sharedConfig.getPassword());
		} catch (Exception ex) {
			response.setCodeResponse("0079");
			response.setResponse("Error al autenticar:" + ex);
			return response;
		}

		try {
			nameFile = nameServer + idResource;
			extensionFile = "jpg";
			routeFile = sharedConfig.getHttp() + sharedConfig.getResourceFolder() + idResource;
			String fileName = "/" + nameFile + "." + extensionFile;
			String sharedFolder = sharedConfig.getFolder() + sharedConfig.getResourceFolder() + idResource;
			String pathDirectory = "smb://" + sharedConfig.getServer() + sharedFolder;
			SmbFile smbDirectoy = new SmbFile(pathDirectory, authentication);
			if (!smbDirectoy.exists()) {
				response.setCodeResponse("0084");
				response.setResponse("Error el directorio" + pathDirectory + " no existe");
				return response;
			}
			String pathFile = pathDirectory + fileName;
			SmbFile smbFile = new SmbFile(pathFile, authentication);
			SmbFileOutputStream smbfos;
			if (smbFile.exists())
				smbFile.delete();
			smbfos = new SmbFileOutputStream(smbFile);
			smbfos.write(bytesFile);
			smbfos.flush();
			smbfos.close();

		} catch (MalformedURLException ex) {
			response.setCodeResponse("0081");
			response.setResponse("Error MalformedURLException:" + ex);
			return response;
		} catch (Exception ex) {
			response.setCodeResponse("0083");
			response.setResponse("Error Server Window Save:" + ex);
			return response;
		}

		response.setCodeResponse(codeSuccess);
		response.setResponse("Shared Server Succesfull.");
		return response;

	}*/
	
	/*private ResponseTransaction deleteFolderMultimedia(Long id) {
	ResponseTransaction response = new ResponseTransaction();

	if (sharedConfig == null) {
		response.setCodeResponse("0090");
		response.setResponse("Error al leer la configuracion del servidor compartido");
		return response;
	}

	response = deleteFolderServer(id);

	return response;
	}*/


}
