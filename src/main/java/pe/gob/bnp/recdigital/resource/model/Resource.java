package pe.gob.bnp.recdigital.resource.model;

import java.util.ArrayList;
import java.util.List;
import pe.gob.bnp.recdigital.type.model.Type;
import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.model.BaseEntity;

public class Resource extends BaseEntity{	

	private String title;
	private String subtitle;
	private String resourceDescription;
	private String routeBanner;
	private String nameBanner;
	private String extensionBanner;
	private String defaultRecommended;
	private String resourceLink;
	private String routeCar;
	private String nameCar;
	private String extensionCar;
	private String resourceDuration;
	private String resourceState;	
	private String urlStartedResource;
	private String codeStartedResource;	
	private Integer resourceOrder;
	private Integer numVotes;
	private Integer totalScore;
	private Integer totalView;
	private Integer totalShare;
	private Integer totalLike;
	private Double rating;
	private Long extensionId;
	private String flagImage;
	private String flagImage2;
	private String flagImage3;
	private String observation;
	
	public String getObservation() {
		return observation;
	}


	public void setObservation(String observation) {
		this.observation = observation;
	}


	public String getFlagImage() {
		return flagImage;
	}


	public void setFlagImage(String flagImage) {
		this.flagImage = flagImage;
	}
	
	public String getFlagImage2() {
		return flagImage2;
	}


	public void setFlagImage2(String flagImage2) {
		this.flagImage2 = flagImage2;
	}


	public String getFlagImage3() {
		return flagImage3;
	}


	public void setFlagImage3(String flagImage3) {
		this.flagImage3 = flagImage3;
	}



	public Long getExtensionId() {
		return extensionId;
	}


	public void setExtensionId(Long extensionId) {
		this.extensionId = extensionId;
	}

	//private Category category;
	private Type type;
	private Long categoryId;
	private Long subcategoryId;
	public Long getSubcategoryId() {
		return subcategoryId;
	}


	public void setSubcategoryId(Long subcategoryId) {
		this.subcategoryId = subcategoryId;
	}

	private Long formatId;
	private Long topicId;
	private Long specialityId;
	//private Subscription subscription;
	private List<ResourceDetail> resourceDetail;
	
	public Resource() {
		super();
		
		this.title = Constants.EMPTY_STRING;
		this.subtitle = Constants.EMPTY_STRING;
		this.resourceDescription = Constants.EMPTY_STRING;
		this.routeBanner = Constants.EMPTY_STRING;
		this.nameBanner = Constants.EMPTY_STRING;
		this.extensionBanner = Constants.EMPTY_STRING;
		this.defaultRecommended = Constants.EMPTY_STRING;
		this.resourceLink = Constants.EMPTY_STRING;
		this.routeCar = Constants.EMPTY_STRING;
		this.nameCar= Constants.EMPTY_STRING;
		this.extensionCar = Constants.EMPTY_STRING;
		this.resourceDuration = Constants.EMPTY_STRING;
		this.resourceState= Constants.EMPTY_STRING;
		this.urlStartedResource = Constants.EMPTY_STRING;;
		this.codeStartedResource = Constants.EMPTY_STRING;
		
		this.resourceOrder= Constants.ZERO_INTEGER;
		this.numVotes= Constants.ZERO_INTEGER;
		this.totalScore= Constants.ZERO_INTEGER;
		this.totalView= Constants.ZERO_INTEGER;
		this.totalShare= Constants.ZERO_INTEGER;
		this.totalLike= Constants.ZERO_INTEGER;
		this.rating= Constants.ZERO_DOUBLE;
		
		this.categoryId= Constants.ZERO_LONG;
		this.subcategoryId= Constants.ZERO_LONG;
		this.formatId= Constants.ZERO_LONG;
		this.topicId= Constants.ZERO_LONG;
		this.specialityId= Constants.ZERO_LONG;
		this.extensionId=Constants.ZERO_LONG;
		//this.category = new Category();
		this.type=new Type();
		//this.subscription=new Subscription();
		this.resourceDetail=new ArrayList<>();
		this.flagImage=Constants.EMPTY_STRING;
		this.flagImage2=Constants.EMPTY_STRING;
		this.flagImage3=Constants.EMPTY_STRING;
		this.observation=Constants.EMPTY_STRING;
	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getResourceDescription() {
		return resourceDescription;
	}

	public void setResourceDescription(String resourceDescription) {
		this.resourceDescription = resourceDescription;
	}

	public String getRouteBanner() {
		return routeBanner;
	}

	public void setRouteBanner(String routeBanner) {
		this.routeBanner = routeBanner;
	}

	public String getNameBanner() {
		return nameBanner;
	}

	public void setNameBanner(String nameBanner) {
		this.nameBanner = nameBanner;
	}

	public String getExtensionBanner() {
		return extensionBanner;
	}

	public void setExtensionBanner(String extensionBanner) {
		this.extensionBanner = extensionBanner;
	}

	public String getDefaultRecommended() {
		return defaultRecommended;
	}

	public void setDefaultRecommended(String defaultRecommended) {
		this.defaultRecommended = defaultRecommended;
	}

	public String getResourceLink() {
		return resourceLink;
	}

	public void setResourceLink(String resourceLink) {
		this.resourceLink = resourceLink;
	}

	public String getRouteCar() {
		return routeCar;
	}

	public void setRouteCar(String routeCar) {
		this.routeCar = routeCar;
	}

	public String getNameCar() {
		return nameCar;
	}

	public void setNameCar(String nameCar) {
		this.nameCar = nameCar;
	}

	public String getExtensionCar() {
		return extensionCar;
	}

	public void setExtensionCar(String extensionCar) {
		this.extensionCar = extensionCar;
	}

	public String getResourceDuration() {
		return resourceDuration;
	}
	
	public String getResourceState() {
		return resourceState;
	}


	public void setResourceState(String resourceState) {
		this.resourceState = resourceState;
	}


	public void setResourceDuration(String resourceDuration) {
		this.resourceDuration = resourceDuration;
	}

	public String getUrlStartedResource() {
		return urlStartedResource;
	}

	public void setUrlStartedResource(String urlStartedResource) {
		this.urlStartedResource = urlStartedResource;
	}

	public String getCodeStartedResource() {
		return codeStartedResource;
	}

	public void setCodeStartedResource(String codeStartedResource) {
		this.codeStartedResource = codeStartedResource;
	}

	public Integer getResourceOrder() {
		return resourceOrder;
	}

	public void setResourceOrder(Integer resourceOrder) {
		this.resourceOrder = resourceOrder;
	}

	public Integer getNumVotes() {
		return numVotes;
	}

	public void setNumVotes(Integer numVotes) {
		this.numVotes = numVotes;
	}

	public Integer getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(Integer totalScore) {
		this.totalScore = totalScore;
	}

	public Integer getTotalView() {
		return totalView;
	}

	public void setTotalView(Integer totalView) {
		this.totalView = totalView;
	}

	public Integer getTotalShare() {
		return totalShare;
	}

	public void setTotalShare(Integer totalShare) {
		this.totalShare = totalShare;
	}

	public Integer getTotalLike() {
		return totalLike;
	}

	public void setTotalLike(Integer totalLike) {
		this.totalLike = totalLike;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	/*public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}*/

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	/*public Subscription getSubscription() {
		return subscription;
	}

	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}*/
	
	public Long getCategoryId() {
		return categoryId;
	}


	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}


	public Long getFormatId() {
		return formatId;
	}


	public void setFormatId(Long formatId) {
		this.formatId = formatId;
	}


	public Long getTopicId() {
		return topicId;
	}


	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}


	public Long getSpecialityId() {
		return specialityId;
	}


	public void setSpecialityId(Long specialityId) {
		this.specialityId = specialityId;
	}

	public List<ResourceDetail> getResourceDetail() {
		return resourceDetail;
	}

	public void setResourceDetail(List<ResourceDetail> resourceDetail) {
		this.resourceDetail = resourceDetail;
	}
	
}
