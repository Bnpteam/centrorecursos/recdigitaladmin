package pe.gob.bnp.recdigital.resource.dto;

import pe.gob.bnp.recdigital.resource.model.Resource;
import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class ResourceDto {
	
	private String id;
	private String title;
	private String subtitle;
	private String description;
	//private String link;
	private String linkCar;
	private String linkCar2;
	private String linkCar3;
	private String order;
	private String enabled;
	private String createdUser;
	private String updatedUser;	
	private String categoryId;
	private String subcategoryId;
	public String getSubcategoryId() {
		return subcategoryId;
	}

	public void setSubcategoryId(String subcategoryId) {
		this.subcategoryId = subcategoryId;
	}

	private String formatId;
	private String extensionId;
	//private String subscriptionId;
	private String observation;


	public ResourceDto() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.title = Constants.EMPTY_STRING;
		this.subtitle = Constants.EMPTY_STRING;
		this.description = Constants.EMPTY_STRING;
		
		//this.link = Constants.EMPTY_STRING;
		this.linkCar=Constants.EMPTY_STRING;
		this.linkCar2=Constants.EMPTY_STRING;
		this.linkCar3=Constants.EMPTY_STRING;
		this.order = Constants.EMPTY_STRING;
		this.enabled = Constants.EMPTY_STRING;
		this.createdUser=Constants.EMPTY_STRING;
		this.updatedUser=Constants.EMPTY_STRING;
		this.categoryId=Constants.EMPTY_STRING;
		this.subcategoryId=Constants.EMPTY_STRING;
		this.formatId=Constants.EMPTY_STRING;
		this.extensionId=Constants.EMPTY_STRING;
		this.observation=Constants.EMPTY_STRING;
		//this.subscriptionId=Constants.EMPTY_STRING;
	}	

	public ResourceDto(Resource resource) {
		super();
		this.id =Utility.parseLongToString(resource.getId());
		this.title = resource.getTitle();	
		this.subtitle= resource.getSubtitle();	
		this.description = resource.getResourceDescription();
		//this.link = resource.getResourceLink();	
		this.order = Utility.parseIntToString(resource.getResourceOrder());
		this.enabled = resource.getIdEnabled();
		this.createdUser=resource.getCreatedUser();
		this.updatedUser=resource.getUpdatedUser();
		this.categoryId= Utility.parseLongToString(resource.getCategoryId());
		this.subcategoryId= Utility.parseLongToString(resource.getSubcategoryId());
		this.formatId= Utility.parseLongToString(resource.getFormatId());
		this.extensionId= Utility.parseLongToString(resource.getExtensionId());
		//this.subscriptionId= Utility.parseLongToString(resource.getSubscription().getId());
		this.observation = resource.getObservation();
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}*/

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}	

	public String getFormatId() {
		return formatId;
	}

	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}

	/*public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}*/

	public String getLinkCar() {
		return linkCar;
	}

	public void setLinkCar(String linkCar) {
		this.linkCar = linkCar;
	}
	
	public String getLinkCar2() {
		return linkCar2;
	}

	public void setLinkCar2(String linkCar2) {
		this.linkCar2 = linkCar2;
	}

	public String getLinkCar3() {
		return linkCar3;
	}

	public void setLinkCar3(String linkCar3) {
		this.linkCar3 = linkCar3;
	}	

	
	public String getExtensionId() {
		return extensionId;
	}

	public void setExtensionId(String extensionId) {
		this.extensionId = extensionId;
	}
	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	
}
