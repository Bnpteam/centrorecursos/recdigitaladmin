package pe.gob.bnp.recdigital.resource.model;


import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.model.BaseEntity;

public class ResourceDetail extends BaseEntity{	
	
	private String resourceDetailTitle;
	private String resourceDetailDescription;
	private Integer resourceDetailOrder;
	private Resource resource;

	public ResourceDetail() {
		super();
		this.resourceDetailTitle = Constants.EMPTY_STRING;
		this.resourceDetailDescription = Constants.EMPTY_STRING;
		this.resourceDetailOrder = Constants.ZERO_INTEGER;
		this.resource = new Resource();
	}

	public String getResourceDetailTitle() {
		return resourceDetailTitle;
	}

	public void setResourceDetailTitle(String resourceDetailTitle) {
		this.resourceDetailTitle = resourceDetailTitle;
	}

	public String getResourceDetailDescription() {
		return resourceDetailDescription;
	}

	public void setResourceDetailDescription(String resourceDetailDescription) {
		this.resourceDetailDescription = resourceDetailDescription;
	}
	public Integer getResourceDetailOrder() {
		return resourceDetailOrder;
	}

	public void setResourceDetailOrder(Integer resourceDetailOrder) {
		this.resourceDetailOrder = resourceDetailOrder;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

}
