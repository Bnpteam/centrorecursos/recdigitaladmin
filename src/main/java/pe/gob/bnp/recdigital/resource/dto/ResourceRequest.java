package pe.gob.bnp.recdigital.resource.dto;

public class ResourceRequest {
	
	private String keyword;
	
	private String state;
	
	private String page;

	private String size;

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}	

}
