package pe.gob.bnp.recdigital.resource.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import pe.gob.bnp.recdigital.resource.dto.CourseDetailDto;
import pe.gob.bnp.recdigital.resource.dto.CourseDto;
import pe.gob.bnp.recdigital.resource.dto.ResourceDto;
import pe.gob.bnp.recdigital.resource.model.Resource;
import pe.gob.bnp.recdigital.resource.model.ResourceDetail;
import pe.gob.bnp.recdigital.utilitary.common.Utility;


@Component
public class ResourceMapper {
	
	public Resource reverseMapperSave(ResourceDto resourceDto) {
		Resource resource= new Resource();
		
		
		resource.setId(Utility.parseStringToLong(resourceDto.getId()));
		resource.setTitle(resourceDto.getTitle());
		resource.setSubtitle(resourceDto.getSubtitle());
		resource.setResourceDescription(resourceDto.getDescription());
		//resource.setResourceLink(resourceDto.getLink());
		
		resource.setResourceOrder(Utility.parseStringToInt(resourceDto.getOrder()));
		resource.setCreatedUser(resourceDto.getCreatedUser());
		resource.setCategoryId(Utility.parseStringToLong(resourceDto.getCategoryId()));
		resource.setFormatId(Utility.parseStringToLong(resourceDto.getFormatId()));
		resource.setExtensionId(Utility.parseStringToLong(resourceDto.getExtensionId()));
		resource.setObservation(resourceDto.getObservation());
		resource.setSubcategoryId(Utility.parseStringToLong(resourceDto.getSubcategoryId()));
		//resource.getSubscription().setId(Utility.parseStringToLong(resourceDto.getSubscriptionId()));
		return resource;
	}
	
	public Resource reverseMapperSaveImage(String name,String extension,String route,String id) {
		Resource resource= new Resource();
		ResourceDto resourceDto=new ResourceDto();
		
		resourceDto.setId(id);
		resource.setId(Utility.parseStringToLong(resourceDto.getId()));
		resource.setNameCar(name);;
		resource.setExtensionCar(extension);	
		resource.setRouteCar(route);
		return resource;
	}
	
	public Resource reverseMapperUpdate(ResourceDto resourceDto) {
		Resource resource= new Resource();
		
		
		resource.setId(Utility.parseStringToLong(resourceDto.getId()));	
		resource.setTitle(resourceDto.getTitle());
		resource.setSubtitle(resourceDto.getSubtitle());
		resource.setResourceDescription(resourceDto.getDescription());
		//.setResourceLink(resourceDto.getLink());		
		resource.setResourceOrder(Utility.parseStringToInt(resourceDto.getOrder()));		
		resource.setCategoryId(Utility.parseStringToLong(resourceDto.getCategoryId()));
		resource.setFormatId(Utility.parseStringToLong(resourceDto.getFormatId()));
		resource.setExtensionId(Utility.parseStringToLong(resourceDto.getExtensionId()));
		resource.setObservation(resourceDto.getObservation());
		resource.setSubcategoryId(Utility.parseStringToLong(resourceDto.getSubcategoryId()));
		//resource.getSubscription().setId(Utility.parseStringToLong(resourceDto.getSubscriptionId()));	
		resource.setIdEnabled(resourceDto.getEnabled());	
		resource.setUpdatedUser(resourceDto.getUpdatedUser());
		return resource;
	}
	
	/*
	 * course
	 */
	
	public Resource reverseMapperCourseSave(CourseDto courseDto) {
		Resource resource= new Resource();
		
		resource.setTitle(courseDto.getTitle());
		resource.setSubtitle(courseDto.getSubtitle());
		resource.setResourceDescription(courseDto.getDescription());
		resource.setResourceLink(courseDto.getLink());
		resource.setUrlStartedResource(courseDto.getLinkStarted());
		resource.setResourceDuration(courseDto.getDuration());
		resource.setResourceDetail(reverseMapperList(courseDto.getDetails()));
		resource.setResourceOrder(Utility.parseStringToInt(courseDto.getOrder()));
		resource.setCreatedUser(courseDto.getCreatedUser());
		resource.setTopicId(Utility.parseStringToLong(courseDto.getTopicId()));
		resource.setSpecialityId(Utility.parseStringToLong(courseDto.getSpecialityId()));
		//resource.getSubscription().setId(Utility.parseStringToLong(courseDto.getSubscriptionId()));
		return resource;
	}	
	
	public Resource reverseMapperUpdate(Long id,String nameCar,String nameBanner,String extension,String route,CourseDto courseDto) {
		Resource resource= new Resource();
		
		courseDto.setId(Utility.parseLongToString(id));
		resource.setId(Utility.parseStringToLong(courseDto.getId()));
		resource.setNameCar(nameCar);;
		resource.setExtensionCar(extension);	
		resource.setRouteCar(route);	
		resource.setNameBanner(nameBanner);;
		resource.setExtensionBanner(extension);	
		resource.setRouteBanner(route);			
		resource.setTitle(courseDto.getTitle());
		resource.setSubtitle(courseDto.getSubtitle());
		resource.setResourceDescription(courseDto.getDescription());
		resource.setResourceLink(courseDto.getLink());	
		resource.setUrlStartedResource(courseDto.getLinkStarted());
		resource.setResourceDuration(courseDto.getDuration());
		resource.setResourceDetail(reverseMapperList(courseDto.getDetails()));
		resource.setResourceOrder(Utility.parseStringToInt(courseDto.getOrder()));		
		//resource.getCategory().setId(Utility.parseStringToLong(courseDto.getCategoryId()));
		//resource.getSubscription().setId(Utility.parseStringToLong(courseDto.getSubscriptionId()));	
		resource.setIdEnabled(courseDto.getEnabled());	
		resource.setUpdatedUser(courseDto.getUpdatedUser());
		resource.setTopicId(Utility.parseStringToLong(courseDto.getTopicId()));
		resource.setSpecialityId(Utility.parseStringToLong(courseDto.getSpecialityId()));
		return resource;
	}	

	private List<ResourceDetail> reverseMapperList(List<CourseDetailDto> detailsDto){
		List<ResourceDetail> details=new ArrayList<>();	
		
		for(CourseDetailDto detail:detailsDto) {
			ResourceDetail resourceDetail= new ResourceDetail();
			resourceDetail.setResourceDetailTitle(detail.getTitle());
			resourceDetail.setResourceDetailDescription(detail.getDescription());
			resourceDetail.setResourceDetailOrder(Utility.parseStringToInt(detail.getOrder()));
			details.add(resourceDetail);
		}		
		
		return details;
		
	}
	
	public Resource reverseMapperSaveImage(String nameCar, String nameBanner,String extension,String route,String id) {
		Resource resource= new Resource();
		ResourceDto resourceDto=new ResourceDto();
		
		resourceDto.setId(id);
		resource.setId(Utility.parseStringToLong(resourceDto.getId()));
		
		resource.setNameCar(nameCar);
		resource.setExtensionCar(extension);	
		resource.setRouteCar(route);
		
		resource.setNameBanner(nameBanner);
		resource.setExtensionBanner(extension);	
		resource.setRouteBanner(route);
		return resource;
	}
	


}
