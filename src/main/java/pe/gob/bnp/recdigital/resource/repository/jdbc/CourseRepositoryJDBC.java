package pe.gob.bnp.recdigital.resource.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigital.resource.dto.CourseDetailResponse;
import pe.gob.bnp.recdigital.resource.dto.CourseResponse;
import pe.gob.bnp.recdigital.resource.dto.ListCourseResponse;
import pe.gob.bnp.recdigital.resource.dto.ResourceRequest;
import pe.gob.bnp.recdigital.resource.model.Resource;
import pe.gob.bnp.recdigital.resource.model.ResourceDetail;
import pe.gob.bnp.recdigital.resource.repository.CourseRepository;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;
import pe.gob.bnp.recdigital.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class CourseRepositoryJDBC extends BaseJDBCOperation implements CourseRepository {

	private static final Logger logger = LoggerFactory.getLogger(CourseRepositoryJDBC.class);

	private String messageSuccess = "Transacción exitosa.";

	@Override
	public ResponseTransaction persist(Resource entity) {
		Connection cn = null;
		String dBTransaction = "{ Call PKG_API_RESOURCE.SP_PERSIST_COURSE(?,?,?,?,?,?,?,?,?,?,?,?)}";/* 12 */
		ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);

			cstm.setString("P_TITLE", Utility.getString(entity.getTitle()));
			cstm.setString("P_SUBTITLE", Utility.getString(entity.getSubtitle()));
			cstm.setString("P_DESCRIPTION", Utility.getString(entity.getResourceDescription()));
			cstm.setString("P_LINK", Utility.getString(entity.getResourceLink()));
			cstm.setString("P_LINK_STARTED", Utility.getString(entity.getUrlStartedResource()));
			cstm.setString("P_DURATION", Utility.getString(entity.getResourceDuration()));
			cstm.setInt("P_ORDER", entity.getResourceOrder());
			cstm.setLong("P_CREATED_USER", Utility.parseStringToLong(entity.getCreatedUser()));
			cstm.setLong("P_TOPIC_ID", entity.getTopicId());
			cstm.setLong("P_SPECIALITY_ID", entity.getSpecialityId());
			// cstm.setLong("P_SUBSCRIPTION_ID", entity.getSubscription().getId());
			cstm.registerOutParameter("S_RESOURCE_ID", OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id = Utility.parseObjectToLong(cstm.getObject("S_RESOURCE_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			} else {
				cn.rollback();
			}
		} catch (SQLException e) {
			logger.error("PKG_API_RESOURCE.SP_PERSIST_COURSE: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));
				response.setResponse(e1.getMessage());
			}
		} finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);
		}
		return response;
	}

	@Override
	public ResponseTransaction persistDetail(Long idCourse, ResourceDetail entity) {
		Connection cn = null;
		String dBTransaction = "{ Call PKG_API_RESOURCE.SP_PERSIST_COURSE_DET(?,?,?,?,?,?)}";
		ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);

			logger.info("{ Call PKG_API_RESOURCE.SP_PERSIST_COURSE_DET(?,?,?,?,?,?)}");

			logger.info("P_COURSE_ID " + idCourse);
			logger.info("P_TITLE " + Utility.getString(entity.getResourceDetailTitle()));
			logger.info("P_DESCRIPTION " + Utility.getString(entity.getResourceDetailDescription()));
			logger.info("P_ORDER " + entity.getResourceDetailOrder());

			cstm.setLong("P_COURSE_ID", idCourse);
			cstm.setString("P_TITLE", Utility.getString(entity.getResourceDetailTitle()));
			cstm.setString("P_DESCRIPTION", Utility.getString(entity.getResourceDetailDescription()));
			cstm.setInt("P_ORDER", entity.getResourceDetailOrder());
			cstm.registerOutParameter("S_COURSE_DET_ID", OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id = Utility.parseObjectToLong(cstm.getObject("S_COURSE_DET_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			} else {
				cn.rollback();
				System.out.println("Error: No se pudo insertar el registro detalle del curso");
				response.setCodeResponse("0007");
				response.setResponse("Error: No se pudo insertar el registro detalle del curso");
			}
		} catch (SQLException e) {
			logger.error("SP_PERSIST_COURSE_DET: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));
				response.setResponse(e1.getMessage());
			}
		} finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);
		}
		return response;
	}

	@Override
	public ResponseTransaction update(Resource entity) {
		Connection cn = null;
		String dBTransaction = "{ Call PKG_API_RESOURCE.SP_UPDATE_COURSE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";/*20*/
		ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);			

			logger.info("P_ID " + entity.getId());
			logger.info("P_TITLE " + Utility.getString(entity.getTitle()));
			logger.info("P_SUBTITLE " + Utility.getString(entity.getSubtitle()));
			logger.info("P_DESCRIPTION " + Utility.getString(entity.getResourceDescription()));
			logger.info("P_LINK " + Utility.getString(entity.getResourceLink()));
			logger.info("P_LINK_STARTED", Utility.getString(entity.getUrlStartedResource()));
			logger.info("P_DURATION " + Utility.getString(entity.getResourceDuration()));
			logger.info("P_NAMECAR " + Utility.getString(entity.getNameCar()));
			logger.info("P_EXTENSIONCAR " + Utility.getString(entity.getExtensionCar()));
			logger.info("P_ROUTECAR " + Utility.getString(entity.getRouteCar()));
			logger.info("P_NAMEBANNER " + Utility.getString(entity.getNameBanner()));
			logger.info("P_EXTENSIONBANNER " + Utility.getString(entity.getExtensionBanner()));
			logger.info("P_ROUTEBANNER " + Utility.getString(entity.getRouteBanner()));
			logger.info("P_ORDER " + entity.getResourceOrder());
			logger.info("P_ENABLED " + Utility.getString(entity.getIdEnabled()));
			logger.info("P_UPDATED_USER " + Utility.parseStringToLong(entity.getUpdatedUser()));
			logger.info("P_TOPIC_ID " + entity.getTopicId());
			// logger.info("P_SUBSCRIPTION_ID "+ entity.getSubscription().getId());

			cstm.setLong("P_ID", entity.getId());
			cstm.setString("P_TITLE", Utility.getString(entity.getTitle()));
			cstm.setString("P_SUBTITLE", Utility.getString(entity.getSubtitle()));
			cstm.setString("P_DESCRIPTION", Utility.getString(entity.getResourceDescription()));
			cstm.setString("P_LINK", Utility.getString(entity.getResourceLink()));
			cstm.setString("P_LINK_STARTED", Utility.getString(entity.getUrlStartedResource()));
			cstm.setString("P_DURATION", Utility.getString(entity.getResourceDuration()));
			cstm.setString("P_NAMECAR", Utility.getString(entity.getNameCar()));
			cstm.setString("P_EXTENSIONCAR", Utility.getString(entity.getExtensionCar()));
			cstm.setString("P_ROUTECAR", Utility.getString(entity.getRouteCar()));
			cstm.setString("P_NAMEBANNER", Utility.getString(entity.getNameBanner()));
			cstm.setString("P_EXTENSIONBANNER", Utility.getString(entity.getExtensionBanner()));
			cstm.setString("P_ROUTEBANNER", Utility.getString(entity.getRouteBanner()));
			cstm.setInt("P_ORDER", entity.getResourceOrder());
			cstm.setString("P_ENABLED", Utility.getString(entity.getIdEnabled()));
			cstm.setLong("P_UPDATED_USER", Utility.parseStringToLong(entity.getUpdatedUser()));
			cstm.setLong("P_TOPIC_ID", entity.getTopicId());
			cstm.setLong("P_SPECIALITY_ID", entity.getSpecialityId());
			// cstm.setLong("P_SUBSCRIPTION_ID", entity.getSubscription().getId());
			cstm.registerOutParameter("S_COURSE_ID", OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);

			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id = Utility.parseObjectToLong(cstm.getObject("S_COURSE_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			} else {
				cn.rollback();
			}
		} catch (SQLException e) {
			logger.error("PKG_API_RESOURCE.SP_UPDATE_COURSE " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));
				response.setResponse(e1.getMessage());
			}
		} finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);
		}
		return response;
	}

	@Override
	public ResponseTransaction persistImage(Resource entity) {
		Connection cn = null;
		String dBTransaction = "{ Call PKG_API_RESOURCE.SP_PERSIST_IMAGE_CARBAN(?,?,?,?,?,?,?,?,?)}";/* 9 */
		ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);

			cstm.setLong("P_ID", entity.getId());
			cstm.setString("P_NAMECAR", Utility.getString(entity.getNameCar()));
			cstm.setString("P_EXTENSIONCAR", Utility.getString(entity.getExtensionCar()));
			cstm.setString("P_ROUTECAR", Utility.getString(entity.getRouteCar()));
			cstm.setString("P_NAMEBANNER", Utility.getString(entity.getNameBanner()));
			cstm.setString("P_EXTENSIONBANNER", Utility.getString(entity.getExtensionBanner()));
			cstm.setString("P_ROUTEBANNER", Utility.getString(entity.getRouteBanner()));
			cstm.registerOutParameter("S_RESOURCE_ID", OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id = Utility.parseObjectToLong(cstm.getObject("S_RESOURCE_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			} else {
				cn.rollback();
			}
		} catch (SQLException e) {
			logger.error("SP_PERSIST_IMAGE_CARBAN: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));
				response.setResponse(e1.getMessage());
			}
		} finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);
		}
		return response;
	}

	@Override
	public ResponseTransaction list(ResourceRequest resourceRequest) {
		Connection cn = null;
		String dBTransaction = "{ Call PKG_API_RESOURCE.SP_LIST_COURSE(?,?,?,?,?,?)}";

		List<Object> resourcesResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();

		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);

			cstm.setString("P_KEYWORD", Utility.getString(resourceRequest.getKeyword()));
			cstm.setString("P_STATE", Utility.getString(resourceRequest.getState()));
			cstm.setInt("P_PAGE", Utility.parseStringToInt(resourceRequest.getPage()));	
			cstm.setInt("P_SIZE", Utility.parseStringToInt(resourceRequest.getSize()));	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					ListCourseResponse resourceResponse = new ListCourseResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("COURSE_ID")));
					resourceResponse.setTitle(Utility.getString(rs.getString("COURSE_TITLE")));
					resourceResponse.setDescription(Utility.getString(rs.getString("COURSE_DESCRIPTION")));
					resourceResponse.setTopicId(Utility.parseLongToString(rs.getLong("COURSE_TOPIC_ID")));
					resourceResponse.setTopic(Utility.getString(rs.getString("COURSE_TOPIC")));
					resourceResponse.setSpecialityId(Utility.parseLongToString(rs.getLong("COURSE_SPECIALITY_ID")));
					resourceResponse.setSpeciality(Utility.getString(rs.getString("COURSE_SPECIALITY")));
					resourceResponse.setStateId(Utility.getString(rs.getString("COURSE_STATE_ID")));
					resourceResponse.setState(Utility.getString(rs.getString("COURSE_STATE")));
					resourceResponse.setEnabled(Utility.getString(rs.getString("COURSE_ENABLED")));
					resourceResponse.setCreatedUser(Utility.getString(rs.getString("CREATED_USER")));
					resourceResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
					resourceResponse.setTotalRecords(Utility.getString(rs.getString("TOTAL_RECORDS")));
					
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
		} catch (SQLException e) {
			logger.error(" PKG_API_RESOURCE.SP_LIST_COURSE: " + e.getMessage());			
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));
				response.setResponse(e1.getMessage());
			}
		} finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);
		}
		return response;
	}

	@Override
	public ResponseTransaction read(Long id) {
		Connection cn = null;
		String dBTransaction = "{ Call PKG_API_RESOURCE.SP_GET_COURSE(?,?,?,?)}";

		CourseResponse courseResponse = new CourseResponse();
		List<Object> resourcesResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();

		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_COURSE_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL", OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_DETAIL", OracleTypes.CURSOR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {

					courseResponse.setId(Utility.parseLongToString(rs.getLong("COURSE_ID")));
					courseResponse.setTitle(Utility.getString(rs.getString("COURSE_TITLE")));
					courseResponse.setDescription(Utility.getString(rs.getString("COURSE_DESCRIPTION")));
					courseResponse.setTopicId(Utility.parseLongToString(rs.getLong("COURSE_TOPIC_ID")));
					courseResponse.setTopic(Utility.getString(rs.getString("COURSE_TOPIC")));
					courseResponse.setSpecialityId(Utility.parseLongToString(rs.getLong("COURSE_SPECIALITY_ID")));
					courseResponse.setSpeciality(Utility.getString(rs.getString("COURSE_SPECIALITY")));
					//courseResponse.setCategoryId(Utility.getString(rs.getString("CATEGORY_ID")));
					//courseResponse.setSubcategoryId(Utility.getString(rs.getString("SUBCATEGORY_ID")));
					//courseResponse.setCategory(Utility.getString(rs.getString("RESOURCE_CATEGORY")));
					courseResponse.setLink(Utility.getString(rs.getString("COURSE_LINK")));
					courseResponse.setLinkStarted(Utility.getString(rs.getString("STARTED_LINK")));
					courseResponse.setDuration(Utility.getString(rs.getString("COURSE_DURATION")));
					courseResponse.setLinkCar(Utility.getString(rs.getString("CAR_LINK")));
					courseResponse.setLinkBanner(Utility.getString(rs.getString("BANNER_LINK")));
					courseResponse.setStateId(Utility.getString(rs.getString("COURSE_STATE_ID")));
					courseResponse.setState(Utility.getString(rs.getString("COURSE_STATE")));
					// courseResponse.setSubscriptionId(Utility.parseLongToString(rs.getLong("SUBSCRIPTION_ID")));
					// courseResponse.setSubscription(Utility.getString(rs.getString("SUBSCRIPTION_NAME")));
					courseResponse.setEnabled(Utility.getString(rs.getString("COURSE_ENABLED")));
					courseResponse.setCreatedUser(Utility.getString(rs.getString("CREATED_USER")));
					courseResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));

				}
				this.closeResultSet(rs);

				rs = (ResultSet) cstm.getObject("S_C_CURSOR_DETAIL");
				List<CourseDetailResponse> detailsDto = new ArrayList<>();
				while (rs.next()) {
					CourseDetailResponse entidad = new CourseDetailResponse();

					entidad.setTitle(Utility.getString(rs.getString("COURSE_DETAIL_TITLE")));
					entidad.setDescription(Utility.getString(rs.getString("COURSE_DETAIL_DES")));
					entidad.setOrder(Utility.parseLongToString(rs.getLong("COURSE_DETAIL_ORDER")));
					detailsDto.add(entidad);
				}

				courseResponse.setDetails(detailsDto);

				if (courseResponse != null && !Utility.isEmptyOrNull(courseResponse.getTitle())) {
					resourcesResponse.add(courseResponse);

				}

				response.setList(resourcesResponse);
			}
		} catch (SQLException e) {
			logger.error(" PKG_API_RESOURCE.SP_GET_COURSE: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));
				response.setResponse(e1.getMessage());
			}
		} finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);
		}
		return response;
	}

}
