package pe.gob.bnp.recdigital.resource.dto;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.recdigital.utilitary.common.Constants;

public class CourseResponse {
	
	private String id;
	private String title;
	private String description;
	private String topicId;	
	private String topic;	
	private String specialityId;
	private String speciality;	
	//private String categoryId;
	//private String subcategoryId;
	//private String category;
	private String stateId;
	private String state;
	//private String subscriptionId;	
	//private String subscription;
	private String link;
	private String linkStarted;		
	private String duration;	
	private String enabled;	
	private String createdUser;
	private String createdDate;
	private String linkCar;	
	private String linkBanner;
	private List<CourseDetailResponse> details;
	
	
	public CourseResponse() {
		super();
		this.id=Constants.EMPTY_STRING;
		this.title=Constants.EMPTY_STRING;
		this.description=Constants.EMPTY_STRING;
		//this.categoryId=Constants.EMPTY_STRING;
		//this.subcategoryId=Constants.EMPTY_STRING;
		
		//this.category=Constants.EMPTY_STRING;
		this.stateId=Constants.EMPTY_STRING;
		this.state=Constants.EMPTY_STRING;
		//this.subscriptionId=Constants.EMPTY_STRING;
		//this.subscription=Constants.EMPTY_STRING;
		this.link=Constants.EMPTY_STRING;	
		this.linkStarted=Constants.EMPTY_STRING;
		this.topicId=Constants.EMPTY_STRING;
		this.topic=Constants.EMPTY_STRING;
		this.specialityId=Constants.EMPTY_STRING;
		this.speciality=Constants.EMPTY_STRING;
		
		this.duration=Constants.EMPTY_STRING;	
		this.enabled=Constants.EMPTY_STRING;
		this.createdUser=Constants.EMPTY_STRING;
		this.createdDate=Constants.EMPTY_STRING;
		this.linkCar=Constants.EMPTY_STRING;
		this.linkBanner=Constants.EMPTY_STRING;
		this.details=new ArrayList<>();
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getTopicId() {
		return topicId;
	}
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	public String getSpecialityId() {
		return specialityId;
	}
	public void setSpecialityId(String specialityId) {
		this.specialityId = specialityId;
	}	
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}
	
	/*public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getSubcategoryId() {
		return subcategoryId;
	}
	public void setSubcategoryId(String subcategoryId) {
		this.subcategoryId = subcategoryId;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}*/
	
	public String getStateId() {
		return stateId;
	}
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	/*public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getSubscription() {
		return subscription;
	}

	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}*/
	
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
	public String getLinkStarted() {
		return linkStarted;
	}

	public void setLinkStarted(String linkStarted) {
		this.linkStarted = linkStarted;
	}
	
	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	public String getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getLinkCar() {
		return linkCar;
	}
	public void setLinkCar(String linkCar) {
		this.linkCar = linkCar;
	}
	public String getLinkBanner() {
		return linkBanner;
	}
	public void setLinkBanner(String linkBanner) {
		this.linkBanner = linkBanner;
	}
	public List<CourseDetailResponse> getDetails() {
		return details;
	}
	public void setDetails(List<CourseDetailResponse> details) {
		this.details = details;
	}	

}
