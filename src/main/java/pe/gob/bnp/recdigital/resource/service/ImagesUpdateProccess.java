package pe.gob.bnp.recdigital.resource.service;

import java.net.MalformedURLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;


public class ImagesUpdateProccess extends Thread {
	
	private static final Logger logger = LoggerFactory.getLogger(ImagesUpdateProccess.class);

	private String nameFile;
	private String pathDirectory;
	private byte[] bytesFile;
	private NtlmPasswordAuthentication authentication;

	public ImagesUpdateProccess(String nameFile, String pathDirectory, byte[] bytesFile,NtlmPasswordAuthentication authentication) {
		super();
		this.nameFile = nameFile;
		this.pathDirectory = pathDirectory;
		this.bytesFile = bytesFile;
		this.authentication = authentication;
	}

	public String getNameFile() {
		return nameFile;
	}

	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}

	public String getPathDirectory() {
		return pathDirectory;
	}

	public void setPathDirectory(String pathDirectory) {
		this.pathDirectory = pathDirectory;
	}

	public byte[] getBytesFile() {
		return bytesFile;
	}

	public void setBytesFile(byte[] bytesFile) {
		this.bytesFile = bytesFile;
	}

	public NtlmPasswordAuthentication getAuthentication() {
		return authentication;
	}

	public void setAuthentication(NtlmPasswordAuthentication authentication) {
		this.authentication = authentication;
	}

	@Override
	public void run() {

		try {
			// car
			String fileName = "/"+nameFile+ ".jpg";
			String pathFile = pathDirectory + fileName;
			SmbFile smbFileCar = new SmbFile(pathFile, authentication);
			logger.info("path file imagen. Update "+pathFile);
			SmbFileOutputStream smbfos;
			if(smbFileCar.exists()) {
				logger.info("path file imagen delete. ");
				smbFileCar.delete();
			}
			smbfos = new SmbFileOutputStream(smbFileCar);
			smbfos.write(bytesFile);
			smbfos.flush();
			smbfos.close();

		} catch (MalformedURLException ex) {			
			logger.error("Error MalformedURLException Update: "+ex.getMessage());

		} catch (Exception ex) {
			logger.error("Error MalformedURLException Exception Update: "+ex.getMessage());
		}

	}

}
