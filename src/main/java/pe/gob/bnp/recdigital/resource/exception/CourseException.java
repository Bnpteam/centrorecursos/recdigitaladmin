package pe.gob.bnp.recdigital.resource.exception;

import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;

public class CourseException {
	
	public static final String error9999 = "Error 9999: No se ejecutó ninguna transacción.";
	public static final String error0001 = "Error 0001: El titulo del curso ya se encuentra registrado.";
	//public static final String error0002 = "Error 0002: La categoria asociada no encontrada.";
	//public static final String error0003 = "Error 0003: La subscripcion asociada no encontrada.";
	public static final String error0003 = "Error 0003: El formato del enlace al aula virtual es incorrecto.";
	
	public static final String error0004 = "Error 0004: La imagen del car no se pude registrar porque no se encontro el curso.";
	public static final String error0007 = "Error 0007: La imagen del banner no se pude registrar porque no se encontro el curso.";
	public static final String error0005 = "Error 0005: Problemas al guardar imagen car en el servidor.";
	public static final String error0008 = "Error 0008: Problemas al guardar imagen banner en el servidor.";
	public static final String error0004Act = "Error 0004: No se puede realizar la actualización: El id del Curso no pertenece a ningun curso registrado.";

	public static ResponseTransaction setMessageResponseSave(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0001")) {
			response.setResponse(error0001);
		}

		/*if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}*/

		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003);
		}

		return response;
	}

	public static ResponseTransaction setMessageResponseSaveImageCar(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0004")) {
			response.setResponse(error0004);
		}

		if (response.getCodeResponse().equals("0005")) {
			response.setResponse(error0005);
		}

		return response;
	}

	public static ResponseTransaction setMessageResponseUpdate(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0001")) {
			response.setResponse(error0001);
		}

		/*if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}*/

		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003);
		}
		
		if (response.getCodeResponse().equals("0004")) {
			response.setResponse(error0004Act);
		}		
		

		return response;
	}

}
