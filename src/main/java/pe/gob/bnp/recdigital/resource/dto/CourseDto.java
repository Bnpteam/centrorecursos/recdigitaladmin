package pe.gob.bnp.recdigital.resource.dto;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.recdigital.resource.model.Resource;
import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class CourseDto {
	
	private String id;
	private String title;
	private String subtitle;
	private String description;
	private String link;
	private String linkStarted;
	private String duration;	
	private String linkCar;
	private String linkBanner;
	private String topicId;
	private String specialityId;
	private String order;
	private String enabled;
	private String createdUser;
	private String updatedUser;	
	//private String categoryId;
	//private String subscriptionId;
	private List<CourseDetailDto> details;
	
	public CourseDto() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.title = Constants.EMPTY_STRING;
		this.subtitle = Constants.EMPTY_STRING;
		this.description = Constants.EMPTY_STRING;
		
		this.link = Constants.EMPTY_STRING;
		this.linkStarted=Constants.EMPTY_STRING;
		this.duration=Constants.EMPTY_STRING;
		this.linkCar=Constants.EMPTY_STRING;
		this.linkBanner=Constants.EMPTY_STRING;
		this.order = Constants.EMPTY_STRING;
		this.enabled = Constants.EMPTY_STRING;
		this.createdUser=Constants.EMPTY_STRING;
		this.updatedUser=Constants.EMPTY_STRING;
		//this.categoryId=Constants.EMPTY_STRING;
		//this.subscriptionId=Constants.EMPTY_STRING;
		this.topicId=Constants.EMPTY_STRING;
		this.specialityId=Constants.EMPTY_STRING;
		this.details=new ArrayList<>();
	}	

	public CourseDto(Resource resource) {
		super();
		this.id =Utility.parseLongToString(resource.getId());
		this.title = resource.getTitle();	
		this.subtitle= resource.getSubtitle();	
		this.description = resource.getResourceDescription();
		this.link = resource.getResourceLink();	
		this.linkStarted=resource.getUrlStartedResource();
		this.duration=resource.getResourceDuration();
		this.order = Utility.parseIntToString(resource.getResourceOrder());
		this.enabled = resource.getIdEnabled();
		this.createdUser=resource.getCreatedUser();
		this.updatedUser=resource.getUpdatedUser();
		this.topicId=Utility.parseLongToString(resource.getTopicId());
		this.specialityId=Utility.parseLongToString(resource.getSpecialityId());
		//this.categoryId= Utility.parseLongToString(resource.getCategory().getId());
		//this.subscriptionId= Utility.parseLongToString(resource.getSubscription().getId());
		this.details=new ArrayList<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
	public String getLinkStarted() {
		return linkStarted;
	}

	public void setLinkStarted(String linkStarted) {
		this.linkStarted = linkStarted;
	}
	
	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getLinkCar() {
		return linkCar;
	}

	public void setLinkCar(String linkCar) {
		this.linkCar = linkCar;
	}

	public String getLinkBanner() {
		return linkBanner;
	}

	public void setLinkBanner(String linkBanner) {
		this.linkBanner = linkBanner;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}
	
	public String getTopicId() {
		return topicId;
	}

	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}

	public String getSpecialityId() {
		return specialityId;
	}

	public void setSpecialityId(String specialityId) {
		this.specialityId = specialityId;
	}

	/*public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}*/

	/*public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}*/
	
	public List<CourseDetailDto> getDetails() {
		return details;
	}

	public void setDetails(List<CourseDetailDto> details) {
		this.details = details;
	}

}
