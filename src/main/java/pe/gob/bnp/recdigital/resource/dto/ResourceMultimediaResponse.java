package pe.gob.bnp.recdigital.resource.dto;

public class ResourceMultimediaResponse {
	
	private String link;
	private String routeFolder;
	private String nameFolder;
	
	
	public String getRouteFolder() {
		return routeFolder;
	}
	public void setRouteFolder(String routeFolder) {
		this.routeFolder = routeFolder;
	}
	public String getNameFolder() {
		return nameFolder;
	}
	public void setNameFolder(String nameFolder) {
		this.nameFolder = nameFolder;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
	
	

}
