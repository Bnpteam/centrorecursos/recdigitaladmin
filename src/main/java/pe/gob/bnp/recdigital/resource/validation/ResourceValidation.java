package pe.gob.bnp.recdigital.resource.validation;

import org.springframework.web.multipart.MultipartFile;

import pe.gob.bnp.recdigital.resource.dto.ResourceDto;
import pe.gob.bnp.recdigital.resource.dto.ResourceRequest;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class ResourceValidation {

	private static String messageResourceSave = "No se encontraron datos del Recurso";
	private static String messageResourceSearchAll = "No se encontró el filtro de búsqueda";
	//private static String messageSubscriptionIdSave = "No se encontro id de subscripcion";
	private static String messageCategoryIdSave = "No se encontro id de categoria";
	private static String messageFormatIdSave = "No se encontro id de formato";
	private static String messageTitleSave = "No se encontro titulo de recurso";
	private static String messageResourceGet = "El id debe ser mayor a cero";
	private static String messageResourceImageSave = "No se encontro imagen car";
	private static String messageResourceIdSave = "No se encontro id del recurso";	
	private static String messageCourseListPage= "Se debe ingresar el nro. de página";
	private static String messageCourseListSize= "Se debe ingresar el nro. de registros";

	public static Notification validation(ResourceDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageResourceSave);
			return notification;
		}

		/*if (Utility.isEmptyOrNull(dto.getSubscriptionId())) {
			notification.addError(messageSubscriptionIdSave);
		}*/
		
		if (Utility.isEmptyOrNull(dto.getCategoryId())) {
			notification.addError(messageCategoryIdSave);
		}
		
		if (Utility.isEmptyOrNull(dto. getFormatId())) {
			notification.addError(messageFormatIdSave);
		}
		
		if (Utility.isEmptyOrNull(dto.getTitle())) {
			notification.addError(messageTitleSave);
		}

		return notification;
	}

	public static Notification validation(ResourceRequest dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageResourceSearchAll);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getPage())) {
			notification.addError(messageCourseListPage);
		}
		
		if (Utility.isEmptyOrNull(dto.getSize())) {
			notification.addError(messageCourseListSize);
		}

		return notification;
	}
	
	public static Notification validation(MultipartFile file,String id) {

		Notification notification = new Notification();

		if (file == null || file.isEmpty()) {
			notification.addError(messageResourceImageSave);
			return notification;
		}

		if (Utility.isEmptyOrNull(id)) {
			notification.addError(messageResourceIdSave);
		}	
	

		return notification;
	}
	
	public static Notification validation(Long id) {
		Notification notification = new Notification();
		if (id <=0L) {
			notification.addError(messageResourceGet);
			return notification;
		}		
		return notification;
	}	

}
