package pe.gob.bnp.recdigital.config.shared.dto;

public class SharedResponse {
	
	private String id;
	private String domain;
	private String user;
	private String password;
	private String server;
	private String http;	
	private String folder;
	private String resourceFolder;
	private String courseFolder;
	private String resourceMultimedia;
		
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	public String getResourceFolder() {
		return resourceFolder;
	}
	public void setResourceFolder(String resourceFolder) {
		this.resourceFolder = resourceFolder;
	}
	public String getCourseFolder() {
		return courseFolder;
	}
	public void setCourseFolder(String courseFolder) {
		this.courseFolder = courseFolder;
	}
	public String getHttp() {
		return http;
	}
	public void setHttp(String http) {
		this.http = http;
	}
	
	public String getResourceMultimedia() {
		return resourceMultimedia;
	}
	public void setResourceMultimedia(String resourceMultimedia) {
		this.resourceMultimedia = resourceMultimedia;
	}

}
