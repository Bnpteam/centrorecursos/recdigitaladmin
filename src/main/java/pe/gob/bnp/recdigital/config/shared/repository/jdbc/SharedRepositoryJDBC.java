package pe.gob.bnp.recdigital.config.shared.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigital.config.shared.dto.SharedResponse;
import pe.gob.bnp.recdigital.config.shared.repository.SharedRepository;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;
import pe.gob.bnp.recdigital.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class SharedRepositoryJDBC  extends BaseJDBCOperation implements SharedRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(SharedRepositoryJDBC.class);

	private String messageSuccess="Transacción exitosa.";
	
	@Override
	public ResponseTransaction read() {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_CONFIG.SP_GET_SHARED_DIRECTORY(?,?)}";
        
        List<Object> sharedsResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					SharedResponse sharedResponse = new SharedResponse();
					sharedResponse.setId(Utility.parseLongToString(rs.getLong("DIRECTORY_ID")));
					sharedResponse.setDomain(Utility.getString(rs.getString("DIRECTORY_DOMAIN")));
					sharedResponse.setUser(Utility.getString(rs.getString("DIRECTORY_USER")));
					sharedResponse.setPassword(Utility.getString(rs.getString("DIRECTORY_PASSWORD")));
					sharedResponse.setServer(Utility.getString(rs.getString("DIRECTORY_SERVER")));
					sharedResponse.setHttp(Utility.getString(rs.getString("DIRECTORY_HTTP")));
					sharedResponse.setFolder(Utility.getString(rs.getString("DIRECTORY_FOLDER")));
					sharedResponse.setResourceFolder(Utility.getString(rs.getString("RESOURCE_FOLDER")));
					sharedResponse.setCourseFolder(Utility.getString(rs.getString("COURSE_FOLDER")));
				    sharedResponse.setResourceMultimedia(Utility.getString(rs.getString("RESOURCE_MULTIMEDIA")));
					sharedsResponse.add(sharedResponse);
				}	
				response.setList(sharedsResponse);					
			}
		}catch(SQLException e) {
			logger.error("PKG_API_CONFIG.SP_GET_SHARED_DIRECTORY: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

}
