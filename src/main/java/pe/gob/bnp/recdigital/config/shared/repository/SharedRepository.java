package pe.gob.bnp.recdigital.config.shared.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;

@Repository
public interface SharedRepository {
	
	public ResponseTransaction read();

}
