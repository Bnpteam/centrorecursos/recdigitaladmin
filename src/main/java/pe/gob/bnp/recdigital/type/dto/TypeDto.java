package pe.gob.bnp.recdigital.type.dto;

import pe.gob.bnp.recdigital.type.model.Type;
import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class TypeDto {
	
	private String id;
	private String name;
	private String description;
	private String order;
	private String enabled;
	private String createdUser;
	private String updatedUser;
	
	public TypeDto() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.name = Constants.EMPTY_STRING;
		this.description = Constants.EMPTY_STRING;
		this.order = Constants.EMPTY_STRING;
		this.enabled = Constants.EMPTY_STRING;
		this.createdUser=Constants.EMPTY_STRING;
		this.updatedUser=Constants.EMPTY_STRING;
	}	

	public TypeDto(Type type) {
		super();
		this.id =Utility.parseLongToString(type.getId());
		this.name = type.getName();
		this.description = type.getTypeDescription();
		this.order = Utility.parseIntToString(type.getTypeOrder());
		this.enabled = type.getIdEnabled();
		this.createdUser=type.getCreatedUser();
		this.updatedUser=type.getUpdatedUser();
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	public String getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}
	public String getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}	
	

}
