package pe.gob.bnp.recdigital.type.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.recdigital.type.dto.TypeRequest;
import pe.gob.bnp.recdigital.type.model.Type;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.repository.BaseRepository;

@Repository
public interface TypeRepository extends BaseRepository<Type>{	

	public ResponseTransaction list(TypeRequest typeRequest);
	public ResponseTransaction read(Long id);
}
