package pe.gob.bnp.recdigital.type.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigital.type.dto.TypeRequest;
import pe.gob.bnp.recdigital.type.dto.TypeResponse;
import pe.gob.bnp.recdigital.type.model.Type;
import pe.gob.bnp.recdigital.type.repository.TypeRepository;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;
import pe.gob.bnp.recdigital.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class TypeRepositoryJDBC extends BaseJDBCOperation implements TypeRepository{
	
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction persist(Type entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_TYPE.SP_PERSIST_TYPE(?,?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setString("P_NAME", Utility.getString(entity.getName()));
			cstm.setString("P_DESCRIPTION",Utility.getString(entity.getTypeDescription()));
			cstm.setInt("P_ORDER",entity.getTypeOrder());
			cstm.setLong("P_CREATED_USER", Utility.parseStringToLong(entity.getCreatedUser()));			
			cstm.registerOutParameter("S_TYPE_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_TYPE_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction update(Type entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_TYPE.SP_UPDATE_TYPE(?,?,?,?,?,?,?,?)}";/*8*/
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_ID", entity.getId());
			cstm.setString("P_NAME", Utility.getString(entity.getName()));
			cstm.setString("P_DESCRIPTION",Utility.getString(entity.getTypeDescription()));
			cstm.setInt("P_ORDER",entity.getTypeOrder());
			cstm.setString("P_ENABLED",Utility.getString(entity.getIdEnabled()));
			cstm.setLong("P_UPDATED_USER", Utility.parseStringToLong(entity.getUpdatedUser()));			
			cstm.registerOutParameter("S_TYPE_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_TYPE_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTransaction list(TypeRequest typeRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_TYPE.SP_LIST_TYPE(?,?,?)}";
        
        List<Object> typesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("P_KEYWORD", Utility.getString(typeRequest.getKeyword()));			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					TypeResponse typeResponse = new TypeResponse();
					typeResponse.setId(Utility.parseLongToString(rs.getLong("TYPE_ID")));
					typeResponse.setName(Utility.getString(rs.getString("TYPE_NAME")));
					typeResponse.setDescription(Utility.getString(rs.getString("TYPE_DESCRIPTION")));
					typeResponse.setOrder(Utility.parseLongToString(rs.getLong("TYPE_ORDER")));
					typeResponse.setEnabled(Utility.getString(rs.getString("TYPE_ENABLED")));
				
					typesResponse.add(typeResponse);
				}
				response.setList(typesResponse);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	@Override
	public ResponseTransaction read(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_TYPE.SP_GET_TYPE(?,?,?)}";
        
        List<Object> typesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_TYPE_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					TypeResponse typeResponse = new TypeResponse();
					typeResponse.setId(Utility.parseLongToString(rs.getLong("TYPE_ID")));
					typeResponse.setName(Utility.getString(rs.getString("TYPE_NAME")));
					typeResponse.setDescription(Utility.getString(rs.getString("TYPE_DESCRIPTION")));
					typeResponse.setOrder(Utility.parseLongToString(rs.getLong("TYPE_ORDER")));
					typeResponse.setEnabled(Utility.getString(rs.getString("TYPE_ENABLED")));
					
					typesResponse.add(typeResponse);
				}	
				response.setList(typesResponse);					
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

}
