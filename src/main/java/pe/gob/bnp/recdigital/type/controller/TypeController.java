package pe.gob.bnp.recdigital.type.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.bnp.recdigital.type.service.TypeService;
import pe.gob.bnp.recdigital.utilitary.common.ResponseHandler;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TypeController {


	@Autowired
	ResponseHandler responseHandler;
	
	@Autowired
	TypeService typeService;

	
	/*@RequestMapping(method = RequestMethod.POST,path="/type", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "registrar tipos de recursos - curso o recurso", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> save(
			@ApiParam(value = "Estructura JSON de tipo de recursos", required = true)
			@RequestBody TypeDto typeDto) throws Exception {
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = typeService.save(typeDto);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}*/
	
	/*@RequestMapping(method = RequestMethod.PUT,path="/type/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "actualizar tipos de recursos - curso o recurso", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> update(
			@ApiParam(value = "Estructura JSON de tipo de recursos", required = true)
			@PathVariable Long id,@RequestBody TypeDto typeDto) throws Exception {
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = typeService.update(id,typeDto);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}*/
		
	/*@RequestMapping(method = RequestMethod.GET, path="/types",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar tipos de recursos - curso o recurso", response= TypeResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAll(
			@RequestParam(value = "keyword", defaultValue = "", required=false) String  keyword){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response= typeService.searchAll(keyword);
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros."+ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}*/
	
	/*@RequestMapping(method = RequestMethod.GET, path="/type/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "leer tipo de recurso - curso o recurso", response= TypeResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> get(
			@ApiParam(value = "id tipo", required = true)
			@PathVariable Long id){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = typeService.get(id);
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}*/	

}
