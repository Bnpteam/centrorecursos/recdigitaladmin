package pe.gob.bnp.recdigital.type.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.recdigital.type.dto.TypeDto;
import pe.gob.bnp.recdigital.type.dto.TypeRequest;
import pe.gob.bnp.recdigital.type.exception.TypeException;
import pe.gob.bnp.recdigital.type.mapper.TypeMapper;
import pe.gob.bnp.recdigital.type.model.Type;
import pe.gob.bnp.recdigital.type.repository.TypeRepository;
import pe.gob.bnp.recdigital.type.validation.TypeValidation;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;


@Service
public class TypeService {

	@Autowired
	TypeRepository typeRepository;

	@Autowired
	TypeMapper typeMapper;

	public ResponseTransaction save(TypeDto typeDto) {

		Notification notification = TypeValidation.validation(typeDto);
		
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}

		Type type= typeMapper.reverseMapperSave(typeDto);
		ResponseTransaction response = typeRepository.persist(type);
		
		return TypeException.setMessageResponseSave(response);

	}
	
	public ResponseTransaction update(Long id,TypeDto typeDto) {
		
    	Notification notification = TypeValidation.validation(typeDto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}

		Type type= typeMapper.reverseMapperUpdate(id,typeDto);
		ResponseTransaction response = typeRepository.update(type);
		
		return TypeException.setMessageResponseUpdate(response);

	}
	
	public ResponseTransaction searchAll(String keyword){
			TypeRequest typeRequest = new TypeRequest();
			typeRequest.setKeyword(keyword);
			
			Notification notification = TypeValidation.validation(typeRequest);
			if (notification.hasErrors()) {
				throw new IllegalArgumentException(notification.errorMessage());
			}		
			return typeRepository.list(typeRequest);
	}
	
	public ResponseTransaction get(Long id) {
		Notification notification = TypeValidation.validation(id);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		return typeRepository.read(id);
	}
}
