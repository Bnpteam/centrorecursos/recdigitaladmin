package pe.gob.bnp.recdigital.type.validation;

import pe.gob.bnp.recdigital.type.dto.TypeDto;
import pe.gob.bnp.recdigital.type.dto.TypeRequest;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class TypeValidation {
	
	private static String messageTypeSave = "No se encontraron datos del Tipo";
	private static String messageTypeSearchAll = "No se encontró el filtro de búsqueda";
	private static String messageTitleSave = "Se debe ingresar el nombre del tipo";
	private static String messageTypeGet = "El id debe ser mayor a cero";

	public static Notification validation(TypeDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageTypeSave);
			return notification;
		}

		if (Utility.isEmptyOrNull(dto.getName())) {
			notification.addError(messageTitleSave);
		}

		return notification;
	}

	public static Notification validation(TypeRequest dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageTypeSearchAll);
			return notification;
		}

		return notification;
	}
	
	public static Notification validation(Long id) {
		Notification notification = new Notification();
		if (id <=0L) {
			notification.addError(messageTypeGet);
			return notification;
		}		
		return notification;
	}	

}
