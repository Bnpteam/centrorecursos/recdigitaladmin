package pe.gob.bnp.recdigital.type.model;

import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.model.BaseEntity;

public class Type extends BaseEntity{
	
	private String name;
	private String typeDescription;
	private Integer typeOrder;
	
	public Type() {
		super();
		this.name = Constants.EMPTY_STRING;		
		this.typeDescription = Constants.EMPTY_STRING;
		this.typeOrder = Constants.ZERO_INTEGER;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTypeDescription() {
		return typeDescription;
	}

	public void setTypeDescription(String typeDescription) {
		this.typeDescription = typeDescription;
	}

	public Integer getTypeOrder() {
		return typeOrder;
	}

	public void setTypeOrder(Integer typeOrder) {
		this.typeOrder = typeOrder;
	}

	
}
