package pe.gob.bnp.recdigital.type.mapper;

import org.springframework.stereotype.Component;

import pe.gob.bnp.recdigital.type.dto.TypeDto;
import pe.gob.bnp.recdigital.type.model.Type;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

@Component
public class TypeMapper {
	
	public Type reverseMapperSave(TypeDto typeDto) {
		Type type = new Type();	
		
		type.setName(typeDto.getName());		
		type.setTypeDescription(typeDto.getDescription());
		type.setTypeOrder(Utility.parseStringToInt(typeDto.getOrder()));
		type.setCreatedUser(typeDto.getCreatedUser());
		return type;
	}
	
	public Type reverseMapperUpdate(Long id,TypeDto typeDto) {
		Type type = new Type();
		
		typeDto.setId(Utility.parseLongToString(id));
		type.setId(Utility.parseStringToLong(typeDto.getId()));
		type.setName(typeDto.getName());
		type.setTypeDescription(typeDto.getDescription());
		type.setTypeOrder(Utility.parseStringToInt(typeDto.getOrder()));
		type.setIdEnabled(typeDto.getEnabled());	
		type.setUpdatedUser(typeDto.getUpdatedUser());
		return type;
	}


}
