package pe.gob.bnp.recdigital.consult.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigital.consult.dto.ConsultRequest;
import pe.gob.bnp.recdigital.consult.dto.ConsultResponse;
import pe.gob.bnp.recdigital.consult.repository.ConsultRepository;
import pe.gob.bnp.recdigital.resource.model.Resource;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;
import pe.gob.bnp.recdigital.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class ConsultRepositoryJDBC extends BaseJDBCOperation implements ConsultRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(ConsultRepositoryJDBC.class);
	
	private String messageSuccess="Transacción exitosa.";
	
	@Override
	public ResponseTransaction list(ConsultRequest resourceRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_CONSULT.SP_LIST_CONSULT(?,?,?,?,?,?,?)}";/*7*/
        
        List<Object> consultsResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setString("P_KEYWORD", Utility.getString(resourceRequest.getKeyword()));	
			cstm.setString("P_TYPE", Utility.getString(resourceRequest.getType()));	
			cstm.setString("P_STATE", Utility.getString(resourceRequest.getState()));	
			cstm.setInt("P_PAGE", Utility.parseStringToInt(resourceRequest.getPage()));	
			cstm.setInt("P_SIZE", Utility.parseStringToInt(resourceRequest.getSize()));	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					ConsultResponse consultResponse = new ConsultResponse();
					consultResponse.setId(Utility.parseLongToString(rs.getLong("RESOURCE_ID")));
					consultResponse.setTitle(Utility.getString(rs.getString("RESOURCE_TITLE")));
					consultResponse.setDescription(Utility.getString(rs.getString("RESOURCE_DESCRIPTION")));				
			     	consultResponse.setCategory(Utility.getString(rs.getString("RESOURCE_CATEGORY")));
					consultResponse.setTypeId(Utility.getString(rs.getString("RESOURCE_TYPE_ID")));
					consultResponse.setType(Utility.getString(rs.getString("RESOURCE_TYPE")));
					consultResponse.setStateId(Utility.getString(rs.getString("RESOURCE_STATE_ID")));
					consultResponse.setState(Utility.getString(rs.getString("RESOURCE_STATE")));
					consultResponse.setCreatedUser(Utility.getString(rs.getString("CREATED_USER")));
					consultResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
					consultResponse.setTotalRecords(Utility.getString(rs.getString("TOTAL_RECORDS")));
					consultsResponse.add(consultResponse);
				}
				response.setList(consultsResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_CONSULT.SP_LIST_CONSULT: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}



	@Override
	public ResponseTransaction persist(Resource entity) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ResponseTransaction update(Resource entity) {
		// TODO Auto-generated method stub
		return null;
	}



}
