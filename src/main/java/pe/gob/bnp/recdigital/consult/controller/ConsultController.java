package pe.gob.bnp.recdigital.consult.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.recdigital.consult.dto.ConsultResponse;
import pe.gob.bnp.recdigital.consult.service.ConsultService;
import pe.gob.bnp.recdigital.utilitary.common.ResponseHandler;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.exception.EntityNotFoundResultException;

@RestController
@RequestMapping("/api")
@Api(value = "/api/consult")
@CrossOrigin(origins = "*")
public class ConsultController {
	
	@Autowired
	ResponseHandler responseHandler;

	@Autowired
	ConsultService consultService;
	
	
	@RequestMapping(method = RequestMethod.GET, path="/consults",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "consultas recursos o cursos", response= ConsultResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAll(
			@RequestParam(value = "keyword", defaultValue = "", required=false) String  keyword,
			@RequestParam(value = "type", defaultValue = "", required=false) String  type,
			@RequestParam(value = "state", defaultValue = "", required=false) String  state,
			@RequestParam(value = "page",  defaultValue = "", required=true) String  page,
			@RequestParam(value = "size",  defaultValue = "", required=true) String  size){
		ResponseTransaction response=new ResponseTransaction();
		try {
			response = consultService.searchAll(keyword,type,state,page,size);
			
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			
			if (response == null || response.getList().size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros."+ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}

}
