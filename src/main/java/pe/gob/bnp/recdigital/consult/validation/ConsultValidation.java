package pe.gob.bnp.recdigital.consult.validation;

import pe.gob.bnp.recdigital.consult.dto.ConsultRequest;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class ConsultValidation {
	
	private static String messageCourseList = "No se encontraron datos de type, state, page o size";
	private static String messageCourseListPage= "Se debe ingresar el nro. de página";
	private static String messageCourseListSize= "Se debe ingresar el nro. de registros";
	
	
	public static Notification validation(ConsultRequest courseRequest) {
		Notification notification = new Notification();

		if (courseRequest == null) {
			notification.addError(messageCourseList);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(courseRequest.getPage())) {
			notification.addError(messageCourseListPage);
		}
		
		if (Utility.isEmptyOrNull(courseRequest.getSize())) {
			notification.addError(messageCourseListSize);
		}

		return notification;
	}

}
