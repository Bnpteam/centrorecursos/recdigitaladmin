package pe.gob.bnp.recdigital.consult.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.recdigital.consult.dto.ConsultRequest;
import pe.gob.bnp.recdigital.consult.repository.ConsultRepository;
import pe.gob.bnp.recdigital.consult.validation.ConsultValidation;
import pe.gob.bnp.recdigital.resource.mapper.ResourceMapper;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Notification;

@Service
public class ConsultService {
	

	@Autowired
	ConsultRepository consultRepository;	

	@Autowired
	ResourceMapper resourceMapper;	
	
	public ResponseTransaction searchAll(String keyword, String type,String state,String page, String size) {
		
		ResponseTransaction response = new ResponseTransaction();
		
		ConsultRequest consultRequest = new ConsultRequest();
		consultRequest.setKeyword(keyword);
		consultRequest.setType(type);
		consultRequest.setState(state);
		consultRequest.setPage(page);
		consultRequest.setSize(size);
		
		Notification notification = ConsultValidation.validation(consultRequest);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());			
			return response;
		}	


		return consultRepository.list(consultRequest);
	}
	

}
