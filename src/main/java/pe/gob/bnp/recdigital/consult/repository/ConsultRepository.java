package pe.gob.bnp.recdigital.consult.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.recdigital.consult.dto.ConsultRequest;
import pe.gob.bnp.recdigital.resource.model.Resource;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.repository.BaseRepository;

@Repository
public interface ConsultRepository extends BaseRepository<Resource>{
	
	public ResponseTransaction list(ConsultRequest consultRequest);

}
