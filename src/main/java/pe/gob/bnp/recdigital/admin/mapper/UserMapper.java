package pe.gob.bnp.recdigital.admin.mapper;

import org.springframework.stereotype.Component;

import pe.gob.bnp.recdigital.admin.dto.UserDto;
import pe.gob.bnp.recdigital.admin.model.User;
import pe.gob.bnp.recdigital.utilitary.common.Utility;


@Component
public class UserMapper {
	

	public User reverseMapperSave(UserDto userDto) {
		User user = new User();	
		
		user.setUserName(userDto.getName());;
		user.setUserDomain(userDto.getDomain());
		user.setCreatedUser(userDto.getCreatedUser());
		
		
		//course
		if(userDto.getFlagCourse().contains("true")){
			userDto.setFlagCourse("1");
		}
		if(userDto.getFlagCourse().contains("false")) {
			userDto.setFlagCourse("0");
		}
		
		////resource
		if(userDto.getFlagResource().contains("true")){
			userDto.setFlagResource("1");
		}
		if(userDto.getFlagResource().contains("false")) {
			userDto.setFlagResource("0");
		}
		////publication
		if(userDto.getFlagPublication().contains("true")){
			userDto.setFlagPublication("1");
		}
		if(userDto.getFlagPublication().contains("false")) {
			userDto.setFlagPublication("0");
		}
		////consult
		if(userDto.getFlagConsult().contains("true")){
			userDto.setFlagConsult("1");
		}
		if(userDto.getFlagConsult().contains("false")) {
			userDto.setFlagConsult("0");
		}
		////mantslider
		if(userDto.getFlagMantSlider().contains("true")){
			userDto.setFlagMantSlider("1");
		}
		if(userDto.getFlagMantSlider().contains("false")) {
			userDto.setFlagMantSlider("0");
		}
		////mantuser
		if(userDto.getFlagMantUser().contains("true")){
			userDto.setFlagMantUser("1");
		}
		if(userDto.getFlagMantUser().contains("false")) {
			userDto.setFlagMantUser("0");
		}
		///mantouter
		if(userDto.getFlagMantOuter().contains("true")){
			userDto.setFlagMantOuter("1");
		}
		if(userDto.getFlagMantOuter().contains("false")) {
			userDto.setFlagMantOuter("0");
		}
		
		user.setFlagCourse(userDto.getFlagCourse());
		user.setFlagResource(userDto.getFlagResource());
		user.setFlagPublication(userDto.getFlagPublication());
		user.setFlagConsult(userDto.getFlagConsult());
		user.setFlagMantSlider(userDto.getFlagMantSlider());
		user.setFlagMantUser(userDto.getFlagMantUser());
		user.setFlagMantOuter(userDto.getFlagMantOuter());
		return user;
	}
	
	public User reverseMapperUpdate(Long id,UserDto userDto) {
		User user= new User();
		
		userDto.setId(Utility.parseLongToString(id));
		user.setId(Utility.parseStringToLong(userDto.getId()));
		user.setUserName(userDto.getName());;
		user.setUserDomain(userDto.getDomain());	
		user.setIdEnabled(userDto.getEnabled());	
		user.setUpdatedUser(userDto.getUpdatedUser());
		
		
		//course
		if(userDto.getFlagCourse().contains("true")){
			userDto.setFlagCourse("1");
		}
		if(userDto.getFlagCourse().contains("false")) {
			userDto.setFlagCourse("0");
		}
		
		////resource
		if(userDto.getFlagResource().contains("true")){
			userDto.setFlagResource("1");
		}
		if(userDto.getFlagResource().contains("false")) {
			userDto.setFlagResource("0");
		}
		////publication
		if(userDto.getFlagPublication().contains("true")){
			user.setFlagPublication("1");
		}
		if(userDto.getFlagPublication().contains("false")) {
			userDto.setFlagPublication("0");
		}
		////consult
		if(userDto.getFlagConsult().contains("true")){
			userDto.setFlagConsult("1");
		}
		if(userDto.getFlagConsult().contains("false")) {
			userDto.setFlagConsult("0");
		}
		////mantslider
		if(userDto.getFlagMantSlider().contains("true")){
			userDto.setFlagMantSlider("1");
		}
		if(userDto.getFlagMantSlider().contains("false")) {
			userDto.setFlagMantSlider("0");
		}
		////mantuser
		if(userDto.getFlagMantUser().contains("true")){
			userDto.setFlagMantUser("1");
		}
		if(userDto.getFlagMantUser().contains("false")) {
			userDto.setFlagMantUser("0");
		}
		///mantouter
		if(userDto.getFlagMantOuter().contains("true")){
			userDto.setFlagMantOuter("1");
		}
		if(userDto.getFlagMantOuter().contains("false")) {
			userDto.setFlagMantOuter("0");
		}
		
		user.setFlagCourse(userDto.getFlagCourse());
		user.setFlagResource(userDto.getFlagResource());
		user.setFlagPublication(userDto.getFlagPublication());
		user.setFlagConsult(userDto.getFlagConsult());
		user.setFlagMantSlider(userDto.getFlagMantSlider());
		user.setFlagMantUser(userDto.getFlagMantUser());
		user.setFlagMantOuter(userDto.getFlagMantOuter());
		return user;
	}


}
