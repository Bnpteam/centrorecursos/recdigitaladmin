package pe.gob.bnp.recdigital.admin.dto;

public class UserDetailResponse {	

	private String id;
	private String name;
	private String domain;
	private String enabled;
	private String  createdDate;
	private String lastLoginDate;
	private String flagCourse;
	private String flagResource;
	private String flagPublication;
	private String flagConsult;
	private String flagMantSlider;
	private String flagMantUser;
	private String flagMantOuter;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getLastLoginDate() {
		return lastLoginDate;
	}
	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}
	public String getFlagCourse() {
		return flagCourse;
	}
	public void setFlagCourse(String flagCourse) {
		this.flagCourse = flagCourse;
	}
	public String getFlagResource() {
		return flagResource;
	}
	public void setFlagResource(String flagResource) {
		this.flagResource = flagResource;
	}
	public String getFlagPublication() {
		return flagPublication;
	}
	public void setFlagPublication(String flagPublication) {
		this.flagPublication = flagPublication;
	}
	public String getFlagConsult() {
		return flagConsult;
	}
	public void setFlagConsult(String flagConsult) {
		this.flagConsult = flagConsult;
	}
	public String getFlagMantSlider() {
		return flagMantSlider;
	}
	public void setFlagMantSlider(String flagMantSlider) {
		this.flagMantSlider = flagMantSlider;
	}
	public String getFlagMantUser() {
		return flagMantUser;
	}
	public void setFlagMantUser(String flagMantUser) {
		this.flagMantUser = flagMantUser;
	}
	public String getFlagMantOuter() {
		return flagMantOuter;
	}
	public void setFlagMantOuter(String flagMantOuter) {
		this.flagMantOuter = flagMantOuter;
	}
	

}
