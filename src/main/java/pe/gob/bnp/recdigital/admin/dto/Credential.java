package pe.gob.bnp.recdigital.admin.dto;

public class Credential {
	
	private String domain;
	
	public Credential() {
		super();
		this.domain = "";		
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

}
