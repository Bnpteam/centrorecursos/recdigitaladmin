package pe.gob.bnp.recdigital.admin.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.recdigital.admin.dto.Credential;
import pe.gob.bnp.recdigital.admin.dto.UserRequest;
import pe.gob.bnp.recdigital.admin.model.User;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.repository.BaseRepository;

@Repository
public interface UserRepository extends BaseRepository<User>{	

	public ResponseTransaction login(Credential credential);
	public ResponseTransaction list(UserRequest userRequest);
	public ResponseTransaction list2();
	public ResponseTransaction read(Long id);
}
