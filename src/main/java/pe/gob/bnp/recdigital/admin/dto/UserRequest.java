package pe.gob.bnp.recdigital.admin.dto;

public class UserRequest {
	
	private String keyword;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

}
