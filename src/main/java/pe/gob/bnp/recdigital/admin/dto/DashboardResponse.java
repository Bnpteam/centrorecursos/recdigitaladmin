package pe.gob.bnp.recdigital.admin.dto;

public class DashboardResponse {
	
	private String coursePublication;
	private String resourcePublication;
	private String outerActive;
	private String resourceDownload;
	private String courseVote;
	private String resourceVote;
	
	
	public String getResourceDownload() {
		return resourceDownload;
	}
	public void setResourceDownload(String resourceDownload) {
		this.resourceDownload = resourceDownload;
	}
	
	public String getCoursePublication() {
		return coursePublication;
	}
	public void setCoursePublication(String coursePublication) {
		this.coursePublication = coursePublication;
	}
	public String getResourcePublication() {
		return resourcePublication;
	}
	public void setResourcePublication(String resourcePublication) {
		this.resourcePublication = resourcePublication;
	}
	public String getOuterActive() {
		return outerActive;
	}
	public void setOuterActive(String outerActive) {
		this.outerActive = outerActive;
	}
	
	public String getCourseVote() {
		return courseVote;
	}
	public void setCourseVote(String courseVote) {
		this.courseVote = courseVote;
	}
	public String getResourceVote() {
		return resourceVote;
	}
	public void setResourceVote(String resourceVote) {
		this.resourceVote = resourceVote;
	}

}
