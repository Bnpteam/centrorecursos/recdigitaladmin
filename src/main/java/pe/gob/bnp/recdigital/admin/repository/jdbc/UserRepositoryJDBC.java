package pe.gob.bnp.recdigital.admin.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigital.admin.dto.Credential;
import pe.gob.bnp.recdigital.admin.dto.DashboardResponse;
import pe.gob.bnp.recdigital.admin.dto.LoginResponse;
import pe.gob.bnp.recdigital.admin.dto.UserDetailResponse;
import pe.gob.bnp.recdigital.admin.dto.UserRequest;
import pe.gob.bnp.recdigital.admin.dto.UserResponse;
import pe.gob.bnp.recdigital.admin.model.User;
import pe.gob.bnp.recdigital.admin.repository.UserRepository;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;
import pe.gob.bnp.recdigital.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class UserRepositoryJDBC extends BaseJDBCOperation implements UserRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(UserRepositoryJDBC.class);
	
	private String messageSuccess="Transacción exitosa."; 
	
	
	@Override
	public ResponseTransaction login(Credential credential) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_AUTH.SP_LOGIN(?,?,?)}";
             
        List<Object> usersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("P_DOMAIN", credential.getDomain());
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				cn.commit();
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					LoginResponse userResponse = new LoginResponse(); 
					userResponse.setId(Utility.parseLongToString(rs.getLong("USER_ID")));
					userResponse.setName(Utility.getString(rs.getString("USER_NAME")));
					userResponse.setDomain(Utility.getString(rs.getString("USER_DOMAIN")));
					userResponse.setEnabled(Utility.getString(rs.getString("USER_ENABLED")));
					
					usersResponse.add(userResponse);
				}	
				response.setList(usersResponse);			
			}else {
				cn.rollback();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_AUTH.SP_LOGIN: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	} 
	

	@Override
	public ResponseTransaction persist(User entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_AUTH.SP_PERSIST_USER(?,?,?,?,?,?,?,?,?,?,?,?)}";/*12*/
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setString("P_NAME", Utility.getString(entity.getUserName()));
			cstm.setString("P_DOMAIN",Utility.getString(entity.getUserDomain()));			
			
			cstm.setString("P_FLAG_COURSE",Utility.getString(entity.getFlagCourse()));	
			cstm.setString("P_FLAG_RESOURCE",Utility.getString(entity.getFlagResource()));	
			cstm.setString("P_FLAG_PUBLICATION",Utility.getString(entity.getFlagPublication()));	
			cstm.setString("P_FLAG_CONSULT",Utility.getString(entity.getFlagConsult()));	
			cstm.setString("P_FLAG_MANTSLIDER",Utility.getString(entity.getFlagMantSlider()));	
			cstm.setString("P_FLAG_MANTUSER",Utility.getString(entity.getFlagMantUser()));	
			cstm.setString("P_FLAG_MANTOUTER",Utility.getString(entity.getFlagMantOuter()));	
			
			cstm.setLong("P_CREATED_USER", Utility.parseStringToLong(entity.getCreatedUser()));			
			cstm.registerOutParameter("S_USER_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_USER_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_AUTH.SP_PERSIST_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction update(User entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_AUTH.SP_UPDATE_USER(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";/*14*/
        ResponseTransaction response = new ResponseTransaction();       
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_ID", entity.getId());
			cstm.setString("P_NAME", Utility.getString(entity.getUserName()));
			cstm.setString("P_DOMAIN",Utility.getString(entity.getUserDomain()));
			
			cstm.setString("P_ENABLED",Utility.getString(entity.getIdEnabled()));			
			cstm.setString("P_FLAG_COURSE",Utility.getString(entity.getFlagCourse()));	
			cstm.setString("P_FLAG_RESOURCE",Utility.getString(entity.getFlagResource()));	
			cstm.setString("P_FLAG_PUBLICATION",Utility.getString(entity.getFlagPublication()));	
			cstm.setString("P_FLAG_CONSULT",Utility.getString(entity.getFlagConsult()));	
			cstm.setString("P_FLAG_MANTSLIDER",Utility.getString(entity.getFlagMantSlider()));	
			cstm.setString("P_FLAG_MANTUSER",Utility.getString(entity.getFlagMantUser()));	
			cstm.setString("P_FLAG_MANTOUTER",Utility.getString(entity.getFlagMantOuter()));	
			cstm.setLong("P_UPDATED_USER", Utility.parseStringToLong(entity.getUpdatedUser()));			
			cstm.registerOutParameter("S_USER_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_USER_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_AUTH.SP_UPDATE_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction list(UserRequest userRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_AUTH.SP_LIST_USER(?,?,?)}";
        
        List<Object> usersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("P_KEYWORD", Utility.getString(userRequest.getKeyword()));			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					UserResponse userResponse = new UserResponse();
					userResponse.setId(Utility.parseLongToString(rs.getLong("USER_ID")));
					userResponse.setName(Utility.getString(rs.getString("USER_NAME")));
					userResponse.setDomain(Utility.getString(rs.getString("USER_DOMAIN")));
					userResponse.setEnabled(Utility.getString(rs.getString("USER_ENABLED")));
					userResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
					userResponse.setLastLoginDate(Utility.parseDateToString(Utility.getDate(rs.getDate("LAST_LOGIN_DATE"))));
				
					usersResponse.add(userResponse);
				}
				response.setList(usersResponse);
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_AUTH.SP_LIST_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	@Override
	public ResponseTransaction list2() {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_DASHBOARD.SP_LIST_DASHBOARD(?,?,?,?,?,?,?)}";
        
        List<Object> usersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_COURSE_PUBLICATION", OracleTypes.NUMBER);
			cstm.registerOutParameter("S_RESOURCE_PUBLICATION", OracleTypes.NUMBER);
			cstm.registerOutParameter("S_OUTER_ACTIVE", OracleTypes.NUMBER);
			cstm.registerOutParameter("S_RESOURCE_DOWNLOAD", OracleTypes.NUMBER);
			cstm.registerOutParameter("S_COURSES_VOTE", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_RESOURCES_VOTE", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
					DashboardResponse userResponse = new DashboardResponse();
					userResponse.setCoursePublication(Utility.parseIntToString2(cstm.getInt("S_COURSE_PUBLICATION")));
					userResponse.setResourcePublication(Utility.parseIntToString2(cstm.getInt("S_RESOURCE_PUBLICATION")));
					userResponse.setOuterActive(Utility.parseIntToString2(cstm.getInt("S_OUTER_ACTIVE")));
					userResponse.setResourceDownload(Utility.parseIntToString2(cstm.getInt("S_RESOURCE_DOWNLOAD")));
					userResponse.setCourseVote(Utility.getString(cstm.getString("S_COURSES_VOTE")));
					userResponse.setResourceVote(Utility.getString(cstm.getString("S_RESOURCES_VOTE")));
				
					usersResponse.add(userResponse);
			
				response.setList(usersResponse);
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_DASHBOARD.SP_LIST_DASHBOARD: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	@Override
	public ResponseTransaction read(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_AUTH.SP_GET_USER(?,?,?)}";
             
        List<Object> usersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_USER_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);		
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					UserDetailResponse userResponse = new UserDetailResponse(); 
					userResponse.setId(Utility.parseLongToString(rs.getLong("USER_ID")));
					userResponse.setName(Utility.getString(rs.getString("USER_NAME")));
					userResponse.setDomain(Utility.getString(rs.getString("USER_DOMAIN")));
					userResponse.setEnabled(Utility.getString(rs.getString("USER_ENABLED")));
					userResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
					userResponse.setLastLoginDate(Utility.parseDateToString(Utility.getDate(rs.getDate("LAST_LOGIN_DATE"))));
					
					String flagCourse=Utility.getString(rs.getString("FLAG_COURSE")).equalsIgnoreCase("1")?"true":"false";
					String flagResource=Utility.getString(rs.getString("FLAG_RESOURCE")).equalsIgnoreCase("1")?"true":"false";
					String flagPublication=Utility.getString(rs.getString("FLAG_PUBLICATION")).equalsIgnoreCase("1")?"true":"false";
					String flagConsult=Utility.getString(rs.getString("FLAG_CONSULT")).equalsIgnoreCase("1")?"true":"false";
					String flagMantSlider=Utility.getString(rs.getString("FLAGMANTSLIDER")).equalsIgnoreCase("1")?"true":"false";
					String flagMantUser=Utility.getString(rs.getString("FLAGMANTUSER")).equalsIgnoreCase("1")?"true":"false";
					String flagMantOuter=Utility.getString(rs.getString("FLAGMANTOUTER")).equalsIgnoreCase("1")?"true":"false";
					
					
					userResponse.setFlagCourse(flagCourse);
					userResponse.setFlagResource(flagResource);
					userResponse.setFlagPublication(flagPublication);
					userResponse.setFlagConsult(flagConsult);
					userResponse.setFlagMantSlider(flagMantSlider);
					userResponse.setFlagMantUser(flagMantUser);
					userResponse.setFlagMantOuter(flagMantOuter);
					
					usersResponse.add(userResponse);
				}	
				response.setList(usersResponse);			
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_AUTH.SP_GET_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

}
