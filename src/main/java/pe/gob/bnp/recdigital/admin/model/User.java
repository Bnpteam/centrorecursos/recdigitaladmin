package pe.gob.bnp.recdigital.admin.model;

import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.model.BaseEntity;

public class User extends BaseEntity{
	
	private String userName;
	private String userDomain;
	
	private String flagCourse;
	private String flagResource;
	private String flagPublication;
	private String flagConsult;
	private String flagMantSlider;
	private String flagMantUser;
	private String flagMantOuter;
	
	public User() {
		super();
		this.userName = Constants.EMPTY_STRING;
		this.userDomain = Constants.EMPTY_STRING;
		
		this.flagCourse= Constants.EMPTY_STRING;;
		this.flagResource= Constants.EMPTY_STRING;;
		this.flagPublication= Constants.EMPTY_STRING;;
		this.flagConsult= Constants.EMPTY_STRING;;
        this.flagMantSlider= Constants.EMPTY_STRING;;
		this.flagMantUser= Constants.EMPTY_STRING;;
		this.flagMantOuter= Constants.EMPTY_STRING;;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserDomain() {
		return userDomain;
	}

	public void setUserDomain(String userDomain) {
		this.userDomain = userDomain;
	}

	public String getFlagCourse() {
		return flagCourse;
	}

	public void setFlagCourse(String flagCourse) {
		this.flagCourse = flagCourse;
	}

	public String getFlagResource() {
		return flagResource;
	}

	public void setFlagResource(String flagResource) {
		this.flagResource = flagResource;
	}

	public String getFlagPublication() {
		return flagPublication;
	}

	public void setFlagPublication(String flagPublication) {
		this.flagPublication = flagPublication;
	}

	public String getFlagConsult() {
		return flagConsult;
	}

	public void setFlagConsult(String flagConsult) {
		this.flagConsult = flagConsult;
	}

	public String getFlagMantSlider() {
		return flagMantSlider;
	}

	public void setFlagMantSlider(String flagMantSlider) {
		this.flagMantSlider = flagMantSlider;
	}

	public String getFlagMantUser() {
		return flagMantUser;
	}

	public void setFlagMantUser(String flagMantUser) {
		this.flagMantUser = flagMantUser;
	}

	public String getFlagMantOuter() {
		return flagMantOuter;
	}

	public void setFlagMantOuter(String flagMantOuter) {
		this.flagMantOuter = flagMantOuter;
	}

}
