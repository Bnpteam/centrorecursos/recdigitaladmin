package pe.gob.bnp.recdigital.admin.dto;

import pe.gob.bnp.recdigital.admin.model.User;
import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class UserDto {
	
	private String id;
	private String name;
	private String domain;
	private String enabled;
	private String createdUser;
	private String updatedUser;	
	
	private String flagCourse;
	private String flagResource;
	private String flagPublication;
	private String flagConsult;
	private String flagMantSlider;
	private String flagMantUser;
	private String flagMantOuter;
	
	public UserDto() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.name = Constants.EMPTY_STRING;
		this.domain = Constants.EMPTY_STRING;
		this.enabled = Constants.EMPTY_STRING;
		this.createdUser=Constants.EMPTY_STRING;
		this.updatedUser=Constants.EMPTY_STRING;
		
		this.flagCourse= Constants.EMPTY_STRING;;
		this.flagResource= Constants.EMPTY_STRING;;
		this.flagPublication= Constants.EMPTY_STRING;;
		this.flagConsult= Constants.EMPTY_STRING;;
        this.flagMantSlider= Constants.EMPTY_STRING;;
		this.flagMantUser= Constants.EMPTY_STRING;;
		this.flagMantOuter= Constants.EMPTY_STRING;;
	}	

	public UserDto(User user) {
		super();
		this.id =Utility.parseLongToString(user.getId());
		this.name = user.getUserName();
		this.domain= user.getUserDomain();
		this.enabled = user.getIdEnabled();
		this.createdUser=user.getCreatedUser();
		this.updatedUser=user.getUpdatedUser();
		
		
		this.flagCourse= user.getFlagCourse();
		this.flagResource= user.getFlagResource();
		this.flagPublication=user.getFlagPublication();
		this.flagConsult= user.getFlagConsult();
        this.flagMantSlider= user.getFlagMantSlider();
		this.flagMantUser= user.getFlagMantUser();
		this.flagMantOuter= user.getFlagMantOuter();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getFlagCourse() {
		return flagCourse;
	}

	public void setFlagCourse(String flagCourse) {
		this.flagCourse = flagCourse;
	}

	public String getFlagResource() {
		return flagResource;
	}

	public void setFlagResource(String flagResource) {
		this.flagResource = flagResource;
	}

	public String getFlagPublication() {
		return flagPublication;
	}

	public void setFlagPublication(String flagPublication) {
		this.flagPublication = flagPublication;
	}

	public String getFlagConsult() {
		return flagConsult;
	}

	public void setFlagConsult(String flagConsult) {
		this.flagConsult = flagConsult;
	}

	public String getFlagMantSlider() {
		return flagMantSlider;
	}

	public void setFlagMantSlider(String flagMantSlider) {
		this.flagMantSlider = flagMantSlider;
	}

	public String getFlagMantUser() {
		return flagMantUser;
	}

	public void setFlagMantUser(String flagMantUser) {
		this.flagMantUser = flagMantUser;
	}

	public String getFlagMantOuter() {
		return flagMantOuter;
	}

	public void setFlagMantOuter(String flagMantOuter) {
		this.flagMantOuter = flagMantOuter;
	}

}
