package pe.gob.bnp.recdigital.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.recdigital.admin.dto.Credential;
import pe.gob.bnp.recdigital.admin.dto.UserDto;
import pe.gob.bnp.recdigital.admin.dto.UserRequest;
import pe.gob.bnp.recdigital.admin.exception.UserException;
import pe.gob.bnp.recdigital.admin.mapper.UserMapper;
import pe.gob.bnp.recdigital.admin.model.User;
import pe.gob.bnp.recdigital.admin.repository.UserRepository;
import pe.gob.bnp.recdigital.admin.validation.UserValidation;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;

@Service
public class AuthService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserMapper userMapper;

	public ResponseTransaction login(Credential credential){
		
		ResponseTransaction response = new ResponseTransaction();
		
		Notification notification = UserValidation.validation(credential);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}	
		
		response=userRepository.login(credential);
		
		return UserException.setMessageResponseLogin(response);	
	}
	
	public ResponseTransaction signup(UserDto userDto) {

		Notification notification = UserValidation.validation(userDto);
		
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}

		User user= userMapper.reverseMapperSave(userDto);
		ResponseTransaction response = userRepository.persist(user);
		
		return UserException.setMessageResponseSave(response);

	}
	
	public ResponseTransaction update(Long id,UserDto userDto) {
		
    	Notification notification = UserValidation.validation(userDto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}

		User user = userMapper.reverseMapperUpdate(id,userDto);
		ResponseTransaction response = userRepository.update(user);
		
		return UserException.setMessageResponseUpdate(response);

	}
	
	public ResponseTransaction searchAll(String keyword){
			UserRequest userRequest = new UserRequest();
			userRequest.setKeyword(keyword);
			
			Notification notification = UserValidation.validation(userRequest);
			if (notification.hasErrors()) {
				throw new IllegalArgumentException(notification.errorMessage());
			}		
			return userRepository.list(userRequest);
	}
	
	public ResponseTransaction searchDashboard(){		
			
		return userRepository.list2();
}
	
	public ResponseTransaction get(Long id) {
		
		ResponseTransaction response = new ResponseTransaction();
		
		Notification notification = UserValidation.validation(id);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}
		return userRepository.read(id);
	}

}
