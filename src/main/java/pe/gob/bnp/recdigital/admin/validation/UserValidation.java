package pe.gob.bnp.recdigital.admin.validation;

import pe.gob.bnp.recdigital.admin.dto.Credential;
import pe.gob.bnp.recdigital.admin.dto.UserDto;
import pe.gob.bnp.recdigital.admin.dto.UserRequest;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class UserValidation {

	private static String messageUserSave = "No se encontraron datos del Usuario";
	private static String messageUserLogin = "No se encontraron datos de acceso";
	private static String messageUserSearchAll = "No se encontró el filtro de búsqueda";
	private static String messageNameSave = "Se debe ingresar un nombre de Usuario";	
	private static String messageDomainSave = "Se debe ingresar un dominio de Usuario";
	private static String messageUserGet = "El id debe ser mayor a cero";
	
	public static Notification validation(Credential credential) {

		Notification notification = new Notification();

		if (credential == null) {
			notification.addError(messageUserLogin);
			return notification;
		}

		if (Utility.isEmptyOrNull(credential.getDomain())) {
			notification.addError(messageDomainSave);
		}

		return notification;
	}
	

	public static Notification validation(UserDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageUserSave);
			return notification;
		}

		if (Utility.isEmptyOrNull(dto.getName())) {
			notification.addError(messageNameSave);
		}
		
		if (Utility.isEmptyOrNull(dto.getDomain())) {
			notification.addError(messageDomainSave);
		}

		return notification;
	}

	public static Notification validation(UserRequest dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageUserSearchAll);
			return notification;
		}

		return notification;
	}
	
	public static Notification validation(Long id) {
		Notification notification = new Notification();
		if (id <=0L) {
			notification.addError(messageUserGet);
			return notification;
		}		
		return notification;
	}	

}
