package pe.gob.bnp.recdigital.recommended.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.recdigital.recommended.dto.RecommendedRequest;
import pe.gob.bnp.recdigital.recommended.dto.RecommendedResponse;
import pe.gob.bnp.recdigital.recommended.model.Recommended;
import pe.gob.bnp.recdigital.utilitary.repository.BaseRepository;


@Repository
public interface RecommendedRepository extends BaseRepository<Recommended>{	

	public List<RecommendedResponse> list(RecommendedRequest recommendedRequest);
	public RecommendedResponse read(Long id);
}