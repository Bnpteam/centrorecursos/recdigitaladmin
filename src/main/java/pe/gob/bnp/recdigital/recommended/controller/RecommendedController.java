package pe.gob.bnp.recdigital.recommended.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.bnp.recdigital.recommended.service.RecommendedService;
import pe.gob.bnp.recdigital.utilitary.common.ResponseHandler;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class RecommendedController {
	
	@Autowired
	ResponseHandler responseHandler;
	
	@Autowired
	RecommendedService recommendedService;

	
	/*@RequestMapping(method = RequestMethod.POST,path="/recommended", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "registrar agrupación para cursos recomendados", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> save(
			@ApiParam(value = "Estructura JSON de agrupación", required = true)
			@RequestBody RecommendedDto recommendedDto) throws Exception {
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = recommendedService.save(recommendedDto);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}*/
	
	/*@RequestMapping(method = RequestMethod.PUT,path="/recommended/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "actualizar agrupación para cursos recomendados", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> update(
			@ApiParam(value = "Estructura JSON de agrupación", required = true)
			@PathVariable Long id,@RequestBody RecommendedDto recommendedDto) throws Exception {
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = recommendedService.update(id,recommendedDto);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}
	*/
		
	/*@RequestMapping(method = RequestMethod.GET, path="/recommendeds",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar agrupaciones para cursos recomendados", response= RecommendedResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAll(
			@RequestParam(value = "code",       defaultValue = "", required=false) String  code,
			@RequestParam(value = "description", defaultValue = "", required=false) String  description){
		ResponseTransaction response = new ResponseTransaction();
		try {
			List<RecommendedResponse> response1 = recommendedService.searchAll(code,description);
			if (response1 == null || response1.size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros."+ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}*/
	
	/*@RequestMapping(method = RequestMethod.GET, path="/recommended/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "leer agrupaciones para cursos recomendados", response= RecommendedResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> get(
			@ApiParam(value = "id agrupacion", required = true)
			@PathVariable Long id){
		ResponseTransaction response = new ResponseTransaction();
		try {
			RecommendedResponse response1 = recommendedService.get(id);
			if (response1 == null || Utility.parseStringToLong(response1.getId())<=0L ) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}*/		

}
