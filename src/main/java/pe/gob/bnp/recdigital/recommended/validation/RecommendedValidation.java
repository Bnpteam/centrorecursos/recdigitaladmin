package pe.gob.bnp.recdigital.recommended.validation;

import pe.gob.bnp.recdigital.recommended.dto.RecommendedDto;
import pe.gob.bnp.recdigital.recommended.dto.RecommendedRequest;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class RecommendedValidation {
	
	private static String messageRecommendedSave = "No se encontraron datos de la recomendacion";
	private static String messageRecommendedSearchAll = "No se encontró el filtro de búsqueda";
	private static String messageDescriptionSave = "Se debe ingresar una descripcion para la recomendacion";
	private static String messageRecommendedGet = "El id debe ser mayor a cero";

	public static Notification validation(RecommendedDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageRecommendedSave);
			return notification;
		}

		if (Utility.isEmptyOrNull(dto.getDescription())) {
			notification.addError(messageDescriptionSave);
		}

		return notification;
	}

	public static Notification validation(RecommendedRequest dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageRecommendedSearchAll);
			return notification;
		}

		return notification;
	}
	
	public static Notification validation(Long id) {
		Notification notification = new Notification();
		if (id <=0L) {
			notification.addError(messageRecommendedGet);
			return notification;
		}		
		return notification;
	}	

}
