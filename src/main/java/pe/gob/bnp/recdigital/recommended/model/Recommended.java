package pe.gob.bnp.recdigital.recommended.model;


import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.model.BaseEntity;

public class Recommended extends BaseEntity{
	
	private String recommendedCode;
	private String recommendedDescription;
	
	public Recommended() {
		super();
		this.recommendedCode = Constants.EMPTY_STRING;;
		this.recommendedDescription = Constants.EMPTY_STRING;;
	}

	public String getRecommendedCode() {
		return recommendedCode;
	}

	public void setRecommendedCode(String recommendedCode) {
		this.recommendedCode = recommendedCode;
	}

	public String getRecommendedDescription() {
		return recommendedDescription;
	}

	public void setRecommendedDescription(String recommendedDescription) {
		this.recommendedDescription = recommendedDescription;
	}
	
	
}
