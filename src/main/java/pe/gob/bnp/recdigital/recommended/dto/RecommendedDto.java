package pe.gob.bnp.recdigital.recommended.dto;

import pe.gob.bnp.recdigital.recommended.model.Recommended;
import pe.gob.bnp.recdigital.utilitary.common.Constants;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

public class RecommendedDto {	

	private String id;
	private String code;
	private String description;
	private String enabled;
	private String createdUser;
	private String updatedUser;	

	public RecommendedDto() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.code = Constants.EMPTY_STRING;
		this.description = Constants.EMPTY_STRING;
		this.enabled = Constants.EMPTY_STRING;
		this.createdUser=Constants.EMPTY_STRING;
		this.updatedUser=Constants.EMPTY_STRING;
	}	

	public RecommendedDto(Recommended recommended) {
		super();
		this.id =Utility.parseLongToString(recommended.getId());
		this.code = recommended.getRecommendedCode();		
		this.description = recommended.getRecommendedDescription();
		this.enabled = recommended.getIdEnabled();
		this.createdUser=recommended.getCreatedUser();
		this.updatedUser=recommended.getUpdatedUser();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}


}
