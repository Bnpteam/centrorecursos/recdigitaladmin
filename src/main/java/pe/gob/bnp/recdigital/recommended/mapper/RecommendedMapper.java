package pe.gob.bnp.recdigital.recommended.mapper;

import org.springframework.stereotype.Component;

import pe.gob.bnp.recdigital.recommended.dto.RecommendedDto;
import pe.gob.bnp.recdigital.recommended.model.Recommended;
import pe.gob.bnp.recdigital.utilitary.common.Utility;

@Component
public class RecommendedMapper {

	public Recommended reverseMapperSave(RecommendedDto recommendedDto) {
		Recommended recommended = new Recommended();	
		
		recommended.setRecommendedDescription(recommendedDto.getDescription());
		recommended.setCreatedUser(recommendedDto.getCreatedUser());
		return recommended;
	}
	
	public Recommended reverseMapperUpdate(Long id,RecommendedDto recommendedDto) {
		Recommended recommended = new Recommended();
		
		recommendedDto.setId(Utility.parseLongToString(id));
		recommended.setRecommendedDescription(recommendedDto.getDescription());
		recommended.setIdEnabled(recommendedDto.getEnabled());	
		recommended.setUpdatedUser(recommendedDto.getUpdatedUser());
		return recommended;
	}

}
