package pe.gob.bnp.recdigital.recommended.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigital.recommended.dto.RecommendedRequest;
import pe.gob.bnp.recdigital.recommended.dto.RecommendedResponse;
import pe.gob.bnp.recdigital.recommended.model.Recommended;
import pe.gob.bnp.recdigital.recommended.repository.RecommendedRepository;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.common.Utility;
import pe.gob.bnp.recdigital.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class RecommendedRepositoryJDBC extends BaseJDBCOperation implements RecommendedRepository{
	
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction persist(Recommended entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_RECOMMENDED.SP_PERSIST_RECOMMENDED(?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);			
			
			cstm.setString("P_DESCRIPTION",Utility.getString(entity.getRecommendedDescription()));
			cstm.setLong("P_CREATED_USER", Utility.parseStringToLong(entity.getCreatedUser()));			
			cstm.registerOutParameter("S_RECOMMENDED_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_RECOMMENDED_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction update(Recommended entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_RECOMMENDED.SP_UPDATE_RECOMMENDED(?,?,?,?,?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_ID", entity.getId());		
			cstm.setString("P_DESCRIPTION",Utility.getString(entity.getRecommendedDescription()));
			cstm.setString("P_ENABLED",Utility.getString(entity.getIdEnabled()));
			cstm.setLong("P_UPDATED_USER", Utility.parseStringToLong(entity.getUpdatedUser()));			
			cstm.registerOutParameter("S_RECOMMENDED_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_RECOMMENDED_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public List<RecommendedResponse> list(RecommendedRequest recommendedRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_RECOMMENDED.SP_LIST_RECOMMENDED(?,?,?,?)}";
        List<RecommendedResponse> response = new ArrayList<>();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("P_CODE", Utility.getString(recommendedRequest.getCode()));
			cstm.setString("P_DESCRIPTION", Utility.getString(recommendedRequest.getDescription()));			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Utility.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					RecommendedResponse recommendedResponse = new RecommendedResponse();
					recommendedResponse.setId(Utility.parseLongToString(rs.getLong("RECOMMENDED_ID")));
					recommendedResponse.setCode(Utility.getString(rs.getString("RECOMMENDED_CODE")));					
					recommendedResponse.setDescription(Utility.getString(rs.getString("RECOMMENDED_DESCRIPTION")));
					recommendedResponse.setEnabled(Utility.getString(rs.getString("RECOMMENDED_ENABLED")));
				
					response.add(recommendedResponse);
				}	
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	@Override
	public RecommendedResponse read(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_RECOMMENDED.SP_GET_RECOMMENDED(?,?,?)}";
        RecommendedResponse recommendedResponse = new RecommendedResponse();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_RECOMMENDED_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Utility.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					recommendedResponse.setId(Utility.parseLongToString(rs.getLong("RECOMMENDED_ID")));
					recommendedResponse.setCode(Utility.getString(rs.getString("RECOMMENDED_CODE")));					
					recommendedResponse.setDescription(Utility.getString(rs.getString("RECOMMENDED_DESCRIPTION")));
					recommendedResponse.setEnabled(Utility.getString(rs.getString("RECOMMENDED_ENABLED")));
				}	
				this.closeResultSet(rs);				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		
		return recommendedResponse;	
	}

}
