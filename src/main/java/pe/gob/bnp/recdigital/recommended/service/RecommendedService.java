package pe.gob.bnp.recdigital.recommended.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.recdigital.recommended.dto.RecommendedDto;
import pe.gob.bnp.recdigital.recommended.dto.RecommendedRequest;
import pe.gob.bnp.recdigital.recommended.dto.RecommendedResponse;
import pe.gob.bnp.recdigital.recommended.exception.RecommendedException;
import pe.gob.bnp.recdigital.recommended.mapper.RecommendedMapper;
import pe.gob.bnp.recdigital.recommended.model.Recommended;
import pe.gob.bnp.recdigital.recommended.repository.RecommendedRepository;
import pe.gob.bnp.recdigital.recommended.validation.RecommendedValidation;
import pe.gob.bnp.recdigital.utilitary.common.Notification;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;

@Service
public class RecommendedService {

	@Autowired
	RecommendedRepository recommendedRepository;

	@Autowired
	RecommendedMapper recommendedMapper;

	public ResponseTransaction save(RecommendedDto recommendedDto) {

		Notification notification = RecommendedValidation.validation(recommendedDto);
		
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}

		Recommended recommended = recommendedMapper.reverseMapperSave(recommendedDto);
		ResponseTransaction response = recommendedRepository.persist(recommended);
		
		return RecommendedException.setMessageResponseSave(response);

	}
	
	public ResponseTransaction update(Long id,RecommendedDto recommendedDto) {
		
    	Notification notification = RecommendedValidation.validation(recommendedDto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}

		Recommended recommended = recommendedMapper.reverseMapperUpdate(id,recommendedDto);
		ResponseTransaction response = recommendedRepository.update(recommended);
		
		return RecommendedException.setMessageResponseUpdate(response);

	}
	
	public List<RecommendedResponse> searchAll(String code,String description){
			RecommendedRequest recommendedRequest = new RecommendedRequest();
			recommendedRequest.setCode(code);
			recommendedRequest.setDescription(description);
			
			Notification notification = RecommendedValidation.validation(recommendedRequest);
			if (notification.hasErrors()) {
				throw new IllegalArgumentException(notification.errorMessage());
			}		
			return recommendedRepository.list(recommendedRequest);
	}
	
	public RecommendedResponse get(Long id) {
		Notification notification = RecommendedValidation.validation(id);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		return recommendedRepository.read(id);
	}

	

}
