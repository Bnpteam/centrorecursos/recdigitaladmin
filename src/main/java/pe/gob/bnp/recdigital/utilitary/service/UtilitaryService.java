package pe.gob.bnp.recdigital.utilitary.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.recdigital.utilitary.repository.UtilitaryRepository;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;

@Service
public class UtilitaryService {
	
	@Autowired
	UtilitaryRepository utilitaryRepository;
	
	public ResponseTransaction searchCategories() {

		return utilitaryRepository.listCategories();
	}
	
	public ResponseTransaction searchFormats() {

		return utilitaryRepository.listFormats();
	}
	
	public ResponseTransaction searchTopics() {

		return utilitaryRepository.listTopics();
	}
	
	public ResponseTransaction searchSpecialities() {

		return utilitaryRepository.listSpecialities();
	}
	
	public ResponseTransaction searchExtensions(String id) {

		return utilitaryRepository.listExtensions(id);
	}
	
	public ResponseTransaction searchSubcategories(String id) {

		return utilitaryRepository.listSubcategories(id);
	}
	


}
