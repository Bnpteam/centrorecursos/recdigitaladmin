package pe.gob.bnp.recdigital.utilitary.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.gob.bnp.recdigital.utilitary.repository.BaseOperation;

public class BaseJDBCOperation implements BaseOperation {
	
	private static final Logger logger = LoggerFactory.getLogger(BaseJDBCOperation.class);
	
	private String catchResultSet="SQLException closeResultSet ";
	private String catchPreparedStatement="SQLException closePreparedStatement ";
	private String catchStatement="SQLException closeStatement ";
	private String catchCallableStatement="SQLException closeCallableStatement ";
	private String catchGetConnection="SQLException getConnection ";
	private String catchCloseConnection="SQLException closeConnection ";
	private String catchCommitTransaction="SQLException commitTransaction ";
	private String catchRollbackTransaction="SQLException rollbackTransaction ";	
	
	
	public void closeSqlConnections(ResultSet rs, CallableStatement cstm) {
		closeResultSet(rs);
		closeCallableStatement(cstm);
	}

	@Override
	public void closeResultSet(ResultSet resultSet) {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
		} catch (Exception exception) {
			logger.error(catchResultSet + exception.getMessage(), exception);
		}
	}

	@Override
	public void closePreparedStatement(PreparedStatement preparedStatement) {
		try {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		} catch (Exception exception) {
			logger.error(catchPreparedStatement + exception.getMessage(), exception);
		}
	}

	@Override
	public void closeStatement(Statement statement) {
		try {
			if (statement != null) {
				statement.close();
			}
		} catch (Exception exception) {
			logger.error(catchStatement + exception.getMessage(), exception);
		}
	}

	@Override
	public void closeCallableStatement(CallableStatement callableStatement) {
		try {
			if (callableStatement != null) {
				callableStatement.close();
			}
		} catch (Exception exception) {
			logger.error(catchCallableStatement + exception.getMessage(), exception);
		}
	}

	@Override
	public Connection getOracleConnection() {
		Connection connection = null;
		try {
			connection = OracleConnection.getConnectionJDBC();
		} catch (Exception exception) {
			logger.error(catchGetConnection + exception.getMessage(), exception);
		}
		return connection;
	}
	
	@Override
	public Connection getMysqlConnection() {
		return null;
	}

	@Override
	public void closeConnection(Connection connection) {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (Exception exception) {
			logger.error(catchCloseConnection + exception.getMessage(), exception);
		}
	}

	@Override
	public void commitTransaction(Connection connection) {
		try {
			if (connection != null) {
				connection.commit();
			}
		} catch (Exception exception) {
			logger.error(catchCommitTransaction + exception.getMessage(), exception);
		}
	}

	@Override
	public void rollbackTransaction(Connection connection) {
		try {
			if (connection != null) {
				connection.rollback();
			}
		} catch (Exception exception) {
			logger.error(catchRollbackTransaction + exception.getMessage(), exception);
		}
	}

}
