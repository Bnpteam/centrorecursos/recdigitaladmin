package pe.gob.bnp.recdigital.utilitary.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import pe.gob.bnp.recdigital.utilitary.dto.ResponseDto;
import pe.gob.bnp.recdigital.utilitary.dto.ResponseOkCommandDto;
import pe.gob.bnp.recdigital.utilitary.dto.ResponseTransactionDto;

@Component
public class ResponseHandler {
	private  Logger logger = LoggerFactory.getLogger(ResponseHandler.class);
	private String codeNotFoundResponse="00404";
	private String codeAppCustomErrorResponse="00400";
	private String codeAppExceptionResponse="00500";
	
	public ResponseEntity<Object> getOkCommandResponse(String message) {
		ResponseDto responseDto = new ResponseDto();
		ResponseOkCommandDto responseOkCommandDto = new ResponseOkCommandDto();
		responseOkCommandDto.setHttpStatus(HttpStatus.OK.value());
		responseOkCommandDto.setMessage(message);
		responseDto.setResponse(responseOkCommandDto);
		return new ResponseEntity<Object>(responseDto.getResponse(), HttpStatus.OK);
	}	
	
	public ResponseEntity<Object> getOkObjectResponse(Object message) {
		return new ResponseEntity<Object>(message, HttpStatus.OK);
	}
	
	public ResponseEntity<Object> getOkResponseTransaction(ResponseTransaction responseTx) {
		ResponseTransactionDto responseTxDto = new ResponseTransactionDto();
		responseTxDto.setHttpStatus(HttpStatus.OK.value());
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.OK);
	}	
	
	public ResponseEntity<Object> getAppCustomErrorResponse(ResponseTransaction responseTx,String errorMessages) {
		
		ResponseTransactionDto responseTxDto = new ResponseTransactionDto();
		responseTxDto.setHttpStatus(HttpStatus.BAD_REQUEST.value());
		responseTx.setCodeResponse(codeAppCustomErrorResponse);
		responseTx.setResponse(errorMessages);
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.BAD_REQUEST);
	}
	
	public ResponseEntity<Object> getAppExceptionResponse(ResponseTransaction responseTx,Throwable e) {
		logger.error(e.getMessage(), e);

		
		ResponseTransactionDto responseTxDto = new ResponseTransactionDto();
		responseTxDto.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		responseTx.setCodeResponse(codeAppExceptionResponse);
		responseTx.setResponse("Error Server:"+e.getMessage());
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.INTERNAL_SERVER_ERROR);
	}		
	
	public ResponseEntity<Object> getNotFoundObjectResponse(ResponseTransaction responseTx,String message) {
		ResponseTransactionDto responseTxDto = new ResponseTransactionDto();
		responseTxDto.setHttpStatus(HttpStatus.NOT_FOUND.value());
		responseTx.setCodeResponse(codeNotFoundResponse);
		responseTx.setResponse("Not Found:"+message);
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.NOT_FOUND);
	}	
	
}
