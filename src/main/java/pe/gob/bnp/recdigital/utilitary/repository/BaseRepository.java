package pe.gob.bnp.recdigital.utilitary.repository;


import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;

public interface BaseRepository<T> {
	
	public ResponseTransaction persist(T entity);
	public ResponseTransaction update(T entity);

}
