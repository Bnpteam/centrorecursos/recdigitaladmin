package pe.gob.bnp.recdigital.utilitary.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;

@Repository
public interface UtilitaryRepository {
	
	public ResponseTransaction listCategories();
	
	public ResponseTransaction listFormats();
	
	public ResponseTransaction listTopics();
	
	public ResponseTransaction listSpecialities();
	
	public ResponseTransaction listExtensions(String id);
	
	public ResponseTransaction listSubcategories(String id);

}
