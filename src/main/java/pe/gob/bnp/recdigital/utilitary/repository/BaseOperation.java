package pe.gob.bnp.recdigital.utilitary.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public interface BaseOperation {

	public void closeResultSet(ResultSet resultSet);

	public void closePreparedStatement(PreparedStatement preparedStatement);

	public void closeStatement(Statement statement);

	public void closeCallableStatement(CallableStatement callableStatement);

	public Connection getOracleConnection();
	
	public Connection getMysqlConnection();

	public void closeConnection(Connection connection);

	public void commitTransaction(Connection connection);

	public void rollbackTransaction(Connection connection);
}
