package pe.gob.bnp.recdigital.utilitary.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigital.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigital.utilitary.repository.UtilitaryRepository;
import pe.gob.bnp.recdigital.utilitary.repository.jdbc.BaseJDBCOperation;
import pe.gob.bnp.recdigital.utilitary.repository.jdbc.UtilitaryRepositoryJDBC;
import pe.gob.bnp.recdigital.utilitary.common.Utility;
import pe.gob.bnp.recdigital.utilitary.dto.CategoryResponse;
import pe.gob.bnp.recdigital.utilitary.dto.ExtensionResponse;
import pe.gob.bnp.recdigital.utilitary.dto.FormatResponse;
import pe.gob.bnp.recdigital.utilitary.dto.SpecialityResponse;
import pe.gob.bnp.recdigital.utilitary.dto.SubcategoryResponse;
import pe.gob.bnp.recdigital.utilitary.dto.TopicResponse;

@Repository
public class UtilitaryRepositoryJDBC extends BaseJDBCOperation  implements UtilitaryRepository{
	
private static final Logger logger = LoggerFactory.getLogger(UtilitaryRepositoryJDBC.class);	
	
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction listCategories() {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_LIST_CATEGORY(?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
				
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					CategoryResponse ubigeoResponse = new CategoryResponse();
					ubigeoResponse.setId(Utility.parseLongToString(rs.getLong("CATEGORY_ID")));
					ubigeoResponse.setName(Utility.getString(rs.getString("CATEGORY_NAME")));
										
					slidersResponse.add(ubigeoResponse);
				}	
				response.setList(slidersResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_UTILITY.SP_LIST_CATEGORY: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTransaction listFormats() {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_LIST_FORMAT(?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
				
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					FormatResponse ubigeoResponse = new FormatResponse();
					ubigeoResponse.setId(Utility.parseLongToString(rs.getLong("FORMAT_ID")));
					ubigeoResponse.setName(Utility.getString(rs.getString("FORMAT_NAME")));
										
					slidersResponse.add(ubigeoResponse);
				}	
				response.setList(slidersResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_UTILITY.SP_LIST_FORMAT: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTransaction listTopics() {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_LIST_TOPIC(?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
				
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					TopicResponse ubigeoResponse = new TopicResponse();
					ubigeoResponse.setId(Utility.parseLongToString(rs.getLong("TOPIC_ID")));
					ubigeoResponse.setName(Utility.getString(rs.getString("TOPIC_NAME")));
										
					slidersResponse.add(ubigeoResponse);
				}	
				response.setList(slidersResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_UTILITY.SP_LIST_TOPIC: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTransaction listSpecialities() {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_LIST_SPECIALITY(?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
				
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					SpecialityResponse ubigeoResponse = new SpecialityResponse();
					ubigeoResponse.setId(Utility.parseLongToString(rs.getLong("SPECIALITY_ID")));
					ubigeoResponse.setName(Utility.getString(rs.getString("SPECIALITY_NAME")));
										
					slidersResponse.add(ubigeoResponse);
				}	
				response.setList(slidersResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_UTILITY.SP_LIST_SPECIALITY: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTransaction listExtensions(String id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_LIST_EXTENSION(?,?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_ID", Utility.parseStringToLong(id));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					ExtensionResponse ubigeoResponse = new ExtensionResponse();
					ubigeoResponse.setId(Utility.parseLongToString(rs.getLong("EXTENSION_ID")));
					ubigeoResponse.setFormatId(Utility.parseLongToString(rs.getLong("FORMAT_ID")));
					ubigeoResponse.setName(Utility.getString(rs.getString("EXTENSION_NAME")));
					
					slidersResponse.add(ubigeoResponse);
				}	
				response.setList(slidersResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_UTILITY.SP_LIST_EXTENSION: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction listSubcategories(String id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_LIST_SUBCATEGORIES(?,?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_ID", Utility.parseStringToLong(id));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					SubcategoryResponse ubigeoResponse = new SubcategoryResponse();
					ubigeoResponse.setId(Utility.parseLongToString(rs.getLong("SUBCATEGORY_ID")));
					ubigeoResponse.setCategoryId(Utility.parseLongToString(rs.getLong("CATEGORY_ID")));
					ubigeoResponse.setName(Utility.getString(rs.getString("SUBCATEGORY_NAME")));
					
					slidersResponse.add(ubigeoResponse);
				}	
				response.setList(slidersResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_UTILITY.SP_LIST_SUBCATEGORIES: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

}
