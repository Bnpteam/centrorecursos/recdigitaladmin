package pe.gob.bnp.recdigital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecdigitalApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecdigitalApplication.class, args);
	}

}
